package org.perfomics.flights.business.flydubai;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.perfomics.flights.business.flydubai.exception.FareQuoteException;
import org.perfomics.flights.business.flydubai.xmlservice.FlydubaiXmlService;
import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommission;
import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommissionRequest;
import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommissionResult;
import org.perfomics.flights.business.flydubai.xo.agencycommission.TravelAgencyCommission;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AirlinePerson;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFees;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFeesRequest;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFeesResult;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfo;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfos;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.LogicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.PhysicalFlight;
import org.perfomics.flights.business.flydubai.xo.common.Body;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePNR;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePNRResult;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePnrRequest;
import org.perfomics.flights.business.flydubai.xo.createpnr.Payment;
import org.perfomics.flights.business.flydubai.xo.createpnr.Payments;
import org.perfomics.flights.business.flydubai.xo.farequote.FareInfo;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteDetail;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteDetails;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteRequestInfo;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteRequestInfos;
import org.perfomics.flights.business.flydubai.xo.farequote.FareType;
import org.perfomics.flights.business.flydubai.xo.farequote.FlightSegment;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuote;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuoteRequest;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuoteResult;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgent;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentRequest;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentResult;
import org.perfomics.flights.business.flydubai.xo.processpnr.PNRPaymentRequest;
import org.perfomics.flights.business.flydubai.xo.processpnr.PNRPayments;
import org.perfomics.flights.business.flydubai.xo.processpnr.Payor;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPayment;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPayment1;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPaymentResult;
import org.perfomics.flights.business.flydubai.xo.processpnr.ReservationInfo;
import org.perfomics.flights.business.flydubai.xo.processpnr.TransactionInfo;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePNR;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePNRResult;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePnrRequest;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityToken;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenRequest;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenResult;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Address;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Passengers;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Person;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Segment;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Segments;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPNR;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPNRResult;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPnrRequest;

public class CreateUnpaidPNRAndRetrieveBookingApp {

	public final static Logger LOGGER = Logger.getLogger(CreatePaidBookingApp.class);

	public static void main(String[] args) throws Exception {

		Calendar cal = Calendar.getInstance();

		cal.set(2016, 4, 8);
		Date departdate = cal.getTime();

		cal.set(2016, 4,11);
		Date returndate = cal.getTime();

		StringBuilder origin = new StringBuilder("DXB");
		StringBuilder destination = new StringBuilder("DEL");

		// Economy  or Business
		StringBuilder cabin = new StringBuilder("BUSINESS");

		int adult = 1;
		int child = 0;
		int infant = 0;

		/*
		 * returnOrOneWay=1 means OneWayTransfer returnOrOneWay=2 means
		 * ReturnTransfer
		 */
		int returnOrOneWay = 1;

		String currency = "INR";
		String iataNumber = "05052015";

		FileOutputStream output = new FileOutputStream("//home/administrator/Desktop/OutputFileOfFlyDubai.xml");
		PrintStream stream = new PrintStream(output);
		System.setOut(stream);

		LOGGER.debug("1.RetrieveSecurityToken\n");

		Envelope tokenRequest = new Envelope();

		CarrierCode ccode = new CarrierCode();
		ccode.setAccessibleCarrierCode("FZ");

		CarrierCodes ccodes = new CarrierCodes();
		ccodes.setCarrierCode(ccode);

		RetrieveSecurityTokenRequest streq = new RetrieveSecurityTokenRequest();
		streq.setLogonId("SPLENDID_FZ_U");
		streq.setPassword("splendid");
		streq.setCarrierCodes(ccodes);

		RetrieveSecurityToken rst = new RetrieveSecurityToken();
		rst.setSecurityTokenRequest(streq);

		Body body = new Body();
		body.setSecurityToken(rst);

		tokenRequest.setBody(body);

		Envelope tokenresponse = FlydubaiXmlService.retrieveSecurityToken(tokenRequest);

		RetrieveSecurityTokenResult rstr = tokenresponse.getBody().getSecurityTokenResponse()
				.getRetrieveSecurityTokenResult();
		String securityToken = rstr.getSecuritytoken();
		LOGGER.debug("\n Security Token:" + securityToken);

		// LoginTravelAgent
		LOGGER.debug("\n 2. LoginTravelAgent\n");

		Envelope loginRequest = new Envelope();

		LoginTravelAgentRequest ltar = new LoginTravelAgentRequest();

		ltar.setSecurityGUID(securityToken);
		ltar.setClientIP(null);
		ltar.setHistoricUserName(null);
		ltar.setIataNumber(iataNumber);
		ltar.setUserName("apipml");
		ltar.setPassword("Dubai@2016");

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		ltar.setCarrierCodes(ccodes);

		LoginTravelAgent lta = new LoginTravelAgent();
		lta.setLoginTravelAgentRequest(ltar);

		Body loginBody = new Body();
		loginBody.setLoginTravelAgent(lta);

		loginRequest.setBody(loginBody);

		Envelope loginResponse = FlydubaiXmlService.loginTravelAgent(loginRequest);

		LoginTravelAgentResult ltaresult = loginResponse.getBody().getLoginTravelAgentResponse()
				.getLoginTravelAgentResult();
		LOGGER.debug("\n" + ltaresult.isLoggedIn());

		// 3. RetrieveAgencyCommission

		LOGGER.debug("\n 3.RetrieveAgencyCommissionRequest ");

		Envelope commissionRequest = new Envelope();

		RetrieveAgencyCommissionRequest racr = new RetrieveAgencyCommissionRequest();
		racr.setSecurityGUID(securityToken);
		racr.setClientIP(null);
		racr.setHistoricUserName(null);
		racr.setCurrencyCode(currency);
		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		racr.setCarrierCodes(ccodes);

		RetrieveAgencyCommission rac = new RetrieveAgencyCommission();
		rac.setRetrieveAgencyCommissionRequest(racr);

		Body commisionBody = new Body();
		commisionBody.setRetrieveAgencyCommission(rac);

		commissionRequest.setBody(commisionBody);

		Envelope commissionResponse = FlydubaiXmlService.retrieveAgencyCommission(commissionRequest);

		RetrieveAgencyCommissionResult racresult = commissionResponse.getBody().getRetrieveAgencyCommissionResponse()
				.getRetrieveAgencyCommissionResult();

		TravelAgencyCommission travelAgencyCommission = racresult.getTravelAgencyCommissions()
				.getTravelAgencyCommission();
		LOGGER.debug("\n Commission Amount:" + travelAgencyCommission.getAmount());

		// Search For The Flight Availability
		LOGGER.debug("\n 4. RetrieveFareQuote ");

		Envelope fareQuoteRequest = new Envelope();

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */

		RetrieveFareQuoteRequest rfqr = new RetrieveFareQuoteRequest();
		rfqr.setSecurityGUID(securityToken);
		rfqr.setCarrierCodes(ccodes);
		rfqr.setHistoricUserName(null);
		rfqr.setCurrencyOfFareQuote(currency);
		rfqr.setPromotionalCode("");
		rfqr.setIataNumberRequestor(iataNumber);
		rfqr.setCorporationID(-214);

		FareQuoteDetail quoteDetail = new FareQuoteDetail();
		quoteDetail.setOrigin(new String(origin));
		quoteDetail.setDestination(new String(destination));
		quoteDetail.setAirportsNotMetroGroups(false);
		quoteDetail.setDepartDate(departdate);
		quoteDetail.setReturnDate(returndate);
		quoteDetail.setReturnOrOneWay(returnOrOneWay);

		quoteDetail.setFareTypeCategory(1);
		quoteDetail.setCabin(new String(cabin));
		quoteDetail.setLfID(-214);
		quoteDetail.setOperatingCarrierCode("FZ");
		quoteDetail.setMarketingCarrierCode("FZ");
		quoteDetail.setLanguageCode("en");
		quoteDetail.setTicketPackageID(1);

		FareQuoteRequestInfo quoteInfo = new FareQuoteRequestInfo();
		quoteInfo.setNumberOfAdult(adult);
		quoteInfo.setNumberOfChild(child);
		quoteInfo.setNumberOfInfant(infant);

		FareQuoteRequestInfos quoteInfos = new FareQuoteRequestInfos();
		quoteInfos.addFareinfolist(quoteInfo);

		quoteDetail.setFareQuoteRequestInfos(quoteInfos);

		FareQuoteDetails quoteDetails = new FareQuoteDetails();
		quoteDetails.addFaredetaillist(quoteDetail);

		rfqr.setFareQuoteDetails(quoteDetails);

		RetrieveFareQuote rfq = new RetrieveFareQuote();
		rfq.setRetrieveFareQuoteRequest(rfqr);

		Body fareQuoteBody = new Body();
		fareQuoteBody.setRetrieveFareQuote(rfq);

		fareQuoteRequest.setBody(fareQuoteBody);

		Envelope fareQuoteResponse = FlydubaiXmlService.retrieveFareQuote(fareQuoteRequest);

		RetrieveFareQuoteResult rfqResult = fareQuoteResponse.getBody().getRetrieveFareQuoteResponse()
				.getRetrieveFareQuoteResult();

		// Extract Day from departDate & returnDate
		int departureDay = DateUtils.toCalendar(departdate).get(Calendar.DATE);
		int returnDay = DateUtils.toCalendar(returndate).get(Calendar.DATE);

		Double departTotal[] = new Double[6];
		Double returnTotal[] = new Double[6];

		double total = 0.0;
		int i = 0;

		List<FlightSegment> flightSegmentList = rfqResult.getFlightSegments().getFlightlist();
		for (FlightSegment flightSegment : flightSegmentList) {
			LOGGER.debug("\n DepartureDate :" + flightSegment.getDepartDate());

			try {
				Date receivedDate = flightSegment.getDepartDate();
				int receivedDay = DateUtils.toCalendar(receivedDate).get(Calendar.DATE);

				if (receivedDay == departureDay) {

					List<FareType> fareTypeList = flightSegment.getFareTypes().getFaretypelist();
					for (FareType fareType : fareTypeList) {
						LOGGER.debug("\n" + fareType.getFareTypeName());
						total = 0.0;

						List<FareInfo> fareInfoList = fareType.getFareInfos().getFareinfolist();
						for (FareInfo fareInfo : fareInfoList) {
							if (fareInfo.getPtcID() == 1) {
								LOGGER.debug("FareID  of Adult(PTCId=1):" + fareInfo.getFareID());
								LOGGER.debug("PTCId :" + fareInfo.getPtcID());
								LOGGER.debug("Amount(Per Pax) :" + fareInfo.getFareAmtInclTax());
								total += (fareInfo.getFareAmtInclTax() * adult);
							}
							if (fareInfo.getPtcID() == 6) {
								LOGGER.debug("PTCId :" + fareInfo.getPtcID());
								LOGGER.debug("Amount(Per Pax) :" + fareInfo.getFareAmtInclTax());
								total += (fareInfo.getFareAmtInclTax() * child);
							}
							if (fareInfo.getPtcID() == 5) {
								LOGGER.debug("PTCId :" + fareInfo.getPtcID());
								LOGGER.debug("Amount(Per Pax) :" + fareInfo.getFareAmtInclTax());
								total += (fareInfo.getFareAmtInclTax() * infant);
							}

						}
						LOGGER.debug("Total Amount" + total);
						departTotal[i] = total;
						i++;
					}
				}

				i = 0;
				if (receivedDay == returnDay) {
					List<FareType> fareTypeList = flightSegment.getFareTypes().getFaretypelist();
					for (FareType fareType : fareTypeList) {
						LOGGER.debug("\n" + fareType.getFareTypeName());
						total = 0.0;

						List<FareInfo> fareInfoList = fareType.getFareInfos().getFareinfolist();
						for (FareInfo fareInfo : fareInfoList) {
							if (fareInfo.getPtcID() == 1) {
								LOGGER.debug("FareID  of Adult(PTCId=1):" + fareInfo.getFareID());
								LOGGER.debug("PTCId :" + fareInfo.getPtcID());
								LOGGER.debug("Amount(Per Pax) :" + fareInfo.getFareAmtInclTax());
								total += (fareInfo.getFareAmtInclTax() * adult);
							}
							if (fareInfo.getPtcID() == 6) {
								LOGGER.debug("PTCId :" + fareInfo.getPtcID());
								LOGGER.debug("Amount(Per Pax) :" + fareInfo.getFareAmtInclTax());
								total += (fareInfo.getFareAmtInclTax() * child);
							}
							if (fareInfo.getPtcID() == 5) {
								LOGGER.debug("PTCId :" + fareInfo.getPtcID());
								LOGGER.debug("Amount(Per Pax) :" + fareInfo.getFareAmtInclTax());
								total += (fareInfo.getFareAmtInclTax() * infant);
							}

						}
						LOGGER.debug("Total Amount" + total);
						returnTotal[i] = total;
						i++;
					}
				}

			} catch (Exception e) {
				throw new FareQuoteException(e);
			}
		}

		double amount = 0.0;
		int fareid1 = 1, fareid2 = 0;

		Scanner sc = new Scanner(System.in);

		if (returnTotal[0] == null) {
			amount = departTotal[0];
		} else {
			LOGGER.debug("\n Enter 2nd Fare ID :");
			fareid2 = sc.nextInt();
			amount = departTotal[0] + returnTotal[0];
		}

		// Enter the Details of Passenger(PNR Summary)

		LOGGER.debug("\n 6.SummaryPNR (creating the booking)");

		Envelope summaryPnrRequest = new Envelope();

		SummaryPnrRequest spr = new SummaryPnrRequest();
		spr.setClientIP(null);
		spr.setSecurityGUID(securityToken);
		spr.setSecurityToken(securityToken);

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */

		spr.setCarrierCodes(ccodes);
		spr.setCarrierCurrency(currency);
		spr.setDisplayCurrency(currency);
		spr.setIataNum(iataNumber);
		spr.setUser("");
		spr.setPromoCode("");

		Address address = new Address();
		address.setAddress1("FZ HQ");
		address.setAddress2("FZ HQ");
		address.setCity("DUBAI");
		address.setState("DUBAI");
		address.setPostal("353");
		address.setCountry("AE");    
		address.setCountryCode("00971");
		address.setAreaCode("4");
		address.setPhoneNumber("6033954");

		spr.setAddress(address);

		ContactInfo cntinfo = new ContactInfo();		
		cntinfo.setEmail("deshpandey.swamy@flydubai.com");
		cntinfo.setWorkPhone("9716033954");
		
		ContactInfos cntinfos = new ContactInfos();
		cntinfos.addContactInfoList(cntinfo);

		spr.setContactInfos(cntinfos);

		Passengers passengers = new Passengers();

		for (i = 0; i <adult; i++) {
			Person person = new Person();
			 cal.set(1992,1,1);

			Date dob = cal.getTime();
			person.setDob(dob);

			person.setFirstName("Test");
			person.setMiddleName("Test");
			person.setLastName("Test");

			person.setTitle("Mr");
			person.setGender("Male");

			passengers.addPersonList(person);
		}
		for (i = 0; i <child; i++) {
			Person person = new Person();
			 cal.set(2009,5,6);

			Date dob = cal.getTime();
			person.setDob(dob);

			person.setFirstName("Test");
			person.setMiddleName("Test");
			person.setLastName("Test");

			person.setTitle("Mr");
			person.setGender("Male");

			passengers.addPersonList(person);
		}
		for (i = 0; i <infant; i++) {
			Person person = new Person();
			 cal.set(2015,4,5);

			Date dob = cal.getTime();
			person.setDob(dob);

			person.setFirstName("Test");
			person.setMiddleName("Test");
			person.setLastName("Test");

			person.setTitle("Mr");
			person.setGender("Male");

			passengers.addPersonList(person);
		}

		sc.close();

		spr.setPassengers(passengers);

		Segments segments = new Segments();

		Segment segment = new Segment();
		segment.setFareInformationID(fareid1);
		segments.addSegmentList(segment);

		if (returnOrOneWay == 2) {
			Segment segment1 = new Segment();
			segment1.setFareInformationID(fareid2);
			segments.addSegmentList(segment1);

		}
		spr.setSegments(segments);

		Payments payments = new Payments();

		Payment payment = new Payment();
		payment.setPaymentAmount(amount);
		payment.setPaymentCurrency(currency);

		payments.setPayment(payment);

		spr.setPayments(payments);

		SummaryPNR summaryPnr = new SummaryPNR();
		summaryPnr.setSummaryPnrRequest(spr);

		Body summaryBody = new Body();
		summaryBody.setSummaryPNR(summaryPnr);

		summaryPnrRequest.setBody(summaryBody);

		Envelope summaryPnrResponse = FlydubaiXmlService.summaryPNR(summaryPnrRequest);

		SummaryPNRResult spresult = summaryPnrResponse.getBody().getSummaryPNRResponse().getSummaryPNRResult();
		LOGGER.debug("\n ReservationBalance :" + spresult.getReservationBalance());
		LOGGER.debug("Total Number of Passenger :" + spresult.getActivePassengerCount());
		LOGGER.debug("Confirmation Number:" + spresult.getConfirmationNumber());

		LogicalFlight logicalFlight = spresult.getAirlines().getAirlineList().get(0).getLogicalFlight()
				.getLogicalFlightList().get(0);

		PhysicalFlight physicalFlight = logicalFlight.getPhysicalFlights().getPhysicalFlightList().get(0);

		AirlinePerson airlinePerson = physicalFlight.getCustomers().getCustomerList().get(0).getAirlinePersons()
				.getAirlinePersonList().get(0);

		String personOrgId = airlinePerson.getPersonOrgID();
		String gender = airlinePerson.getGender();
		String title = airlinePerson.getTitle();
		String firstName = airlinePerson.getFirstName();
		String lastName = airlinePerson.getLastName();
		String middleName = airlinePerson.getMiddleName();
		Date dob = airlinePerson.getDob();

		// 7. AssessAgencyTransactionFees

		LOGGER.debug("\n7. AssessAgencyTransactionFees");

		Envelope transactionFeesRequest = new Envelope();

		AssessAgencyTransactionFeesRequest atfr = new AssessAgencyTransactionFeesRequest();

		atfr.setSecurityGUID(securityToken);
		atfr.setClientIP(null);
		atfr.setHistoricUserName(null);
		atfr.setAgencyCurrency(currency);

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		atfr.setCarrierCodes(ccodes);

		AssessAgencyTransactionFees atf = new AssessAgencyTransactionFees();
		atf.setAssessAgencyTransactionFeesRequest(atfr);

		Body transactionFeesBody = new Body();
		transactionFeesBody.setAssessAgencyTransactionFees(atf);

		transactionFeesRequest.setBody(transactionFeesBody);

		Envelope transactionFeesResponse = FlydubaiXmlService.agencyTransactionFees(transactionFeesRequest);

		AssessAgencyTransactionFeesResult atfresult = transactionFeesResponse.getBody()
				.getAssessAgencyTransactionFeesResponse().getAssessAgencyTransactionFeesResult();
		double agencyReservationBalance = atfresult.getReservationBalance();

		LOGGER.debug("\n Agency ReservationBalance :" + agencyReservationBalance);

		
		// 8. CreatePNR (ActionType - CommitSummary)

		LOGGER.debug("\n8.CreatePNR(ActionType - CommitSummary)");

		Envelope createPnrRequest = new Envelope();

		CreatePnrRequest cpr = new CreatePnrRequest();
		cpr.setSecurityGUID(securityToken);
		cpr.setActionType("CommitSummary");

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		cpr.setCarrierCodes(ccodes);

		CreatePNR createPnr = new CreatePNR();
		createPnr.setCreatePnrRequest(cpr);

		Body createPnrBody = new Body();
		createPnrBody.setCreatePNR(createPnr);

		createPnrRequest.setBody(createPnrBody);

		Envelope createPnrResponse = FlydubaiXmlService.createPNR(createPnrRequest);

		CreatePNRResult cpresult = createPnrResponse.getBody().getCreatePNRResponse().getCreatePNRResult();

		String confirmationNumber = cpresult.getConfirmationNumber();
		Date bookingDate = cpresult.getBookDate();

		LOGGER.debug("\n ReservationBalance :" + cpresult.getReservationBalance());
		LOGGER.debug("\n PNR Pin :" + cpresult.getPnrPin());
		LOGGER.debug("Total Number of Passenger :" + cpresult.getActivePassengerCount());
		LOGGER.debug("Confirmation Number:" + confirmationNumber);
		LOGGER.debug("Booking Date:" + bookingDate);

		
		// 9. RetrievePNR
		LOGGER.debug("\n9. RetrievePNR");

		Envelope retrievePnrRequest = new Envelope();

		ReservationInfo rinfo = new ReservationInfo();
		rinfo.setConfirmationNumber(confirmationNumber);

		RetrievePnrRequest rpr = new RetrievePnrRequest();

		rpr.setSecurityGUID(securityToken);
		rpr.setReservationInfo(rinfo);
		rpr.setActionType("GetReservation");

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		rpr.setCarrierCodes(ccodes);

		RetrievePNR retrievePnr = new RetrievePNR();
		retrievePnr.setRetrievePnrRequest(rpr);

		Body retrievePnrBody = new Body();
		retrievePnrBody.setRetrievePNR(retrievePnr);

		retrievePnrRequest.setBody(retrievePnrBody);

		Envelope retrievePnrResponse = FlydubaiXmlService.retreivePNR(retrievePnrRequest);

		RetrievePNRResult rpResult = retrievePnrResponse.getBody().getRetrievePNRResponse().getRetrievePNRResult();

		LOGGER.debug("\n ReservationBalance :" + rpResult.getReservationBalance());
		LOGGER.debug("\n LogicalFlightCount:" + rpResult.getLogicalFlightCount());
		LOGGER.debug("Total Number of Passenger :" + rpResult.getActivePassengerCount());
		LOGGER.debug("Confirmation Number:" + rpResult.getConfirmationNumber());

		confirmationNumber = rpResult.getConfirmationNumber();
		double reservationBalance = rpResult.getReservationBalance();

		// 10. ProcessPNRPayment
		LOGGER.debug("\n 10.ProcessPNRPayment");

		Envelope processPnrRequest = new Envelope();

		PNRPaymentRequest pnrPaymentRequest = new PNRPaymentRequest();

		TransactionInfo transinfo = new TransactionInfo();
		transinfo.setSecurityGUID(securityToken);
		transinfo.setCarrierCodes(ccodes);

		rinfo.setConfirmationNumber(confirmationNumber);

		pnrPaymentRequest.setTransactionInfo(transinfo);
		pnrPaymentRequest.setReservationInfo(rinfo);

		PNRPayments pnrPayments = new PNRPayments();

		// Setting Reservation Balance
		ProcessPNRPayment1 processPnrPayment1 = new ProcessPNRPayment1();
		processPnrPayment1.setBaseAmount(reservationBalance);
		processPnrPayment1.setBaseCurrency(currency);
		processPnrPayment1.setPaymentAmount(reservationBalance);
		processPnrPayment1.setIataNumber(iataNumber);
		processPnrPayment1.setOriginalCurrency(currency);
		processPnrPayment1.setOriginalAmount(reservationBalance);

		// Setting of Payor Attributes
		Payor payor = new Payor();

		payor.setAddress(address);
		payor.setContactInfos(cntinfos);
		payor.setFirstName(firstName);
		payor.setLastName(lastName);
		payor.setMiddleName(middleName);
		payor.setPersonOrgID(personOrgId);

		payor.setDob(dob);
		payor.setGender(gender);
		payor.setTitle(title);

		processPnrPayment1.setPayor(payor);

		pnrPayments.setProcessPNRPayment1(processPnrPayment1);
		pnrPaymentRequest.setPnrPayments(pnrPayments);

		ProcessPNRPayment processPNRPayment = new ProcessPNRPayment();
		processPNRPayment.setPnrPaymentRequest(pnrPaymentRequest);

		Body processPnrBody = new Body();
		processPnrBody.setProcessPNRPayment(processPNRPayment);

		processPnrRequest.setBody(processPnrBody);

		Envelope processPnrResponse = FlydubaiXmlService.processPNR(processPnrRequest);
		ProcessPNRPaymentResult pppResult = processPnrResponse.getBody().getProcessPNRPaymentResponse()
				.getProcessPNRPaymentResult();

		LOGGER.debug("\n ReservationBalance :" + pppResult.getReservationBalance());
		LOGGER.debug("Total Number of Passenger :" + pppResult.getActivePassengerCount());

		// 11. CreatePNR (ActionType - SaveReservation)

		LOGGER.debug("\n11.CreatePNR(ActionType - SaveReservation)");

		rinfo.setConfirmationNumber(confirmationNumber);

		cpr.setActionType("SaveReservation");
		cpr.setReservationInfo(rinfo);

		createPnrResponse = FlydubaiXmlService.createPNR(createPnrRequest);

		cpresult = createPnrResponse.getBody().getCreatePNRResponse().getCreatePNRResult();

		LOGGER.debug("\n ReservationBalance :" + cpresult.getReservationBalance());
		LOGGER.debug("\n PNR Pin :" + cpresult.getPnrPin());
		LOGGER.debug("Total Number of Passenger :" + cpresult.getActivePassengerCount());
		LOGGER.debug("Confirmation Number:" + cpresult.getConfirmationNumber());

	}
}

package org.perfomics.flights.business.flydubai;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.perfomics.flights.business.flydubai.xmlservice.FlydubaiXmlService;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AirlinePerson;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfo;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfos;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.LogicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.PhysicalFlight;
import org.perfomics.flights.business.flydubai.xo.common.Body;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePNR;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePNRResult;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePnrRequest;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgent;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentRequest;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentResult;
import org.perfomics.flights.business.flydubai.xo.processpnr.PNRPaymentRequest;
import org.perfomics.flights.business.flydubai.xo.processpnr.PNRPayments;
import org.perfomics.flights.business.flydubai.xo.processpnr.Payor;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPayment;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPayment1;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPaymentResult;
import org.perfomics.flights.business.flydubai.xo.processpnr.ReservationInfo;
import org.perfomics.flights.business.flydubai.xo.processpnr.TransactionInfo;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePNR;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePNRResult;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePnrRequest;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityToken;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenRequest;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenResult;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Address;

public class RetrieveAndPayForUnpaidPNRApp {

	public final static Logger LOGGER = Logger.getLogger(CreatePaidBookingApp.class);

	public static void main(String[] args) throws Exception {

		String currency = "INR";
		String iataNumber = "05052015";

		FileOutputStream output = new FileOutputStream("//home/administrator/Desktop/OutputFileOfFlyDubai.xml");
		PrintStream stream = new PrintStream(output);
		System.setOut(stream);

		LOGGER.debug("1.RetrieveSecurityToken\n");

		Envelope tokenRequest = new Envelope();

		CarrierCode ccode = new CarrierCode();
		ccode.setAccessibleCarrierCode("FZ");

		CarrierCodes ccodes = new CarrierCodes();
		ccodes.setCarrierCode(ccode);

		RetrieveSecurityTokenRequest streq = new RetrieveSecurityTokenRequest();
		streq.setLogonId("SPLENDID_FZ_U");
		streq.setPassword("splendid");
		streq.setCarrierCodes(ccodes);

		RetrieveSecurityToken rst = new RetrieveSecurityToken();
		rst.setSecurityTokenRequest(streq);

		Body body = new Body();
		body.setSecurityToken(rst);

		tokenRequest.setBody(body);

		Envelope tokenresponse = FlydubaiXmlService.retrieveSecurityToken(tokenRequest);

		RetrieveSecurityTokenResult rstr = tokenresponse.getBody().getSecurityTokenResponse()
				.getRetrieveSecurityTokenResult();
		String securityToken = rstr.getSecuritytoken();
		LOGGER.debug("\n Security Token:" + securityToken);

		// LoginTravelAgent
		LOGGER.debug("\n 2. LoginTravelAgent\n");

		Envelope loginRequest = new Envelope();

		LoginTravelAgentRequest ltar = new LoginTravelAgentRequest();

		ltar.setSecurityGUID(securityToken);
		ltar.setClientIP(null);
		ltar.setHistoricUserName(null);
		ltar.setIataNumber(iataNumber);
		ltar.setUserName("apipml");
		ltar.setPassword("Dubai@2016");

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		ltar.setCarrierCodes(ccodes);

		LoginTravelAgent lta = new LoginTravelAgent();
		lta.setLoginTravelAgentRequest(ltar);

		Body loginBody = new Body();
		loginBody.setLoginTravelAgent(lta);

		loginRequest.setBody(loginBody);

		Envelope loginResponse = FlydubaiXmlService.loginTravelAgent(loginRequest);

		LoginTravelAgentResult ltaresult = loginResponse.getBody().getLoginTravelAgentResponse()
				.getLoginTravelAgentResult();
		LOGGER.debug("\n" + ltaresult.isLoggedIn());


		LOGGER.debug("List Of Confirmation Number :");
		
		File file1 = new File("//home/administrator/Desktop/FlyDubai/StoreOfConfirmationNumber/");
		String s[]=file1.list();
		for(String name:s)
		{
			LOGGER.debug(name);
		}
		
		LOGGER.debug("Enter Confirmation Number :");
		Scanner sc = new Scanner(System.in);
		String confirmationNumber = sc.next();

		sc.close();

		
		// 3. RetrievePNR
		LOGGER.debug("\n3. RetrievePNR");

		Envelope retrievePnrRequest = new Envelope();

		ReservationInfo rinfo = new ReservationInfo();
		rinfo.setConfirmationNumber(confirmationNumber);

		RetrievePnrRequest rpr = new RetrievePnrRequest();

		rpr.setSecurityGUID(securityToken);
		rpr.setReservationInfo(rinfo);
		rpr.setActionType("GetReservation");

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		rpr.setCarrierCodes(ccodes);

		RetrievePNR retrievePnr = new RetrievePNR();
		retrievePnr.setRetrievePnrRequest(rpr);

		Body retrievePnrBody = new Body();
		retrievePnrBody.setRetrievePNR(retrievePnr);

		retrievePnrRequest.setBody(retrievePnrBody);

		Envelope retrievePnrResponse = FlydubaiXmlService.retreivePNR(retrievePnrRequest);

		RetrievePNRResult rpResult = retrievePnrResponse.getBody().getRetrievePNRResponse().getRetrievePNRResult();

		LogicalFlight logicalFlight = rpResult.getAirlines().getAirlineList().get(0).getLogicalFlight()
				.getLogicalFlightList().get(0);

		PhysicalFlight physicalFlight = logicalFlight.getPhysicalFlights().getPhysicalFlightList().get(0);

		AirlinePerson airlinePerson = physicalFlight.getCustomers().getCustomerList().get(0).getAirlinePersons()
				.getAirlinePersonList().get(0);

		String personOrgId = airlinePerson.getPersonOrgID();
		String gender = airlinePerson.getGender();
		String title = airlinePerson.getTitle();
		String firstName = airlinePerson.getFirstName();
		String lastName = airlinePerson.getLastName();
		String middleName = airlinePerson.getMiddleName();
		Date dob = airlinePerson.getDob();

		ContactInfo contactInfo=rpResult.getContactInfos().getContactInfoList().get(1);
		String countryCode=contactInfo.getCountryCode();
		String areaCode=contactInfo.getAreaCode();
		String phoneNumber=contactInfo.getPhoneNumber();
		String workPhone=contactInfo.getContactField();
		
		ContactInfo contactInfo2=rpResult.getContactInfos().getContactInfoList().get(2);
		String contactID=contactInfo2.getContactID();
		String email=contactInfo2.getContactField();
		
		LOGGER.debug("\n ReservationBalance :" + rpResult.getReservationBalance());
		LOGGER.debug("\n LogicalFlightCount:" + rpResult.getLogicalFlightCount());
		LOGGER.debug("Total Number of Passenger :" + rpResult.getActivePassengerCount());
		LOGGER.debug("Confirmation Number:" + rpResult.getConfirmationNumber());

		confirmationNumber = rpResult.getConfirmationNumber();
		double reservationBalance = rpResult.getReservationBalance();

		Address address = new Address();
		address.setAddress1(airlinePerson.getAddress());
		address.setAddress2(airlinePerson.getAddress2());
		address.setCity(airlinePerson.getCity());
		address.setState(airlinePerson.getState());
		address.setPostal(airlinePerson.getPostal());
		address.setCountry(airlinePerson.getCountry());
		address.setCountryCode(countryCode);
		address.setAreaCode(areaCode);
		address.setPhoneNumber(phoneNumber);

		ContactInfo cntinfo = new ContactInfo();
		cntinfo.setEmail(email);
		cntinfo.setWorkPhone(workPhone);

		ContactInfos cntinfos = new ContactInfos();
		cntinfos.addContactInfoList(cntinfo);

		// 4. ProcessPNRPayment
		LOGGER.debug("\n 4.ProcessPNRPayment");

		Envelope processPnrRequest = new Envelope();

		PNRPaymentRequest pnrPaymentRequest = new PNRPaymentRequest();

		TransactionInfo transinfo = new TransactionInfo();
		transinfo.setSecurityGUID(securityToken);
		transinfo.setCarrierCodes(ccodes);

		rinfo.setConfirmationNumber(confirmationNumber);

		pnrPaymentRequest.setTransactionInfo(transinfo);
		pnrPaymentRequest.setReservationInfo(rinfo);

		PNRPayments pnrPayments = new PNRPayments();

		// Setting Reservation Balance
		ProcessPNRPayment1 processPnrPayment1 = new ProcessPNRPayment1();
		processPnrPayment1.setBaseAmount(reservationBalance);
		processPnrPayment1.setBaseCurrency(currency);
		processPnrPayment1.setPaymentAmount(reservationBalance);
		processPnrPayment1.setIataNumber(iataNumber);
		processPnrPayment1.setOriginalCurrency(currency);
		processPnrPayment1.setOriginalAmount(reservationBalance);

		// Setting of Payor Attributes
		Payor payor = new Payor();

		payor.setAddress(address);
		payor.setContactInfos(cntinfos);
		payor.setFirstName(firstName);
		payor.setLastName(lastName);
		payor.setMiddleName(middleName);
		payor.setPersonOrgID(personOrgId);
		payor.setContactID(contactID);
		
		payor.setDob(dob);
		payor.setGender(gender);
		payor.setTitle(title);

		processPnrPayment1.setPayor(payor);

		pnrPayments.setProcessPNRPayment1(processPnrPayment1);
		pnrPaymentRequest.setPnrPayments(pnrPayments);

		ProcessPNRPayment processPNRPayment = new ProcessPNRPayment();
		processPNRPayment.setPnrPaymentRequest(pnrPaymentRequest);

		Body processPnrBody = new Body();
		processPnrBody.setProcessPNRPayment(processPNRPayment);

		processPnrRequest.setBody(processPnrBody);

		Envelope processPnrResponse = FlydubaiXmlService.processPNR(processPnrRequest);
		ProcessPNRPaymentResult pppResult = processPnrResponse.getBody().getProcessPNRPaymentResponse()
				.getProcessPNRPaymentResult();

		LOGGER.debug("\n ReservationBalance :" + pppResult.getReservationBalance());
		LOGGER.debug("Total Number of Passenger :" + pppResult.getActivePassengerCount());

		// 5. CreatePNR (ActionType - SaveReservation)

		LOGGER.debug("\n5.CreatePNR(ActionType - SaveReservation)");

		Envelope createPnrRequest = new Envelope();

		rinfo.setConfirmationNumber(confirmationNumber);

		CreatePnrRequest cpr = new CreatePnrRequest();
		cpr.setSecurityGUID(securityToken);
		cpr.setActionType("SaveReservation");
		cpr.setReservationInfo(rinfo);

		/*
		 * Same Above CarrierCode ccode=new CarrierCode();
		 * ccode.setAccessibleCarrierCode("FZ");
		 * 
		 * CarrierCodes ccodes=new CarrierCodes(); ccodes.setCarrierCode(ccode);
		 * 
		 */
		cpr.setCarrierCodes(ccodes);

		CreatePNR createPnr = new CreatePNR();
		createPnr.setCreatePnrRequest(cpr);

		Body createPnrBody = new Body();
		createPnrBody.setCreatePNR(createPnr);

		createPnrRequest.setBody(createPnrBody);

		Envelope createPnrResponse = FlydubaiXmlService.createPNR(createPnrRequest);

		CreatePNRResult cpresult = createPnrResponse.getBody().getCreatePNRResponse().getCreatePNRResult();

		LOGGER.debug("\n ReservationBalance :" + cpresult.getReservationBalance());
		LOGGER.debug("\n PNR Pin :" + cpresult.getPnrPin());
		LOGGER.debug("Total Number of Passenger :" + cpresult.getActivePassengerCount());
		LOGGER.debug("Confirmation Number:" + cpresult.getConfirmationNumber());

		File file = new File("//home/administrator/Desktop/FlyDubai/StoreOfConfirmationNumber/"+confirmationNumber);		
		if(file.exists())
		{
			file.delete();
		}

		
	}
}

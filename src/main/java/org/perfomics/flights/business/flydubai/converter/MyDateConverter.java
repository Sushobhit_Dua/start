package org.perfomics.flights.business.flydubai.converter;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class MyDateConverter extends AbstractSingleValueConverter  {

	public boolean canConvert(Class clazz) {
		return clazz.equals(Date.class);
	}

	@Override
	public Object fromString(String str) {
		
		Date deprtureDate=null;
		
		try {
			deprtureDate = DateUtils.parseDate(str, new String[] { "yyyy-MM-dd'T'HH:mm:ss",
					"yyyy-MM-dd'T'HH:mm:ss.SSSSSSS+hh:mm" });
				
		} catch (ParseException e) {
			e.printStackTrace();
		}	
		return deprtureDate;
	}

}

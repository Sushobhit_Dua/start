package org.perfomics.flights.business.flydubai.exception;

public class CommissionException extends Exception {

	public CommissionException(String arg0, Throwable arg1) {
		super(arg0, arg1);

	}

	public CommissionException(String arg0) {
		super(arg0);
		
	}

	public CommissionException(Throwable arg0) {
		super(arg0);
		
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -6756328780384493704L;
	

}

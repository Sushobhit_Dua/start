package org.perfomics.flights.business.flydubai.exception;

public class CreatePNRException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2051837589208641497L;

	public CreatePNRException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public CreatePNRException(String arg0) {
		super(arg0);
		
	}

	public CreatePNRException(Throwable arg0) {
		super(arg0);
		
	}

}

package org.perfomics.flights.business.flydubai.exception;

public class FareQuoteException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5646878289851211833L;

	public FareQuoteException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public FareQuoteException(String arg0) {
		super(arg0);
	
	}

	public FareQuoteException(Throwable arg0) {
		super(arg0);
	
	}

}

package org.perfomics.flights.business.flydubai.exception;

public class IncorrectRequestException extends Exception {

	/**
	 * Only Validators Throws This type of Exception
	 */
	private static final long serialVersionUID = 1L;

	public IncorrectRequestException(String message, Throwable cause) {
		super(message, cause);
	}

	public IncorrectRequestException(String message) {
		super(message);
	}

	public IncorrectRequestException(Throwable cause) {
		super(cause);
	}

}

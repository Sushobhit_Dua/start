package org.perfomics.flights.business.flydubai.exception;

public class LogonException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3527583929162957051L;

	public LogonException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public LogonException(String arg0) {
		super(arg0);
		
	}

	public LogonException(Throwable arg0) {
		super(arg0);
		
	}
	

}

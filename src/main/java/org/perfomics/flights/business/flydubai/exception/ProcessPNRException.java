package org.perfomics.flights.business.flydubai.exception;

public class ProcessPNRException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3584884326908684402L;

	public ProcessPNRException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public ProcessPNRException(String arg0) {
		super(arg0);
		
	}

	public ProcessPNRException(Throwable arg0) {
		super(arg0);
		
	}

}

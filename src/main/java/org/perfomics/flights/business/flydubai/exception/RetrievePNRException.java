package org.perfomics.flights.business.flydubai.exception;

public class RetrievePNRException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3688547732736413350L;

	public RetrievePNRException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public RetrievePNRException(String arg0) {
		super(arg0);
		
	}

	public RetrievePNRException(Throwable arg0) {
		super(arg0);
		
	}
	

}

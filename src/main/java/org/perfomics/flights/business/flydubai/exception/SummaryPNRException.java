package org.perfomics.flights.business.flydubai.exception;

public class SummaryPNRException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1050483498653857759L;

	public SummaryPNRException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		
	}

	public SummaryPNRException(String arg0) {
		super(arg0);
	
	}

	public SummaryPNRException(Throwable arg0) {
		super(arg0);
	
	}

}

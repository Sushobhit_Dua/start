package org.perfomics.flights.business.flydubai.exception;

public class TransactionFeesException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1212539907559343837L;

	public TransactionFeesException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	
	}

	public TransactionFeesException(String arg0) {
		super(arg0);
	
	}

	public TransactionFeesException(Throwable arg0) {
		super(arg0);
	
	}

}

package org.perfomics.flights.business.flydubai.exception;

public class UnableToRetrieveTokenException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1033235680337283494L;

	public UnableToRetrieveTokenException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public UnableToRetrieveTokenException(String arg0) {
		super(arg0);
	
	}

	public UnableToRetrieveTokenException(Throwable arg0) {
		super(arg0);
	
	}
	

}

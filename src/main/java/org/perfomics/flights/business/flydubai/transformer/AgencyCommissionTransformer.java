package org.perfomics.flights.business.flydubai.transformer;

import java.util.List;

import org.perfomics.flights.business.flydubai.exception.CommissionException;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.validator.AgencyCommissionValidator;
import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommissionRequest;
import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommissionResult;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.exceptions.Detail;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.exceptions.InnerExcption;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;

import com.thoughtworks.xstream.XStream;

public class AgencyCommissionTransformer {

	public String transformRequest(Envelope commissionRequest) throws CommissionException {
		try {
			AgencyCommissionValidator commisionValidator = new AgencyCommissionValidator();
			commisionValidator.validate(commissionRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.TravelAgents.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/>");
			builder.append("<soapenv:Body><tem:RetrieveAgencyCommission>");
			builder.append("<tem:RetrieveAgencyCommissionRequest>");

			RetrieveAgencyCommissionRequest racr = commissionRequest.getBody().getRetrieveAgencyCommission()
					.getRetrieveAgencyCommissionRequest();
			builder.append("<rad:SecurityGUID>");
			builder.append(racr.getSecurityGUID());
			builder.append("</rad:SecurityGUID>");

			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			CarrierCode ccode = racr.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			// builder.append("<rad:ClientIPAddress>");
			// builder.append(racr.getClientIP());
			// builder.append("</rad:ClientIPAddress>");

			// builder.append(<rad:HistoricUserName>);
			// builder.append(racr.getHistoricUserName());
			// builder.append(</rad:HistoricUserName>);

			builder.append("<rad1:CurrencyCode>");
			builder.append(racr.getCurrencyCode()); // INR Always
			builder.append("</rad1:CurrencyCode>");

			builder.append("</tem:RetrieveAgencyCommissionRequest></tem:RetrieveAgencyCommission>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new CommissionException(ire);
		} catch (Exception e) {
			throw new CommissionException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws CommissionException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		// Parsing the XML response
		Envelope commissionResponse = (Envelope) xstream.fromXML(xmlRequest);

		RetrieveAgencyCommissionResult racresult = commissionResponse.getBody().getRetrieveAgencyCommissionResponse()
				.getRetrieveAgencyCommissionResult();

		List<ExceptionInformation> exceptionInformationList = racresult.getExceptions()
				.getExceptionExceptionInformationList();

		for (ExceptionInformation exceptionInformation : exceptionInformationList) {
			if (exceptionInformation.getExceptionCode() != 0) {

				throw new CommissionException(exceptionInformation.getExceptionSource() + ":"
						+ exceptionInformation.getExceptionDescription());
			}
		}

		if (commissionResponse.getBody().getRetrieveAgencyCommissionResponse() == null) {

			Detail detail = commissionResponse.getBody().getFault().getDetail();
			InnerExcption innerExcption = detail.getExceptionDetail().getInnerException();

			throw new CommissionException(innerExcption.getMessage());
		}

		return commissionResponse;

	}

}

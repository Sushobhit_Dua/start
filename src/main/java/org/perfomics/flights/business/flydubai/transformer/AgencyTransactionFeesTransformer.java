package org.perfomics.flights.business.flydubai.transformer;

import org.perfomics.flights.business.flydubai.converter.MyDateConverter;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.exception.TransactionFeesException;
import org.perfomics.flights.business.flydubai.validator.AgencyTransactionFeesValidator;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AirlinePerson;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFeesRequest;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFeesResult;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.Charge;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.LogicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.PhysicalFlight;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.exceptions.Detail;
import org.perfomics.flights.business.flydubai.xo.exceptions.InnerExcption;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;

import com.thoughtworks.xstream.XStream;

public class AgencyTransactionFeesTransformer {

	public String transformRequest(Envelope transactionFeesRequest) throws TransactionFeesException {
		try {
			AgencyTransactionFeesValidator feesValidator = new AgencyTransactionFeesValidator();
			feesValidator.validate(transactionFeesRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Fees.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/><soapenv:Body>");
			builder.append("<tem:AssessAgencyTransactionFees>");

			AssessAgencyTransactionFeesRequest atfr = transactionFeesRequest.getBody().getAssessAgencyTransactionFees()
					.getAssessAgencyTransactionFeesRequest();

			builder.append("<tem:AssessAgencyTransactionFeesRequest>");
			builder.append("<rad:SecurityGUID>");
			builder.append(atfr.getSecurityGUID());
			builder.append("</rad:SecurityGUID>");

			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			CarrierCode ccode = atfr.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			builder.append("<rad1:PaymentMethod>");
			builder.append("INVC");
			builder.append("</rad1:PaymentMethod>");
			builder.append("<rad1:AgencyCurrency>");
			builder.append(atfr.getAgencyCurrency());
			builder.append("</rad1:AgencyCurrency>");

			builder.append("</tem:AssessAgencyTransactionFeesRequest></tem:AssessAgencyTransactionFees>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new TransactionFeesException(ire);
		} catch (Exception e) {
			throw new TransactionFeesException(e);
		}
	}

	public Envelope transformResponse(String xmlRequest) throws TransactionFeesException {
		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		xstream.registerLocalConverter(AssessAgencyTransactionFeesResult.class, "bookDate", new MyDateConverter());
		xstream.registerLocalConverter(AssessAgencyTransactionFeesResult.class, "todaysDate", new MyDateConverter());
		xstream.registerLocalConverter(AssessAgencyTransactionFeesResult.class, "lastModified", new MyDateConverter());
		xstream.registerLocalConverter(AssessAgencyTransactionFeesResult.class, "reservationExpirationDate",
				new MyDateConverter());

		xstream.registerLocalConverter(LogicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "arrivaltime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemBookDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemStartDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemEndDate", new MyDateConverter());

		xstream.registerLocalConverter(PhysicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "arrivaltime", new MyDateConverter());

		xstream.registerLocalConverter(AirlinePerson.class, "dob", new MyDateConverter());
		xstream.registerLocalConverter(AirlinePerson.class, "ticketControlModifiedDate", new MyDateConverter());

		xstream.registerLocalConverter(Charge.class, "billDate", new MyDateConverter());
		xstream.registerLocalConverter(Charge.class, "exchangeRateDate", new MyDateConverter());

		// Parsing the XML response
		Envelope transactionFeesResponse = (Envelope) xstream.fromXML(xmlRequest);

		if (transactionFeesResponse.getBody().getAssessAgencyTransactionFeesResponse() == null) {

			Detail detail = transactionFeesResponse.getBody().getFault().getDetail();
			InnerExcption innerExcption = detail.getExceptionDetail().getInnerException();

			throw new TransactionFeesException(innerExcption.getMessage());
		}
		return transactionFeesResponse;

	}

}

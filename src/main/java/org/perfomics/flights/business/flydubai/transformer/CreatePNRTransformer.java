package org.perfomics.flights.business.flydubai.transformer;

import java.util.List;

import org.perfomics.flights.business.flydubai.converter.MyDateConverter;
import org.perfomics.flights.business.flydubai.exception.CreatePNRException;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.validator.CreatePNRValidator;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AirlinePerson;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.Charge;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.History;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.LogicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.PhysicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ReservationPaymentMap;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.SeatAssignment;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePNRResult;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePnrRequest;
import org.perfomics.flights.business.flydubai.xo.createpnr.ReservationContact;
import org.perfomics.flights.business.flydubai.xo.exceptions.Detail;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.exceptions.InnerExcption;
import org.perfomics.flights.business.flydubai.xo.processpnr.ReservationInfo;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;

import com.thoughtworks.xstream.XStream;

public class CreatePNRTransformer {

	public String transformRequest(Envelope createPNRRequest) throws CreatePNRException {

		try {
			CreatePNRValidator createPnrValidator = new CreatePNRValidator();
			createPnrValidator.validate(createPNRRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/>");
			builder.append("<soapenv:Body>");

			builder.append("<tem:CreatePNR>");
			builder.append("<tem:CreatePnrRequest>");

			CreatePnrRequest cpr = createPNRRequest.getBody().getCreatePNR().getCreatePnrRequest();
			builder.append("<rad:SecurityGUID>");
			builder.append(cpr.getSecurityGUID());
			builder.append("</rad:SecurityGUID>");

			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			CarrierCode ccode = cpr.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			builder.append("<rad1:ActionType>");
			builder.append(cpr.getActionType());
			builder.append("</rad1:ActionType>");

			ReservationInfo rinfo = cpr.getReservationInfo();

			String confirmationNumber = "";
			if (rinfo != null) {
				confirmationNumber = rinfo.getConfirmationNumber();
			}

			builder.append("<rad1:ReservationInfo>");
			builder.append("<rad:SeriesNumber>299</rad:SeriesNumber>");
			builder.append("<rad:ConfirmationNumber>");
			builder.append(confirmationNumber);
			builder.append("</rad:ConfirmationNumber></rad1:ReservationInfo>");

			builder.append("</tem:CreatePnrRequest></tem:CreatePNR>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new CreatePNRException(ire);
		} catch (Exception e) {
			throw new CreatePNRException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws CreatePNRException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		xstream.registerLocalConverter(CreatePNRResult.class, "bookDate", new MyDateConverter());
		xstream.registerLocalConverter(CreatePNRResult.class, "todaysDate", new MyDateConverter());
		xstream.registerLocalConverter(CreatePNRResult.class, "lastModified", new MyDateConverter());
		xstream.registerLocalConverter(CreatePNRResult.class, "reservationExpirationDate", new MyDateConverter());

		xstream.registerLocalConverter(LogicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "arrivaltime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemBookDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemStartDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemEndDate", new MyDateConverter());

		xstream.registerLocalConverter(PhysicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "arrivaltime", new MyDateConverter());

		xstream.registerLocalConverter(AirlinePerson.class, "dob", new MyDateConverter());
		xstream.registerLocalConverter(AirlinePerson.class, "ticketControlModifiedDate", new MyDateConverter());

		xstream.registerLocalConverter(Charge.class, "billDate", new MyDateConverter());
		xstream.registerLocalConverter(Charge.class, "exchangeRateDate", new MyDateConverter());

		xstream.registerLocalConverter(ReservationPaymentMap.class, "datePaid", new MyDateConverter());

		xstream.registerLocalConverter(SeatAssignment.class, "actualDepartureDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "checkInDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "lastModifiedDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "frequentFlyerNumberLAstModifiedDate",
				new MyDateConverter());

		xstream.registerLocalConverter(History.class, "activityDate", new MyDateConverter());

		xstream.registerLocalConverter(ReservationContact.class, "dob", new MyDateConverter());

		// Parsing the XML response
		Envelope createPNRResponse = (Envelope) xstream.fromXML(xmlRequest);

		CreatePNRResult cpresult = createPNRResponse.getBody().getCreatePNRResponse().getCreatePNRResult();

		List<ExceptionInformation> exceptionInformationList = cpresult.getExceptions()
				.getExceptionExceptionInformationList();

		for (ExceptionInformation exceptionInformation : exceptionInformationList) {
			if (exceptionInformation.getExceptionCode() != 0) {

				throw new CreatePNRException(exceptionInformation.getExceptionSource() + ":"
						+ exceptionInformation.getExceptionDescription());
			}
		}
		if (createPNRResponse.getBody().getCreatePNRResponse() == null) {

			Detail detail = createPNRResponse.getBody().getFault().getDetail();
			InnerExcption innerExcption = detail.getExceptionDetail().getInnerException();

			throw new CreatePNRException(innerExcption.getMessage());
		}

		return createPNRResponse;

	}

}

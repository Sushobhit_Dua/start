package org.perfomics.flights.business.flydubai.transformer;

import java.util.Date;
import java.util.List;

import org.perfomics.flights.business.flydubai.converter.MyDateConverter;
import org.perfomics.flights.business.flydubai.exception.FareQuoteException;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.validator.FareQuoteValidator;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.exceptions.Detail;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.exceptions.InnerExcption;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteDetail;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteRequestInfo;
import org.perfomics.flights.business.flydubai.xo.farequote.FlightLegDetail;
import org.perfomics.flights.business.flydubai.xo.farequote.FlightSegment;
import org.perfomics.flights.business.flydubai.xo.farequote.LegDetail;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuoteRequest;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuoteResult;
import org.perfomics.flights.business.flydubai.xo.farequote.SegmentDetail;
import org.perfomics.flights.business.flydubai.xo.farequote.TaxDetail;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;

public class FareQuoteTransformer {

	public String transformRequest(Envelope fareQuoteRequest) throws FareQuoteException {

		try {
			FareQuoteValidator fareValidator = new FareQuoteValidator();
			fareValidator.validate(fareQuoteRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Pricing.Request.FareQuote");
			builder.append("\">");
			builder.append("<soapenv:Header/>");
			builder.append("<soapenv:Body>");
			builder.append("<tem:RetrieveFareQuote><tem:RetrieveFareQuoteRequest>");

			RetrieveFareQuoteRequest rfqr = fareQuoteRequest.getBody().getRetrieveFareQuote()
					.getRetrieveFareQuoteRequest();
			builder.append("<rad:SecurityGUID>");
			builder.append(rfqr.getSecurityGUID());
			builder.append("</rad:SecurityGUID>");

			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			CarrierCode ccode = rfqr.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			// builder.append(<rad:HistoricUserName>);
			// builder.append(rfqr.getHistoricUserName());
			// builder.append(</rad:HistoricUserName>);

			builder.append("<rad1:CurrencyOfFareQuote>");
			builder.append(rfqr.getCurrencyOfFareQuote());
			builder.append("</rad1:CurrencyOfFareQuote>");
			builder.append("<rad1:PromotionalCode>");
			builder.append(rfqr.getPromotionalCode());
			builder.append("</rad1:PromotionalCode>");
			builder.append("<rad1:IataNumberOfRequestor>");
			builder.append(rfqr.getIataNumberRequestor());
			builder.append("</rad1:IataNumberOfRequestor>");
			builder.append("<rad1:CorporationID>");
			builder.append(rfqr.getCorporationID());
			builder.append("</rad1:CorporationID>");
			builder.append("<rad1:FareFilterMethod>");
			builder.append("NoCombinabilityRoundtripLowestFarePerFareType");
			builder.append("</rad1:FareFilterMethod>");
			builder.append("<rad1:FareGroupMethod>");
			builder.append("WebFareTypes");
			builder.append("</rad1:FareGroupMethod>");
			builder.append("<rad1:InventoryFilterMethod>");
			builder.append("Available");
			builder.append("</rad1:InventoryFilterMethod>");

			FareQuoteDetail fareQuoteList = rfqr.getFareQuoteDetails().getFaredetaillist().get(0);
			int value = fareQuoteList.getReturnOrOneWay();

			DateConverter dateconverter = new DateConverter("yyyy-MM-dd", new String[] {});
			Date departDate = fareQuoteList.getDepartDate();
			Date returnDate = fareQuoteList.getReturnDate();

			builder.append("<rad1:FareQuoteDetails>");
			if (value == 1 || value == 2) {
				builder.append("<rad1:FareQuoteDetail>");
				builder.append("<rad1:Origin>");
				builder.append(fareQuoteList.getOrigin());
				builder.append("</rad1:Origin>");
				builder.append("<rad1:Destination>");
				builder.append(fareQuoteList.getDestination());
				builder.append("</rad1:Destination>");
				builder.append("<rad1:UseAirportsNotMetroGroups>");
				builder.append(fareQuoteList.isAirportsNotMetroGroups());
				builder.append("</rad1:UseAirportsNotMetroGroups>");
				builder.append("<rad1:DateOfDeparture>");
				builder.append(dateconverter.toString(departDate));
				builder.append("</rad1:DateOfDeparture>");
				builder.append("<rad1:FareTypeCategory>");
				builder.append(fareQuoteList.getFareTypeCategory());
				builder.append("</rad1:FareTypeCategory>");
				builder.append("<rad1:FareClass>");
				//builder.append(fareQuoteList.getFareClass());
				builder.append("</rad1:FareClass>");
				builder.append("<rad1:FareBasisCode>");
				// builder.append(fareQuoteList.getFareBasisCode());
				builder.append("</rad1:FareBasisCode>");
				builder.append("<rad1:Cabin>");
				builder.append(fareQuoteList.getCabin());
				builder.append("</rad1:Cabin>");
				builder.append("<rad1:LFID>");
				builder.append(fareQuoteList.getLfID());
				builder.append("</rad1:LFID>");
				builder.append("<rad1:OperatingCarrierCode>");
				builder.append(fareQuoteList.getOperatingCarrierCode());
				builder.append("</rad1:OperatingCarrierCode>");
				builder.append("<rad1:MarketingCarrierCode>");
				builder.append(fareQuoteList.getMarketingCarrierCode());
				builder.append("</rad1:MarketingCarrierCode>");
				builder.append("<rad1:NumberOfDaysBefore>");
				builder.append("0");
				builder.append("</rad1:NumberOfDaysBefore>");
				builder.append("<rad1:NumberOfDaysAfter>");
				builder.append("0");
				builder.append("</rad1:NumberOfDaysAfter>");
				builder.append("<rad1:LanguageCode>");
				builder.append(fareQuoteList.getLanguageCode());
				builder.append("</rad1:LanguageCode>");
				builder.append("<rad1:TicketPackageID>");
				builder.append("1");
				builder.append("</rad1:TicketPackageID>");

				FareQuoteRequestInfo fareInfolList = fareQuoteList.getFareQuoteRequestInfos().getFareinfolist().get(0);
				int adult = fareInfolList.getNumberOfAdult();
				int child = fareInfolList.getNumberOfChild();
				int infant = fareInfolList.getNumberOfInfant();

				builder.append("<rad1:FareQuoteRequestInfos>");

				if (adult > 0) {

					builder.append("<rad1:FareQuoteRequestInfo>");
					builder.append("<rad1:PassengerTypeID>1</rad1:PassengerTypeID>");
					builder.append("<rad1:TotalSeatsRequired>");
					builder.append(adult);
					builder.append("</rad1:TotalSeatsRequired></rad1:FareQuoteRequestInfo>");
				}
				if (child > 0) {

					builder.append("<rad1:FareQuoteRequestInfo>");
					builder.append("<rad1:PassengerTypeID>6</rad1:PassengerTypeID>");
					builder.append("<rad1:TotalSeatsRequired>");
					builder.append(child);
					builder.append("</rad1:TotalSeatsRequired></rad1:FareQuoteRequestInfo>");
				}
				if (infant > 0) {

					builder.append("<rad1:FareQuoteRequestInfo>");
					builder.append("<rad1:PassengerTypeID>5</rad1:PassengerTypeID>");
					builder.append("<rad1:TotalSeatsRequired>");
					builder.append(infant);
					builder.append("</rad1:TotalSeatsRequired></rad1:FareQuoteRequestInfo>");
				}

				builder.append("</rad1:FareQuoteRequestInfos></rad1:FareQuoteDetail>");
			}
			if (value == 2) {
				builder.append("<rad1:FareQuoteDetail>");
				builder.append("<rad1:Origin>");
				builder.append(fareQuoteList.getDestination());
				builder.append("</rad1:Origin>");
				builder.append("<rad1:Destination>");
				builder.append(fareQuoteList.getOrigin());
				builder.append("</rad1:Destination>");
				builder.append("<rad1:UseAirportsNotMetroGroups>");
				builder.append(fareQuoteList.isAirportsNotMetroGroups());
				builder.append("</rad1:UseAirportsNotMetroGroups>");
				builder.append("<rad1:DateOfDeparture>");
				builder.append(dateconverter.toString(returnDate));
				builder.append("</rad1:DateOfDeparture>");
				builder.append("<rad1:FareTypeCategory>");
				builder.append(fareQuoteList.getFareTypeCategory());
				builder.append("</rad1:FareTypeCategory>");
				builder.append("<rad1:FareClass>");
				//builder.append(fareQuoteList.getFareClass());
				builder.append("</rad1:FareClass>");
				builder.append("<rad1:FareBasisCode>");
				//builder.append(fareQuoteList.getFareBasisCode());
				builder.append("</rad1:FareBasisCode>");
				builder.append("<rad1:Cabin>");
				builder.append(fareQuoteList.getCabin());
				builder.append("</rad1:Cabin>");
				builder.append("<rad1:LFID>");
				builder.append(fareQuoteList.getLfID());
				builder.append("</rad1:LFID>");
				builder.append("<rad1:OperatingCarrierCode>");
				builder.append(fareQuoteList.getOperatingCarrierCode());
				builder.append("</rad1:OperatingCarrierCode>");
				builder.append("<rad1:MarketingCarrierCode>");
				builder.append(fareQuoteList.getMarketingCarrierCode());
				builder.append("</rad1:MarketingCarrierCode>");
				builder.append("<rad1:NumberOfDaysBefore>");
				builder.append(fareQuoteList.getNumberOfDaysBefore());
				builder.append("</rad1:NumberOfDaysBefore>");
				builder.append("<rad1:NumberOfDaysAfter>");
				builder.append(fareQuoteList.getNumberOfDaysAfter());
				builder.append("</rad1:NumberOfDaysAfter>");
				builder.append("<rad1:LanguageCode>");
				builder.append(fareQuoteList.getLanguageCode());
				builder.append("</rad1:LanguageCode>");
				builder.append("<rad1:TicketPackageID>");
				builder.append(fareQuoteList.getTicketPackageID());
				builder.append("</rad1:TicketPackageID>");

				FareQuoteRequestInfo fareInfolList = fareQuoteList.getFareQuoteRequestInfos().getFareinfolist().get(0);
				int adult = fareInfolList.getNumberOfAdult();
				int child = fareInfolList.getNumberOfChild();
				int infant = fareInfolList.getNumberOfInfant();

				builder.append("<rad1:FareQuoteRequestInfos>");

				if (adult > 0) {

					builder.append("<rad1:FareQuoteRequestInfo>");
					builder.append("<rad1:PassengerTypeID>1</rad1:PassengerTypeID>");
					builder.append("<rad1:TotalSeatsRequired>");
					builder.append(adult);
					builder.append("</rad1:TotalSeatsRequired></rad1:FareQuoteRequestInfo>");
				}
				if (child > 0) {

					builder.append("<rad1:FareQuoteRequestInfo>");
					builder.append("<rad1:PassengerTypeID>6</rad1:PassengerTypeID>");
					builder.append("<rad1:TotalSeatsRequired>");
					builder.append(child);
					builder.append("</rad1:TotalSeatsRequired></rad1:FareQuoteRequestInfo>");
				}
				if (infant > 0) {

					builder.append("<rad1:FareQuoteRequestInfo>");
					builder.append("<rad1:PassengerTypeID>5</rad1:PassengerTypeID>");
					builder.append("<rad1:TotalSeatsRequired>");
					builder.append(infant);
					builder.append("</rad1:TotalSeatsRequired></rad1:FareQuoteRequestInfo>");
				}

				builder.append("</rad1:FareQuoteRequestInfos></rad1:FareQuoteDetail>");
			}
			builder.append("</rad1:FareQuoteDetails>");
			builder.append("</tem:RetrieveFareQuoteRequest></tem:RetrieveFareQuote>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new FareQuoteException(ire);
		} catch (Exception e) {
			throw new FareQuoteException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws FareQuoteException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		xstream.registerLocalConverter(FlightSegment.class, "departDate", new MyDateConverter());
		xstream.registerLocalConverter(FlightSegment.class, "arrivalDate", new MyDateConverter());
		xstream.registerLocalConverter(FlightLegDetail.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(LegDetail.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(LegDetail.class, "arrivalDate", new MyDateConverter());

		xstream.registerLocalConverter(SegmentDetail.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(SegmentDetail.class, "arrivalDate", new MyDateConverter());
		xstream.registerLocalConverter(TaxDetail.class, "exchangeDate", new MyDateConverter());

		Envelope fareQuoteResponse = (Envelope) xstream.fromXML(xmlRequest);

		if (fareQuoteResponse.getBody().getRetrieveFareQuoteResponse() == null) {

			Detail detail = fareQuoteResponse.getBody().getFault().getDetail();
			InnerExcption innerExcption = detail.getExceptionDetail().getInnerException();

			throw new FareQuoteException(innerExcption.getMessage());
		}

		RetrieveFareQuoteResult rfqResult = fareQuoteResponse.getBody().getRetrieveFareQuoteResponse()
				.getRetrieveFareQuoteResult();

		List<ExceptionInformation> exceptionInformationList = rfqResult.getExceptions()
				.getExceptionExceptionInformationList();

		for (ExceptionInformation exceptionInformation : exceptionInformationList) {
			if (exceptionInformation.getExceptionCode() != 0) {

				throw new FareQuoteException(exceptionInformation.getExceptionSource() + ":"
						+ exceptionInformation.getExceptionDescription());
			}
		}

		List<FlightSegment> flightSegmentList = rfqResult.getFlightSegments().getFlightlist();

		try {
			flightSegmentList.isEmpty();

		} catch (NullPointerException e) {
			throw new FareQuoteException("Sorry, there are no flights found for the selected date.");
		}

		return fareQuoteResponse;
	}

}

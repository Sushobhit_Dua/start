package org.perfomics.flights.business.flydubai.transformer;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.perfomics.flights.business.flydubai.converter.MyDateConverter;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.exception.ProcessPNRException;
import org.perfomics.flights.business.flydubai.validator.ProcessPNRValidator;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AirlinePerson;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.Charge;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfo;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.History;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.LogicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.PhysicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ReservationPaymentMap;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.SeatAssignment;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.createpnr.ReservationContact;
import org.perfomics.flights.business.flydubai.xo.exceptions.Detail;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.exceptions.InnerExcption;
import org.perfomics.flights.business.flydubai.xo.processpnr.PNRPaymentRequest;
import org.perfomics.flights.business.flydubai.xo.processpnr.Payor;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPayment1;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPaymentResult;
import org.perfomics.flights.business.flydubai.xo.processpnr.ReservationInfo;
import org.perfomics.flights.business.flydubai.xo.processpnr.TransactionInfo;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Address;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;

public class ProcessPNRTransformer {

	public String transformRequest(Envelope processPnrRequest) throws ProcessPNRException {

		try {
			ProcessPNRValidator processValidator = new ProcessPNRValidator();
			processValidator.validate(processPnrRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Fulfillment.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad2=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/><soapenv:Body>");
			builder.append("<tem:ProcessPNRPayment>");

			PNRPaymentRequest pnrPaymentRequest = processPnrRequest.getBody().getProcessPNRPayment()
					.getPnrPaymentRequest();

			TransactionInfo transactionInfo = pnrPaymentRequest.getTransactionInfo();

			builder.append("<tem:PNRPaymentRequest>");
			builder.append("<rad:TransactionInfo>");
			builder.append("<rad1:SecurityGUID>");
			builder.append(transactionInfo.getSecurityGUID());
			builder.append("</rad1:SecurityGUID>");

			builder.append("<rad1:CarrierCodes>");
			builder.append("<rad1:CarrierCode><rad1:AccessibleCarrierCode>");

			CarrierCode ccode = transactionInfo.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad1:AccessibleCarrierCode></rad1:CarrierCode>");
			builder.append("</rad1:CarrierCodes>");

			builder.append("</rad:TransactionInfo>");

			ReservationInfo rinfo = pnrPaymentRequest.getReservationInfo();

			String confirmationNumber = "";
			if (rinfo != null) {
				confirmationNumber = rinfo.getConfirmationNumber();
			}

			builder.append("<rad:ReservationInfo>");
			builder.append("<rad1:SeriesNumber>299</rad1:SeriesNumber>");
			builder.append("<rad1:ConfirmationNumber>");
			builder.append(confirmationNumber);
			builder.append("</rad1:ConfirmationNumber></rad:ReservationInfo>");

			ProcessPNRPayment1 processPnrPayment1 = pnrPaymentRequest.getPnrPayments().getProcessPNRPayment1();

			DateConverter dateconverter = new DateConverter("yyyy-MM-dd", new String[] {});
			Date datePaid = new Date();

			builder.append("<rad:PNRPayments><rad:ProcessPNRPayment>");
			builder.append("<rad:BaseAmount>");
			builder.append(processPnrPayment1.getBaseAmount());
			builder.append("</rad:BaseAmount>");
			builder.append("<rad:BaseCurrency>");
			builder.append(processPnrPayment1.getBaseCurrency());
			builder.append("</rad:BaseCurrency>");
			builder.append("<rad:CardHolder></rad:CardHolder>");
			builder.append("<rad:CardNumber></rad:CardNumber>");
			builder.append("<rad:CheckNumber>1234</rad:CheckNumber>");
			builder.append("<rad:CurrencyPaid>INR</rad:CurrencyPaid>");
			builder.append("<rad:CVCode></rad:CVCode>");
			builder.append("<rad:DatePaid>");
			builder.append(dateconverter.toString(datePaid));
			builder.append("</rad:DatePaid>");
			builder.append("<rad:DocumentReceivedBy>DESH TEST</rad:DocumentReceivedBy>");

			datePaid = DateUtils.addYears(datePaid, 1);

			builder.append("<rad:ExpirationDate>");
			builder.append(dateconverter.toString(datePaid));
			builder.append("</rad:ExpirationDate>");

			datePaid = new Date();

			builder.append("<rad:ExchangeRate>1</rad:ExchangeRate>");
			builder.append("<rad:ExchangeRateDate>");
			builder.append(dateconverter.toString(datePaid));
			builder.append("</rad:ExchangeRateDate>");
			builder.append("<rad:FFNumber>11</rad:FFNumber>");
			builder.append("<rad:PaymentComment>test comments</rad:PaymentComment>");
			builder.append("<rad:PaymentAmount>");
			builder.append(processPnrPayment1.getPaymentAmount());
			builder.append("</rad:PaymentAmount>");
			builder.append("<rad:PaymentMethod>INVC</rad:PaymentMethod>");
			builder.append("<rad:Reference>Test</rad:Reference>");
			builder.append("<rad:TerminalID>1</rad:TerminalID>");
			builder.append("<rad:UserData></rad:UserData>");
			builder.append("<rad:UserID></rad:UserID>");
			builder.append("<rad:IataNumber>");
			builder.append(processPnrPayment1.getIataNumber());
			builder.append("</rad:IataNumber>");
			builder.append("<rad:ValueCode></rad:ValueCode>");
			builder.append("<rad:VoucherNumber>-2147483648</rad:VoucherNumber>");
			builder.append("<rad:IsTACreditCard>false</rad:IsTACreditCard>");
			builder.append("<rad:GcxID>1</rad:GcxID>");
			builder.append("<rad:GcxOptOption>1</rad:GcxOptOption>");
			builder.append("<rad:OriginalCurrency>");
			builder.append(processPnrPayment1.getOriginalCurrency());
			builder.append("</rad:OriginalCurrency>");
			builder.append("<rad:OriginalAmount>");
			builder.append(processPnrPayment1.getOriginalAmount());
			builder.append("</rad:OriginalAmount>");
			builder.append("<rad:TransactionStatus>APPROVED</rad:TransactionStatus>");
			builder.append("<rad:AuthorizationCode></rad:AuthorizationCode>");
			builder.append("<rad:PaymentReference>na</rad:PaymentReference>");
			builder.append("<rad:ResponseMessage>na</rad:ResponseMessage>");
			builder.append("<rad:CardCurrency>INR</rad:CardCurrency>");
			builder.append("<rad:BillingCountry>IN</rad:BillingCountry>");
			builder.append("<rad:FingerPrintingSessionID></rad:FingerPrintingSessionID>");

			Payor payor = processPnrPayment1.getPayor();

			Date dob = payor.getDob();

			// To get the Current Year
			int currentyear = Calendar.getInstance().get(Calendar.YEAR);

			int personyear = DateUtils.toCalendar(dob).get(Calendar.YEAR);
			int age = currentyear - personyear;
			String gender = null;
			if (payor.getGender().equals("M")) {
				gender = "Male";
			}

			builder.append("<rad:Payor>");
			builder.append("<rad2:PersonOrgID>");
			builder.append(payor.getPersonOrgID());
			builder.append("</rad2:PersonOrgID>");
			builder.append("<rad2:FirstName>");
			builder.append(payor.getFirstName());
			builder.append("</rad2:FirstName>");
			builder.append("<rad2:LastName>");
			builder.append(payor.getLastName());
			builder.append("</rad2:LastName>");
			builder.append("<rad2:MiddleName>");
			builder.append(payor.getMiddleName());
			builder.append("</rad2:MiddleName>");
			builder.append("<rad2:Age>");
			builder.append(age);
			builder.append("</rad2:Age>");
			builder.append("<rad2:DOB>");
			builder.append(dateconverter.toString(dob));
			builder.append("</rad2:DOB>");
			builder.append("<rad2:Gender>");
			builder.append(gender);
			builder.append("</rad2:Gender>");
			builder.append("<rad2:Title>");
			builder.append(payor.getTitle());
			builder.append("</rad2:Title>");
			builder.append("<rad2:NationalityLaguageID>-214</rad2:NationalityLaguageID>");
			builder.append("<rad2:RelationType>Self</rad2:RelationType>");
			builder.append("<rad2:WBCID>1</rad2:WBCID>");
			builder.append("<rad2:PTCID>1</rad2:PTCID>");
			builder.append("<rad2:PTC>1</rad2:PTC>");
			builder.append("<rad2:TravelsWithPersonOrgID>");
			builder.append(payor.getPersonOrgID());
			builder.append("</rad2:TravelsWithPersonOrgID>");
			builder.append("<rad2:RedressNumber>na</rad2:RedressNumber>");
			builder.append("<rad2:KnownTravelerNumber>na</rad2:KnownTravelerNumber>");
			builder.append("<rad2:MarketingOptIn>false</rad2:MarketingOptIn>");
			builder.append("<rad2:UseInventory>false</rad2:UseInventory>");

			Address address = payor.getAddress();

			builder.append("<rad2:Address>");
			builder.append("<rad2:Address1>");
			builder.append(address.getAddress1());
			builder.append("</rad2:Address1>");
			builder.append("<rad2:Address2>");
			builder.append(address.getAddress2());
			builder.append("</rad2:Address2>");
			builder.append("<rad2:City>");
			builder.append(address.getCity());
			builder.append("</rad2:City>");
			builder.append("<rad2:State>");
			builder.append(address.getState());
			builder.append("</rad2:State>");
			builder.append("<rad2:Postal>");
			builder.append(address.getPostal());
			builder.append("</rad2:Postal>");
			builder.append("<rad2:Country>");
			builder.append(address.getCountry());
			builder.append("</rad2:Country>");
			builder.append("<rad2:CountryCode>");
			builder.append(address.getCountryCode());
			builder.append("</rad2:CountryCode>");
			builder.append("<rad2:AreaCode>");
			builder.append(address.getAreaCode());
			builder.append("</rad2:AreaCode>");
			builder.append("<rad2:PhoneNumber>");
			builder.append(address.getPhoneNumber());
			builder.append("</rad2:PhoneNumber>");
			builder.append("<rad2:Display>na</rad2:Display></rad2:Address>");

			builder.append("<rad2:Company>TEST</rad2:Company>");
			builder.append("<rad2:Comments></rad2:Comments>");
			builder.append("<rad2:Passport></rad2:Passport>");
			builder.append("<rad2:Nationality></rad2:Nationality>");
			builder.append("<rad2:ProfileId>-214</rad2:ProfileId>");
			builder.append("<rad2:IsPrimaryPassenger>true</rad2:IsPrimaryPassenger>");

			ContactInfo contactinfo = payor.getContactInfos().getContactInfoList().get(0);

			
			builder.append("<rad2:ContactInfos>");
			builder.append("<rad2:ContactInfo>");
			builder.append("<rad2:ContactID>");
			builder.append(payor.getContactID());
			builder.append("</rad2:ContactID>");
			builder.append("<rad2:PersonOrgID>");
			builder.append(payor.getPersonOrgID());
			builder.append("</rad2:PersonOrgID>");
			builder.append("<rad2:ContactField>");
			builder.append(contactinfo.getEmail());
			builder.append("</rad2:ContactField>");

			builder.append("<rad2:ContactType>Email</rad2:ContactType>");
			builder.append("<rad2:Extension></rad2:Extension>");
			builder.append("<rad2:CountryCode>");
			builder.append(address.getCountryCode());
			builder.append("</rad2:CountryCode>");
			builder.append("<rad2:AreaCode>");
			builder.append(address.getAreaCode());
			builder.append("</rad2:AreaCode>");
			builder.append("<rad2:PhoneNumber>");
			builder.append(address.getPhoneNumber());
			builder.append("</rad2:PhoneNumber>");
			builder.append("<rad2:Display>na</rad2:Display>");
			builder.append("<rad2:PreferredContactMethod>true</rad2:PreferredContactMethod>");
			builder.append("</rad2:ContactInfo></rad2:ContactInfos>");

			builder.append("</rad:Payor>");

			builder.append("</rad:ProcessPNRPayment></rad:PNRPayments>");

			builder.append("</tem:PNRPaymentRequest></tem:ProcessPNRPayment>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new ProcessPNRException(ire);
		} catch (Exception e) {
			throw new ProcessPNRException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws ProcessPNRException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		xstream.registerLocalConverter(ProcessPNRPaymentResult.class, "bookDate", new MyDateConverter());
		xstream.registerLocalConverter(ProcessPNRPaymentResult.class, "todaysDate", new MyDateConverter());
		xstream.registerLocalConverter(ProcessPNRPaymentResult.class, "lastModified", new MyDateConverter());
		xstream.registerLocalConverter(ProcessPNRPaymentResult.class, "reservationExpirationDate",
				new MyDateConverter());

		xstream.registerLocalConverter(LogicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "arrivaltime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemBookDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemStartDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemEndDate", new MyDateConverter());

		xstream.registerLocalConverter(PhysicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "arrivaltime", new MyDateConverter());

		xstream.registerLocalConverter(AirlinePerson.class, "dob", new MyDateConverter());
		xstream.registerLocalConverter(AirlinePerson.class, "ticketControlModifiedDate", new MyDateConverter());

		xstream.registerLocalConverter(Charge.class, "billDate", new MyDateConverter());
		xstream.registerLocalConverter(Charge.class, "exchangeRateDate", new MyDateConverter());

		xstream.registerLocalConverter(ReservationPaymentMap.class, "datePaid", new MyDateConverter());

		xstream.registerLocalConverter(SeatAssignment.class, "actualDepartureDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "checkInDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "lastModifiedDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "frequentFlyerNumberLAstModifiedDate",
				new MyDateConverter());

		xstream.registerLocalConverter(History.class, "activityDate", new MyDateConverter());

		xstream.registerLocalConverter(ReservationContact.class, "dob", new MyDateConverter());

		// Parsing the XML response
		Envelope processPnrResponse = (Envelope) xstream.fromXML(xmlRequest);

		if (processPnrResponse.getBody().getProcessPNRPaymentResponse() == null) {

			Detail detail = processPnrResponse.getBody().getFault().getDetail();
			InnerExcption innerExcption = detail.getExceptionDetail().getInnerException();

			throw new ProcessPNRException(innerExcption.getMessage());
		}

		ProcessPNRPaymentResult pppResult = processPnrResponse.getBody().getProcessPNRPaymentResponse()
				.getProcessPNRPaymentResult();

		List<ExceptionInformation> exceptionInformationList = pppResult.getExceptions()
				.getExceptionExceptionInformationList();

		for (ExceptionInformation exceptionInformation : exceptionInformationList) {
			if (exceptionInformation.getExceptionCode() != 0) {

				throw new ProcessPNRException(exceptionInformation.getExceptionSource() + ":"
						+ exceptionInformation.getExceptionDescription());
			}
		}

		return processPnrResponse;

	}

}

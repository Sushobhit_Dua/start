package org.perfomics.flights.business.flydubai.transformer;

import java.util.List;

import org.perfomics.flights.business.flydubai.converter.MyDateConverter;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.exception.RetrievePNRException;
import org.perfomics.flights.business.flydubai.validator.RetrievePNRValidator;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AirlinePerson;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.Charge;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.History;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.LogicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.PhysicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ReservationPaymentMap;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.SeatAssignment;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.createpnr.ReservationContact;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.processpnr.ReservationInfo;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePNRResult;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePnrRequest;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;

import com.thoughtworks.xstream.XStream;

public class RetrievePNRTransformer {

	public String transformRequest(Envelope retrievePnrRequest) throws RetrievePNRException {

		try {
			RetrievePNRValidator retrievePNRValidator = new RetrievePNRValidator();
			retrievePNRValidator.validate(retrievePnrRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/>");
			builder.append("<soapenv:Body>");

			builder.append("<tem:RetrievePNR>");
			builder.append("<tem:RetrievePnrRequest>");

			RetrievePnrRequest rpr = retrievePnrRequest.getBody().getRetrievePNR().getRetrievePnrRequest();

			builder.append("<rad:SecurityGUID>");
			builder.append(rpr.getSecurityGUID());
			builder.append("</rad:SecurityGUID>");

			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			CarrierCode ccode = rpr.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			builder.append("<rad1:ActionType>GetReservation</rad1:ActionType>");

			ReservationInfo rinfo = rpr.getReservationInfo();

			builder.append("<rad1:ReservationInfo>");
			builder.append("<rad:SeriesNumber>299</rad:SeriesNumber>");
			builder.append("<rad:ConfirmationNumber>");
			builder.append(rinfo.getConfirmationNumber());
			builder.append("</rad:ConfirmationNumber></rad1:ReservationInfo>");

			builder.append("</tem:RetrievePnrRequest></tem:RetrievePNR>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new RetrievePNRException(ire);
		} catch (Exception e) {
			throw new RetrievePNRException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws RetrievePNRException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		xstream.registerLocalConverter(RetrievePNRResult.class, "bookDate", new MyDateConverter());
		xstream.registerLocalConverter(RetrievePNRResult.class, "todaysDate", new MyDateConverter());
		xstream.registerLocalConverter(RetrievePNRResult.class, "lastModified", new MyDateConverter());
		xstream.registerLocalConverter(RetrievePNRResult.class, "reservationExpirationDate", new MyDateConverter());

		xstream.registerLocalConverter(LogicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "arrivaltime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemBookDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemStartDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemEndDate", new MyDateConverter());

		xstream.registerLocalConverter(PhysicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "arrivaltime", new MyDateConverter());

		xstream.registerLocalConverter(AirlinePerson.class, "dob", new MyDateConverter());
		xstream.registerLocalConverter(AirlinePerson.class, "ticketControlModifiedDate", new MyDateConverter());

		xstream.registerLocalConverter(Charge.class, "billDate", new MyDateConverter());
		xstream.registerLocalConverter(Charge.class, "exchangeRateDate", new MyDateConverter());

		xstream.registerLocalConverter(ReservationPaymentMap.class, "datePaid", new MyDateConverter());

		xstream.registerLocalConverter(SeatAssignment.class, "actualDepartureDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "checkInDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "lastModifiedDate", new MyDateConverter());
		xstream.registerLocalConverter(SeatAssignment.class, "frequentFlyerNumberLAstModifiedDate",
				new MyDateConverter());

		xstream.registerLocalConverter(History.class, "activityDate", new MyDateConverter());

		xstream.registerLocalConverter(ReservationContact.class, "dob", new MyDateConverter());

		// Parsing the XML response
		Envelope retrievePnrResponse = (Envelope) xstream.fromXML(xmlRequest);

		RetrievePNRResult rpResult = retrievePnrResponse.getBody().getRetrievePNRResponse().getRetrievePNRResult();

		if (rpResult.getLogicalFlightCount() == 0 || rpResult.getActivePassengerCount() == 0) {
			throw new RetrievePNRException("Reservation is Cancelled");

		}
		if (rpResult.getReservationBalance() == 0.0) {
			throw new RetrievePNRException("Reservation is Cancelled");
		}
		
		List<ExceptionInformation> exceptionInformationList = rpResult.getExceptions()
				.getExceptionExceptionInformationList();

		for (ExceptionInformation exceptionInformation : exceptionInformationList) {
			if (exceptionInformation.getExceptionCode() != 0) {

				throw new RetrievePNRException(exceptionInformation.getExceptionSource() + ":"
						+ exceptionInformation.getExceptionDescription());
			}
		}

		
	
		
		return retrievePnrResponse;
	}

}

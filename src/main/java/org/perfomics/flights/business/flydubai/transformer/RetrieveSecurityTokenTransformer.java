package org.perfomics.flights.business.flydubai.transformer;

import java.util.List;

import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.exception.UnableToRetrieveTokenException;
import org.perfomics.flights.business.flydubai.validator.SecurityTokenValidator;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenRequest;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenResult;

import com.thoughtworks.xstream.XStream;

public class RetrieveSecurityTokenTransformer {

	public String transformRequest(Envelope tokenrequest) throws UnableToRetrieveTokenException {
		try {
			SecurityTokenValidator tokenValidator = new SecurityTokenValidator();

			tokenValidator.validate(tokenrequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Security.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/>");
			builder.append("<soapenv:Body><tem:RetrieveSecurityToken>");
			builder.append("<tem:RetrieveSecurityTokenRequest>");
			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			RetrieveSecurityTokenRequest rstr = tokenrequest.getBody().getSecurityToken().getSecurityTokenRequest();
			CarrierCodes ccodes = rstr.getCarrierCodes();

			builder.append(ccodes.getCarrierCode().getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			builder.append("<rad1:LogonID>");
			builder.append(rstr.getLogonId());
			builder.append("</rad1:LogonID>");

			builder.append("<rad1:Password>");
			builder.append(rstr.getPassword());
			builder.append("</rad1:Password>");

			builder.append("</tem:RetrieveSecurityTokenRequest></tem:RetrieveSecurityToken>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new UnableToRetrieveTokenException(ire);
		} catch (Exception e) {
			throw new UnableToRetrieveTokenException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws UnableToRetrieveTokenException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		// Parsing the XML response
		Envelope tokenresponse = (Envelope) xstream.fromXML(xmlRequest);

		RetrieveSecurityTokenResult rstr = tokenresponse.getBody().getSecurityTokenResponse()
				.getRetrieveSecurityTokenResult();
		List<ExceptionInformation> exceptionInformationList = rstr.getExceptions()
				.getExceptionExceptionInformationList();

		for (ExceptionInformation exceptionInformation : exceptionInformationList) {
			if (exceptionInformation.getExceptionCode() != 0) {

				throw new UnableToRetrieveTokenException(exceptionInformation.getExceptionSource() + ":"
						+ exceptionInformation.getExceptionDescription());
			}
		}

		return tokenresponse;

	}

}

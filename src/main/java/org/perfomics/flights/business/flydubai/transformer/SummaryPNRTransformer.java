package org.perfomics.flights.business.flydubai.transformer;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.perfomics.flights.business.flydubai.converter.MyDateConverter;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.exception.SummaryPNRException;
import org.perfomics.flights.business.flydubai.validator.SummaryPNRValidator;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AirlinePerson;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.Charge;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfo;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.LogicalFlight;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.PhysicalFlight;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.createpnr.Payment;
import org.perfomics.flights.business.flydubai.xo.exceptions.Detail;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.exceptions.InnerExcption;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Address;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Person;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Segment;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPNRResult;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPnrRequest;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.basic.DateConverter;

public class SummaryPNRTransformer {

	public String transformRequest(Envelope summaryPnrRequest) throws SummaryPNRException {
		try {
			SummaryPNRValidator summaryValidator = new SummaryPNRValidator();
			summaryValidator.validate(summaryPnrRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/><soapenv:Body>");
			builder.append("<tem:SummaryPNR>");

			SummaryPnrRequest spr = summaryPnrRequest.getBody().getSummaryPNR().getSummaryPnrRequest();

			builder.append("<tem:SummaryPnrRequest>");
			builder.append("<rad:SecurityGUID>");
			builder.append(spr.getSecurityGUID());
			builder.append("</rad:SecurityGUID>");

			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			CarrierCode ccode = spr.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			builder.append("<rad1:ActionType>GetSummary</rad1:ActionType>");
			builder.append("<rad1:ReservationInfo>");
			builder.append("<rad:SeriesNumber>299</rad:SeriesNumber>");
			builder.append("<rad:ConfirmationNumber>");
			// builder.append(RandomStringUtils.randomAlphanumeric(6));
			builder.append("</rad:ConfirmationNumber></rad1:ReservationInfo>");

			builder.append("<rad1:SecurityToken>");
			builder.append(spr.getSecurityToken());
			builder.append("</rad1:SecurityToken>");

			builder.append("<rad1:CarrierCurrency>");
			builder.append(spr.getCarrierCurrency());
			builder.append("</rad1:CarrierCurrency>");
			builder.append("<rad1:DisplayCurrency>");
			builder.append(spr.getDisplayCurrency());
			builder.append("</rad1:DisplayCurrency>");
			builder.append("<rad1:IATANum>");
			builder.append(spr.getIataNum());
			builder.append("</rad1:IATANum>");
			builder.append("<rad1:User></rad1:User>");
			builder.append("<rad1:ReceiptLanguageID>1</rad1:ReceiptLanguageID>");
			builder.append("<rad1:PromoCode>");
			builder.append(spr.getPromoCode());
			builder.append("</rad1:PromoCode>");
			builder.append("<rad1:ExternalBookingID>");
			builder.append("na");
			builder.append("</rad1:ExternalBookingID>");

			Address address = spr.getAddress();

			builder.append("<rad1:Address>");
			builder.append("<rad1:Address1>");
			builder.append(address.getAddress1());
			builder.append("</rad1:Address1>");
			builder.append("<rad1:Address2>");
			builder.append(address.getAddress2());
			builder.append("</rad1:Address2>");
			builder.append("<rad1:City>");
			builder.append(address.getCity());
			builder.append("</rad1:City>");
			builder.append("<rad1:State>");
			builder.append(address.getState());
			builder.append("</rad1:State>");
			builder.append("<rad1:Postal>");
			builder.append(address.getPostal());
			builder.append("</rad1:Postal>");
			builder.append("<rad1:Country>");
			builder.append(address.getCountry());
			builder.append("</rad1:Country>");
			builder.append("<rad1:CountryCode>");
			builder.append(address.getCountryCode());
			builder.append("</rad1:CountryCode>");
			builder.append("<rad1:AreaCode>");
			builder.append(address.getAreaCode());
			builder.append("</rad1:AreaCode>");
			builder.append("<rad1:PhoneNumber>");
			builder.append(address.getPhoneNumber());
			builder.append("</rad1:PhoneNumber>");
			builder.append("<rad1:Display>na</rad1:Display>");
			builder.append("</rad1:Address>");

			ContactInfo contactinfo = spr.getContactInfos().getContactInfoList().get(0);
			String personOrgID = RandomStringUtils.randomNumeric(3);
			String contactID = RandomStringUtils.randomNumeric(4);

			builder.append("<rad1:ContactInfos>");
			builder.append("<rad1:ContactInfo>");
			builder.append("<rad1:ContactID>-");
			builder.append(contactID);
			builder.append("</rad1:ContactID>");
			builder.append("<rad1:PersonOrgID>-");
			builder.append(personOrgID);
			builder.append("</rad1:PersonOrgID>");
			builder.append("<rad1:ContactField>");
			builder.append(address.getPhoneNumber());
			builder.append("</rad1:ContactField>");
			builder.append("<rad1:ContactType>");
			builder.append("HomePhone");
			builder.append("</rad1:ContactType>");
			builder.append("<rad1:Extension></rad1:Extension>");
			builder.append("<rad1:CountryCode>");
			builder.append(address.getCountryCode());
			builder.append("</rad1:CountryCode>");
			builder.append("<rad1:AreaCode>");
			builder.append(address.getAreaCode());
			builder.append("</rad1:AreaCode>");
			builder.append("<rad1:PhoneNumber>");
			builder.append(address.getPhoneNumber());
			builder.append("</rad1:PhoneNumber>");

			builder.append("<rad1:Display>na</rad1:Display>");
			builder.append("<rad1:PreferredContactMethod>false</rad1:PreferredContactMethod>");
			builder.append("</rad1:ContactInfo>");
			builder.append("</rad1:ContactInfos>");

			List<Person> personList = spr.getPassengers().getPersonList();
			builder.append("<rad1:Passengers>");

			DateConverter dateconverter = new DateConverter("yyyy-MM-dd", new String[] {});

			// To get the Current Year
			int currentyear = Calendar.getInstance().get(Calendar.YEAR);
			int personyear = 0, age = 0, ptcid = 0;
			boolean primaryPassenger = true;
			String duplicatePersonOrgID = personOrgID;
			String duplicatecontactID = contactID;

			for (Person person : personList) {
				Date dob = person.getDob();
				personyear = DateUtils.toCalendar(dob).get(Calendar.YEAR);
				age = currentyear - personyear;

				if (age <= 2) {
					ptcid = 5;
				} else if (age <= 13) {
					ptcid = 6;
				} else {
					ptcid = 1;
				}

				if (age == 0) {
					age = 1;
				}

				builder.append("<rad1:Person>");
				builder.append("<rad1:PersonOrgID>-");
				builder.append(duplicatePersonOrgID);
				builder.append("</rad1:PersonOrgID>");
				builder.append("<rad1:FirstName>");
				builder.append(person.getFirstName());
				builder.append("</rad1:FirstName>");
				builder.append("<rad1:LastName>");
				builder.append(person.getLastName());
				builder.append("</rad1:LastName>");
				builder.append("<rad1:MiddleName>");
				builder.append(person.getMiddleName());
				builder.append("</rad1:MiddleName>");
				builder.append("<rad1:Age>");
				builder.append(age);
				builder.append("</rad1:Age>");
				builder.append("<rad1:DOB>");
				builder.append(dateconverter.toString(dob));
				builder.append("</rad1:DOB>");
				builder.append("<rad1:Gender>");
				builder.append(person.getGender());
				builder.append("</rad1:Gender>");
				builder.append("<rad1:Title>");
				builder.append(person.getTitle());
				builder.append("</rad1:Title>");
				builder.append("<rad1:NationalityLaguageID>");
				builder.append("-214");
				builder.append("</rad1:NationalityLaguageID>");
				builder.append("<rad1:RelationType>");
				builder.append("Self");
				builder.append("</rad1:RelationType>");
				builder.append("<rad1:WBCID>");
				builder.append(ptcid);
				builder.append("</rad1:WBCID>");
				builder.append("<rad1:PTCID>");
				builder.append(ptcid);
				builder.append("</rad1:PTCID>");
				builder.append("<rad1:PTC>");
				builder.append(ptcid);
				builder.append("</rad1:PTC>");
				builder.append("<rad1:TravelsWithPersonOrgID>-");
				builder.append(personOrgID);
				builder.append("</rad1:TravelsWithPersonOrgID>");
				builder.append("<rad1:RedressNumber>na</rad1:RedressNumber>");
				builder.append("<rad1:KnownTravelerNumber>na</rad1:KnownTravelerNumber>");
				builder.append("<rad1:MarketingOptIn>true</rad1:MarketingOptIn>");
				builder.append("<rad1:UseInventory>false</rad1:UseInventory>");

				builder.append("<rad1:Address>");
				builder.append("<rad1:Address1>");
				builder.append(address.getAddress1());
				builder.append("</rad1:Address1>");
				builder.append("<rad1:Address2>");
				builder.append(address.getAddress2());
				builder.append("</rad1:Address2>");
				builder.append("<rad1:City>");
				builder.append(address.getCity());
				builder.append("</rad1:City>");
				builder.append("<rad1:State>");
				builder.append(address.getState());
				builder.append("</rad1:State>");
				builder.append("<rad1:Postal>");
				builder.append(address.getPostal());
				builder.append("</rad1:Postal>");
				builder.append("<rad1:Country>");
				builder.append(address.getCountry());
				builder.append("</rad1:Country>");
				builder.append("<rad1:CountryCode>");
				builder.append(address.getCountryCode());
				builder.append("</rad1:CountryCode>");
				builder.append("<rad1:AreaCode>");
				builder.append(address.getAreaCode());
				builder.append("</rad1:AreaCode>");
				builder.append("<rad1:PhoneNumber>");
				builder.append(address.getPhoneNumber());
				builder.append("</rad1:PhoneNumber>");
				builder.append("<rad1:Display>na</rad1:Display>");
				builder.append("</rad1:Address>");

				builder.append("<rad1:Company>Abc</rad1:Company>");
				builder.append("<rad1:Comments>comments</rad1:Comments>");
				builder.append("<rad1:Passport>123456</rad1:Passport>");
				builder.append("<rad1:Nationality></rad1:Nationality>");
				builder.append("<rad1:ProfileId>-2147483648</rad1:ProfileId>");
				builder.append("<rad1:IsPrimaryPassenger>");
				builder.append(primaryPassenger);
				builder.append("</rad1:IsPrimaryPassenger>");

				builder.append("<rad1:ContactInfos>");

				// Email

				builder.append("<rad1:ContactInfo>");
				builder.append("<rad1:ContactID>-");
				builder.append(duplicatecontactID);
				builder.append("</rad1:ContactID>");
				builder.append("<rad1:PersonOrgID>-");
				builder.append(duplicatePersonOrgID);
				builder.append("</rad1:PersonOrgID>");
				builder.append("<rad1:ContactField>");
				builder.append(contactinfo.getEmail());
				builder.append("</rad1:ContactField>");
				builder.append("<rad1:ContactType>Email</rad1:ContactType>");
				builder.append("<rad1:Extension></rad1:Extension>");
				builder.append("<rad1:CountryCode>");
				builder.append(address.getCountryCode());
				builder.append("</rad1:CountryCode>");
				builder.append("<rad1:AreaCode>");
				builder.append(address.getAreaCode());
				builder.append("</rad1:AreaCode>");
				builder.append("<rad1:PhoneNumber>");
				builder.append(address.getPhoneNumber());
				builder.append("</rad1:PhoneNumber>");
				builder.append("<rad1:Display>na</rad1:Display>");
				builder.append("<rad1:PreferredContactMethod>true</rad1:PreferredContactMethod>");
				builder.append("</rad1:ContactInfo>");

				// Home Phone
				duplicatecontactID = RandomStringUtils.randomNumeric(4);

				builder.append("<rad1:ContactInfo>");
				builder.append("<rad1:ContactID>-");
				builder.append(duplicatecontactID);
				builder.append("</rad1:ContactID>");
				builder.append("<rad1:PersonOrgID>-");
				builder.append(duplicatePersonOrgID);
				builder.append("</rad1:PersonOrgID>");
				builder.append("<rad1:ContactField>00");
				builder.append(contactinfo.getWorkPhone());
				builder.append("</rad1:ContactField>");
				builder.append("<rad1:ContactType>HomePhone</rad1:ContactType>");
				builder.append("<rad1:Extension></rad1:Extension>");
				builder.append("<rad1:CountryCode>");
				builder.append(address.getCountryCode());
				builder.append("</rad1:CountryCode>");
				builder.append("<rad1:AreaCode>");
				builder.append(address.getAreaCode());
				builder.append("</rad1:AreaCode>");
				builder.append("<rad1:PhoneNumber>");
				builder.append(address.getPhoneNumber());
				builder.append("</rad1:PhoneNumber>");
				builder.append("<rad1:Display>na</rad1:Display>");
				builder.append("<rad1:PreferredContactMethod>false</rad1:PreferredContactMethod>");
				builder.append("</rad1:ContactInfo>");

				// WorkPhone

				duplicatecontactID = RandomStringUtils.randomNumeric(4);

				builder.append("<rad1:ContactInfo>");
				builder.append("<rad1:ContactID>-");
				builder.append(duplicatecontactID);
				builder.append("</rad1:ContactID>");
				builder.append("<rad1:PersonOrgID>-");
				builder.append(duplicatePersonOrgID);
				builder.append("</rad1:PersonOrgID>");
				builder.append("<rad1:ContactField>");
				builder.append(contactinfo.getWorkPhone());
				builder.append("</rad1:ContactField>");
				builder.append("<rad1:ContactType>WorkPhone</rad1:ContactType>");
				builder.append("<rad1:Extension></rad1:Extension>");
				builder.append("<rad1:CountryCode>");
				builder.append(address.getCountryCode());
				builder.append("</rad1:CountryCode>");
				builder.append("<rad1:AreaCode>");
				builder.append(address.getAreaCode());
				builder.append("</rad1:AreaCode>");
				builder.append("<rad1:PhoneNumber>");
				builder.append(address.getPhoneNumber());
				builder.append("</rad1:PhoneNumber>");
				builder.append("<rad1:Display>na</rad1:Display>");
				builder.append("<rad1:PreferredContactMethod>false</rad1:PreferredContactMethod>");
				builder.append("</rad1:ContactInfo>");

				builder.append("</rad1:ContactInfos>");
				builder.append("</rad1:Person>");

				primaryPassenger = false;
				duplicatePersonOrgID = RandomStringUtils.randomNumeric(5);
				duplicatecontactID = RandomStringUtils.randomNumeric(4);
			}
			builder.append("</rad1:Passengers>");

			List<Segment> s = spr.getSegments().getSegmentList();
			builder.append("<rad1:Segments>");

			for (Segment segment : s) {

				if (segment.getFareInformationID() != 0) {
					builder.append("<rad1:Segment>");
					builder.append("<rad1:PersonOrgID>-");
					builder.append(personOrgID);
					builder.append("</rad1:PersonOrgID>");
					builder.append("<rad1:FareInformationID>");
					builder.append(segment.getFareInformationID());
					builder.append("</rad1:FareInformationID>");
					builder.append("<rad1:MarketingCode>");
					builder.append("na");
					builder.append("</rad1:MarketingCode>");
					builder.append("<rad1:StoreFrontID>");
					builder.append("na");
					builder.append("</rad1:StoreFrontID>");
					builder.append("<rad1:SpecialServices>");
					// builder.append("<rad1:SpecialService>");
					//
					// builder.append("<rad1:CodeType>BAGB</rad1:CodeType> ");
					// builder.append("<rad1:ServiceID>-2147483648</rad1:ServiceID>");
					// builder.append("<rad1:SSRCategory>99</rad1:SSRCategory>
					// ");
					// builder.append("<rad1:LogicalFlightID>8356524</rad1:LogicalFlightID>");
					// builder.append("<rad1:DepartureDate>2015-10-03T00:00:00</rad1:DepartureDate>");
					// builder.append("<rad1:Amount>40</rad1:Amount>");
					// builder.append("<rad1:OverrideAmount>true</rad1:OverrideAmount>");
					// builder.append("<rad1:CurrencyCode>AED</rad1:CurrencyCode>");
					// builder.append("<rad1:ChargeComment>BAGB</rad1:ChargeComment>");
					// builder.append("<rad1:PersonOrgID>-214</rad1:PersonOrgID>");
					//
					// builder.append("</rad1:SpecialService>");
					builder.append("</rad1:SpecialServices>");
					builder.append("<rad1:Seats></rad1:Seats>");
					builder.append("</rad1:Segment>");
				}
			}

			builder.append("</rad1:Segments>");

			Payment payment = spr.getPayments().getPayment();

			Date datePaid = new Date();
			datePaid = DateUtils.addYears(datePaid, 1);

			builder.append("<rad1:Payments>");
			builder.append("<rad1:Payment>");
			builder.append("<rad1:ReservationPaymentID>123</rad1:ReservationPaymentID>");
			builder.append("<rad1:CompanyName>paulmerchant</rad1:CompanyName>");
			builder.append("<rad1:FirstName>Test</rad1:FirstName>");
			builder.append("<rad1:LastName>Test</rad1:LastName>");
			builder.append("<rad1:CardType></rad1:CardType>");
			builder.append("<rad1:CardHolder></rad1:CardHolder>");
			builder.append("<rad1:PaymentCurrency>");
			builder.append(payment.getPaymentCurrency());
			builder.append("</rad1:PaymentCurrency>");
			builder.append("<rad1:ISOCurrency>1</rad1:ISOCurrency>");
			builder.append("<rad1:PaymentAmount>");
			builder.append(payment.getPaymentAmount());
			builder.append("</rad1:PaymentAmount>");
			builder.append("<rad1:PaymentMethod>");
			builder.append("INVC");
			builder.append("</rad1:PaymentMethod>");
			builder.append("<rad1:CardNum></rad1:CardNum>");
			builder.append("<rad1:CVCode></rad1:CVCode>");
			builder.append("<rad1:ExpirationDate>");
			builder.append(dateconverter.toString(datePaid));
			builder.append("</rad1:ExpirationDate>");
			builder.append("<rad1:IsTACreditCard>false</rad1:IsTACreditCard>");
			builder.append("<rad1:VoucherNum>123</rad1:VoucherNum>");
			builder.append("<rad1:GcxID>1</rad1:GcxID>");
			builder.append("<rad1:GcxOpt>1</rad1:GcxOpt>");
			builder.append("<rad1:OriginalCurrency>");
			builder.append(payment.getPaymentCurrency());
			builder.append("</rad1:OriginalCurrency>");
			builder.append("<rad1:OriginalAmount>");
			builder.append(payment.getPaymentAmount());
			builder.append("</rad1:OriginalAmount>");
			builder.append("<rad1:ExchangeRate>1</rad1:ExchangeRate>");

			datePaid = new Date();

			builder.append("<rad1:ExchangeRateDate>");
			builder.append(dateconverter.toString(datePaid));
			builder.append("</rad1:ExchangeRateDate>");
			builder.append("<rad1:PaymentComment>OnLine Booking(LNB)</rad1:PaymentComment>");
			builder.append("<rad1:BillingCountry>IN</rad1:BillingCountry>");
			builder.append("</rad1:Payment>");
			builder.append("</rad1:Payments>");

			builder.append("</tem:SummaryPnrRequest></tem:SummaryPNR>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new SummaryPNRException(ire);
		} catch (Exception e) {
			throw new SummaryPNRException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws SummaryPNRException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		xstream.registerLocalConverter(SummaryPNRResult.class, "bookDate", new MyDateConverter());
		xstream.registerLocalConverter(SummaryPNRResult.class, "todaysDate", new MyDateConverter());
		xstream.registerLocalConverter(SummaryPNRResult.class, "lastModified", new MyDateConverter());
		xstream.registerLocalConverter(SummaryPNRResult.class, "reservationExpirationDate", new MyDateConverter());

		xstream.registerLocalConverter(LogicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "arrivaltime", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemBookDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemStartDate", new MyDateConverter());
		xstream.registerLocalConverter(LogicalFlight.class, "packageItemEndDate", new MyDateConverter());

		xstream.registerLocalConverter(PhysicalFlight.class, "departureDate", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "departureTime", new MyDateConverter());
		xstream.registerLocalConverter(PhysicalFlight.class, "arrivaltime", new MyDateConverter());

		xstream.registerLocalConverter(AirlinePerson.class, "dob", new MyDateConverter());
		xstream.registerLocalConverter(AirlinePerson.class, "ticketControlModifiedDate", new MyDateConverter());

		xstream.registerLocalConverter(Charge.class, "billDate", new MyDateConverter());
		xstream.registerLocalConverter(Charge.class, "exchangeRateDate", new MyDateConverter());

		// Parsing the XML response
		Envelope summaryPnrResponse = (Envelope) xstream.fromXML(xmlRequest);

		if (summaryPnrResponse.getBody().getSummaryPNRResponse() == null) {

			Detail detail = summaryPnrResponse.getBody().getFault().getDetail();
			InnerExcption innerExcption = detail.getExceptionDetail().getInnerException();

			throw new SummaryPNRException(innerExcption.getMessage());
		}

		SummaryPNRResult spresult = summaryPnrResponse.getBody().getSummaryPNRResponse().getSummaryPNRResult();

		List<ExceptionInformation> exceptionInformationList = spresult.getExceptions()
				.getExceptionExceptionInformationList();

		for (ExceptionInformation exceptionInformation : exceptionInformationList) {
			if (exceptionInformation.getExceptionCode() != 0) {

				throw new SummaryPNRException(exceptionInformation.getExceptionSource() + ":"
						+ exceptionInformation.getExceptionDescription());
			}
		}

		return summaryPnrResponse;

	}

}

package org.perfomics.flights.business.flydubai.transformer;

import java.util.List;

import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.exception.LogonException;
import org.perfomics.flights.business.flydubai.validator.TravelAgentLoginValidator;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.exceptions.ExceptionInformation;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentRequest;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentResult;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCode;

import com.thoughtworks.xstream.XStream;

public class TravelAgentTransformer {

	public String transformRequest(Envelope loginRequest) throws LogonException {
		try {
			TravelAgentLoginValidator loginValidator = new TravelAgentLoginValidator();

			loginValidator.validate(loginRequest);

			StringBuilder builder = new StringBuilder("<soapenv:Envelope xmlns:soapenv=\"");
			builder.append("http://schemas.xmlsoap.org/soap/envelope/");
			builder.append("\" xmlns:tem=\"");
			builder.append("http://tempuri.org/");
			builder.append("\" xmlns:rad=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Request");
			builder.append("\" xmlns:rad1=\"");
			builder.append("http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.TravelAgents.Request");
			builder.append("\">");
			builder.append("<soapenv:Header/>");
			builder.append("<soapenv:Body><tem:LoginTravelAgent>");
			builder.append("<tem:LoginTravelAgentRequest>");

			LoginTravelAgentRequest ltar = loginRequest.getBody().getLoginTravelAgent().getLoginTravelAgentRequest();
			builder.append("<rad:SecurityGUID>");
			builder.append(ltar.getSecurityGUID());
			builder.append("</rad:SecurityGUID>");

			builder.append("<rad:CarrierCodes>");
			builder.append("<rad:CarrierCode><rad:AccessibleCarrierCode>");

			CarrierCode ccode = ltar.getCarrierCodes().getCarrierCode();

			builder.append(ccode.getAccessibleCarrierCode());
			builder.append("</rad:AccessibleCarrierCode></rad:CarrierCode>");
			builder.append("</rad:CarrierCodes>");

			// builder.append("<rad:ClientIPAddress>");
			// builder.append(ltar.getClientIP());
			// builder.append("</rad:ClientIPAddress>");

			// builder.append(<rad:HistoricUserName>);
			// builder.append(ltar.getHistoricUserName());
			// builder.append(</rad:HistoricUserName>);

			builder.append("<rad1:IATANumber>");
			builder.append(ltar.getIataNumber());
			builder.append("</rad1:IATANumber>");
			builder.append("<rad1:UserName>");
			builder.append(ltar.getUserName());
			builder.append("</rad1:UserName>");
			builder.append("<rad1:Password>");
			builder.append(ltar.getPassword());
			builder.append("</rad1:Password>");

			builder.append("</tem:LoginTravelAgentRequest></tem:LoginTravelAgent>");
			builder.append("</soapenv:Body></soapenv:Envelope>");

			return builder.toString();
		} catch (IncorrectRequestException ire) {
			throw new LogonException(ire);
		} catch (Exception e) {
			throw new LogonException(e);
		}

	}

	public Envelope transformResponse(String xmlRequest) throws LogonException {

		// Create an instance of XStream
		XStream xstream = new XStream();
		xstream.processAnnotations(Envelope.class);

		// Parsing the XML response
		Envelope loginresponse = (Envelope) xstream.fromXML(xmlRequest);
		
		LoginTravelAgentResult ltaresult = loginresponse.getBody().getLoginTravelAgentResponse()
				.getLoginTravelAgentResult();
		List<ExceptionInformation> exceptionInformationList =ltaresult.getExceptions().getExceptionExceptionInformationList();

		for(ExceptionInformation exceptionInformation:exceptionInformationList ){
		if (exceptionInformation.getExceptionCode() != 0) {
			
			throw new LogonException(exceptionInformation.getExceptionSource()+":"+exceptionInformation.getExceptionDescription());
		}
		}
		return loginresponse;

	}

}

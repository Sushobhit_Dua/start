package org.perfomics.flights.business.flydubai.validator;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommissionRequest;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;

public class AgencyCommissionValidator implements RequestValidator {

	public void validate(Envelope commissionRequest) throws IncorrectRequestException {
		try {
			RetrieveAgencyCommissionRequest racr = commissionRequest.getBody().getRetrieveAgencyCommission()
					.getRetrieveAgencyCommissionRequest();
			
			if (StringUtils.isEmpty(racr.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID can't be Empty");
			}
			if (StringUtils.isEmpty(racr.getCurrencyCode())) {
				throw new IncorrectRequestException("Currency Code can't be Empty");
			}
			

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}
	}

}

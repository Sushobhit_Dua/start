package org.perfomics.flights.business.flydubai.validator;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFeesRequest;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;

public class AgencyTransactionFeesValidator implements RequestValidator {

	public void validate(Envelope transactionFeesRequest) throws IncorrectRequestException {
		try {
			AssessAgencyTransactionFeesRequest atfr = transactionFeesRequest.getBody().getAssessAgencyTransactionFees()
					.getAssessAgencyTransactionFeesRequest();

			if (StringUtils.isEmpty(atfr.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID can't be Empty");
			}
			if (StringUtils.isEmpty(atfr.getAgencyCurrency())) {
				throw new IncorrectRequestException("Currency Code can't be Empty");
			}

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}
	}

}

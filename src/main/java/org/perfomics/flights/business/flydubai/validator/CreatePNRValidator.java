package org.perfomics.flights.business.flydubai.validator;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePnrRequest;

public class CreatePNRValidator implements RequestValidator {

	public void validate(Envelope createPNRRequest) throws IncorrectRequestException {
		try {
			CreatePnrRequest cpr = createPNRRequest.getBody().getCreatePNR().getCreatePnrRequest();

			if (StringUtils.isEmpty(cpr.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID can't be Empty");
			}

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}
	}

}

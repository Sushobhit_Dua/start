package org.perfomics.flights.business.flydubai.validator;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteDetail;
import org.perfomics.flights.business.flydubai.xo.farequote.FareQuoteRequestInfo;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuoteRequest;

public class FareQuoteValidator implements RequestValidator {

	public void validate(Envelope fareQuoteRequest) throws IncorrectRequestException {
		try {
			RetrieveFareQuoteRequest rfqr = fareQuoteRequest.getBody().getRetrieveFareQuote()
					.getRetrieveFareQuoteRequest();

			if (StringUtils.isEmpty(rfqr.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID must not be null");
			}

			if (StringUtils.isEmpty(rfqr.getIataNumberRequestor())) {
				throw new IncorrectRequestException("IATA Number must not be null");
			}

			List<FareQuoteDetail> fareList = rfqr.getFareQuoteDetails().getFaredetaillist();
			for (FareQuoteDetail fareQuoteDetail : fareList) {

				if (!(fareQuoteDetail.getDepartDate().getClass().equals(new Date().getClass()))) {
					throw new IncorrectRequestException("Departure date Must be of Date Type");

				}
				
				if (StringUtils.isEmpty(fareQuoteDetail.getOrigin())) {
					throw new IncorrectRequestException("Origin must not be null");
				}
				if (StringUtils.isEmpty(fareQuoteDetail.getDestination())) {
					throw new IncorrectRequestException("Destination must not be null");
				
				}
				
				List<FareQuoteRequestInfo> fareInfoList=fareQuoteDetail.getFareQuoteRequestInfos().getFareinfolist();
				for(FareQuoteRequestInfo fareRequestInfo:fareInfoList){
					
					if (fareRequestInfo.getNumberOfAdult()==0) {
						throw new IncorrectRequestException("Number of Adults must not be zero");
					
					}
					
				}

			}

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}
	}

}

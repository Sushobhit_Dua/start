package org.perfomics.flights.business.flydubai.validator;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.processpnr.PNRPaymentRequest;
import org.perfomics.flights.business.flydubai.xo.processpnr.Payor;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPayment1;
import org.perfomics.flights.business.flydubai.xo.processpnr.TransactionInfo;

public class ProcessPNRValidator implements RequestValidator {

	public void validate(Envelope processPnrRequest) throws IncorrectRequestException {
		try {

			PNRPaymentRequest pnrPaymentRequest = processPnrRequest.getBody().getProcessPNRPayment()
					.getPnrPaymentRequest();

			TransactionInfo transactionInfo = pnrPaymentRequest.getTransactionInfo();

			if (StringUtils.isEmpty(transactionInfo.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID can't be Empty");
			}

			ProcessPNRPayment1 processPnrPayment1 = pnrPaymentRequest.getPnrPayments().getProcessPNRPayment1();

			if (processPnrPayment1.getBaseAmount() == 0.0) {
				throw new IncorrectRequestException("Base Amount Cannot be 0.0");
			}
			if (processPnrPayment1.getOriginalAmount() == 0.0) {
				throw new IncorrectRequestException("Original Amount Cannot be 0.0");
			}
			if (processPnrPayment1.getPaymentAmount() == 0.0) {
				throw new IncorrectRequestException("Payment Amount Cannot be 0.0");
			}
			if (StringUtils.isEmpty(processPnrPayment1.getIataNumber())) {
				throw new IncorrectRequestException("IATA Number can't be Empty");
			}

			Payor payor = processPnrPayment1.getPayor();
			if (StringUtils.isEmpty(payor.getPersonOrgID())) {
				throw new IncorrectRequestException("PersonOrgID must not be null");
			}
			if (StringUtils.isEmpty(payor.getFirstName())) {
				throw new IncorrectRequestException("FirstName must not be null");

			}
			if (StringUtils.isEmpty(payor.getLastName())) {
				throw new IncorrectRequestException("LastName must not be null");

			}

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}
	}

}

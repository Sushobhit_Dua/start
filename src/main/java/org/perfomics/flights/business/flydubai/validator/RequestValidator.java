package org.perfomics.flights.business.flydubai.validator;

import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;

public interface RequestValidator {
	
	/**
	 * Common validate method to be Called by each Validator Class
	 * @param request
	 * @throws IncorrectRequestException
	 */
	  public void validate(Envelope request)throws IncorrectRequestException;
	 

}

package org.perfomics.flights.business.flydubai.validator;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePnrRequest;

public class RetrievePNRValidator implements RequestValidator {

	public void validate(Envelope retrievePnrRequest) throws IncorrectRequestException {
		try {
			RetrievePnrRequest rpr= retrievePnrRequest.getBody().getRetrievePNR().getRetrievePnrRequest();
			
			if (StringUtils.isEmpty(rpr.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID can't be Empty");
			}
			if(StringUtils.isEmpty(rpr.getReservationInfo().getConfirmationNumber())){
				throw new IncorrectRequestException("Confirmation Number can't be Empty");	
			}
			

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}
	}

}

package org.perfomics.flights.business.flydubai.validator;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenRequest;

public class SecurityTokenValidator implements RequestValidator {

	public void validate(Envelope tokenrequest) throws IncorrectRequestException {
		try {

			RetrieveSecurityTokenRequest streq = tokenrequest.getBody().getSecurityToken().getSecurityTokenRequest();
			if (StringUtils.isEmpty(streq.getLogonId())) {
				throw new IncorrectRequestException("LogonId received : null or Empty");
			}
			if (StringUtils.isEmpty(streq.getPassword())) {
				throw new IncorrectRequestException("Password received : null or Empty");
			}

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}

	}

}

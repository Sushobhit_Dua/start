package org.perfomics.flights.business.flydubai.validator;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfo;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Address;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Person;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPnrRequest;

public class SummaryPNRValidator implements RequestValidator {

	public void validate(Envelope summaryPnrRequest) throws IncorrectRequestException {
		try {
			SummaryPnrRequest spr = summaryPnrRequest.getBody().getSummaryPNR().getSummaryPnrRequest();

			if (StringUtils.isEmpty(spr.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID can't be Empty");
			}
			if (StringUtils.isEmpty(spr.getSecurityToken())) {
				throw new IncorrectRequestException("Security Token can't be Empty");
			}

			Address address = spr.getAddress();
			if (StringUtils.isEmpty(address.getCity())) {
				throw new IncorrectRequestException("City must not be null");

			}
			if (StringUtils.isEmpty(address.getCountry())) {
				throw new IncorrectRequestException("Country must not be null");

			}
			if (StringUtils.isEmpty(address.getState())) {
				throw new IncorrectRequestException("State must not be null");

			}
			if (StringUtils.isEmpty(address.getAddress1())) {
				throw new IncorrectRequestException("Address1 must not be null");

			}
			
			List<ContactInfo> contactInfoList = spr.getContactInfos().getContactInfoList();
			for(ContactInfo contactInfo:contactInfoList){
				if (StringUtils.isEmpty(contactInfo.getEmail())) {
					throw new IncorrectRequestException("Email Id must not be null");

				}
				
			}
			
			
			List<Person> personList = spr.getPassengers().getPersonList();
			for (Person person : personList) {
				if (StringUtils.isEmpty(person.getFirstName())) {
					throw new IncorrectRequestException("FirstName must not be null");

				}
				if (StringUtils.isEmpty(person.getLastName())) {
					throw new IncorrectRequestException("LastName must not be null");

				}
				if(!(person.getDob().getClass().equals(new Date().getClass()))){
					throw new IncorrectRequestException("Passenger DateOfBirth Must be of Date Type");			
				}

			}

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}

	}

}

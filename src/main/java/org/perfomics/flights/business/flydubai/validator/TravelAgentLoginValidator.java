package org.perfomics.flights.business.flydubai.validator;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentRequest;

public class TravelAgentLoginValidator implements RequestValidator {

	public void validate(Envelope loginRequest) throws IncorrectRequestException {

		try {
			LoginTravelAgentRequest ltareq = loginRequest.getBody().getLoginTravelAgent().getLoginTravelAgentRequest();
			if (StringUtils.isEmpty(ltareq.getSecurityGUID())) {
				throw new IncorrectRequestException("Security GUID can't be Empty");
			}
			if (StringUtils.isEmpty(ltareq.getIataNumber())) {
				throw new IncorrectRequestException("IATA Number can't be Empty");
			}
			if (StringUtils.isEmpty(ltareq.getUserName())) {
				throw new IncorrectRequestException("UserName can't be blank");
			}
			if (StringUtils.isEmpty(ltareq.getPassword())) {
				throw new IncorrectRequestException("Password field can't be Empty");
			}

		} catch (IncorrectRequestException ire) {
			throw ire;
		} catch (Exception e) {
			throw new IncorrectRequestException(e);
		}

	}

}

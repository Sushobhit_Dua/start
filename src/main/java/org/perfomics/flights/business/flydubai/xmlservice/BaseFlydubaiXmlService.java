package org.perfomics.flights.business.flydubai.xmlservice;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.xo.DefaultSettings;

public class BaseFlydubaiXmlService {

	public final static Logger LOGGER = Logger.getLogger(BaseFlydubaiXmlService.class);
	
	/**
	 *  XMLService call this according to the Request
	 * @param request
	 * @param count
	 * @return String
	 * @throws IncorrectRequestException
	 */
	   public static String sendAndReceive(String request, int count) throws IncorrectRequestException {
	 
	 

		String postresponse = null;
		LOGGER.debug(request);
		System.out.println(request);
		
		byte bytearray[] = request.getBytes();
		InputStream stream = new ByteArrayInputStream(bytearray);

		HttpClient client = new HttpClient();

		PostMethod post = new PostMethod(DefaultSettings.PostLinks[count]);

		try {

			post.setRequestEntity(new InputStreamRequestEntity(stream));
			post.setRequestHeader("Content-type", "text/xml");
			post.setRequestHeader("SOAPAction", DefaultSettings.SoapActions[count]);

			// Start of Connection
			client.executeMethod(post);
			
		LOGGER.debug(post.getResponseBodyAsString());

			postresponse = post.getResponseBodyAsString();
			System.out.println(postresponse);
			
		}

		catch (UnknownHostException uhe) {
			throw new IncorrectRequestException("Please check your the internet Connection And Try Again", uhe);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			post.releaseConnection();
		}

		return postresponse;
	}

}

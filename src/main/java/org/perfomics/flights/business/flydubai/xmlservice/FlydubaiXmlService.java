package org.perfomics.flights.business.flydubai.xmlservice;

import org.apache.commons.lang3.StringUtils;
import org.perfomics.flights.business.flydubai.exception.CommissionException;
import org.perfomics.flights.business.flydubai.exception.CreatePNRException;
import org.perfomics.flights.business.flydubai.exception.FareQuoteException;
import org.perfomics.flights.business.flydubai.exception.IncorrectRequestException;
import org.perfomics.flights.business.flydubai.exception.LogonException;
import org.perfomics.flights.business.flydubai.exception.ProcessPNRException;
import org.perfomics.flights.business.flydubai.exception.RetrievePNRException;
import org.perfomics.flights.business.flydubai.exception.SummaryPNRException;
import org.perfomics.flights.business.flydubai.exception.TransactionFeesException;
import org.perfomics.flights.business.flydubai.exception.UnableToRetrieveTokenException;
import org.perfomics.flights.business.flydubai.transformer.AgencyCommissionTransformer;
import org.perfomics.flights.business.flydubai.transformer.AgencyTransactionFeesTransformer;
import org.perfomics.flights.business.flydubai.transformer.CreatePNRTransformer;
import org.perfomics.flights.business.flydubai.transformer.FareQuoteTransformer;
import org.perfomics.flights.business.flydubai.transformer.ProcessPNRTransformer;
import org.perfomics.flights.business.flydubai.transformer.RetrievePNRTransformer;
import org.perfomics.flights.business.flydubai.transformer.RetrieveSecurityTokenTransformer;
import org.perfomics.flights.business.flydubai.transformer.SummaryPNRTransformer;
import org.perfomics.flights.business.flydubai.transformer.TravelAgentTransformer;
import org.perfomics.flights.business.flydubai.xo.common.Envelope;

public class FlydubaiXmlService extends BaseFlydubaiXmlService {

	/**
	 * API call to fetch security token which is a requisite for further API
	 * call
	 * 
	 * @param tokenRequest
	 * @return Envelope
	 * @throws UnableToRetrieveTokenException
	 * @throws IncorrectRequestException
	 */
	public static Envelope retrieveSecurityToken(Envelope tokenRequest)
			throws UnableToRetrieveTokenException, IncorrectRequestException {
		RetrieveSecurityTokenTransformer tokentransformer = new RetrieveSecurityTokenTransformer();
		String request = tokentransformer.transformRequest(tokenRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 0);

		if (StringUtils.isEmpty(postresponse)) {
			throw new UnableToRetrieveTokenException("Response is not Coming");
		}

		Envelope tokenResponse = tokentransformer.transformResponse(postresponse);
		return tokenResponse;
	}

	/**
	 * API call to Login the Travel Agent
	 * 
	 * @param loginRequest
	 * @return Envelope
	 * @throws LogonException
	 * @throws IncorrectRequestException
	 */
	public static Envelope loginTravelAgent(Envelope loginRequest) throws LogonException, IncorrectRequestException {

		TravelAgentTransformer agenttransformer = new TravelAgentTransformer();
		String request = agenttransformer.transformRequest(loginRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 1);

		Envelope loginResponse = agenttransformer.transformResponse(postresponse);
		return loginResponse;
	}

	/**
	 * API call to Get the Agency Commission for the Travel Agent which is
	 * Logged On
	 * 
	 * @param commissionRequest
	 * @return Envelope
	 * @throws CommissionException
	 * @throws IncorrectRequestException
	 */
	public static Envelope retrieveAgencyCommission(Envelope commissionRequest)
			throws CommissionException, IncorrectRequestException {

		AgencyCommissionTransformer commissionTransformer = new AgencyCommissionTransformer();
		String request = commissionTransformer.transformRequest(commissionRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 2);

		Envelope commissionResponse = commissionTransformer.transformResponse(postresponse);
		return commissionResponse;
	}

	/**
	 * API call to Get the FlightSearch Results
	 * 
	 * @param fareQuoteRequest
	 * @return Envelope
	 * @throws FareQuoteException
	 * @throws IncorrectRequestException
	 */
	public static Envelope retrieveFareQuote(Envelope fareQuoteRequest)
			throws FareQuoteException, IncorrectRequestException {

		FareQuoteTransformer fareTransformer = new FareQuoteTransformer();
		String request = fareTransformer.transformRequest(fareQuoteRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 3);

		Envelope fareQuoteResponse = fareTransformer.transformResponse(postresponse);
		return fareQuoteResponse;
	}

	/**
	 * API call to get the Summary of PNR by giving the person details
	 * 
	 * @param summaryPnrRequest
	 * @return Envelope
	 * @throws SummaryPNRException
	 * @throws IncorrectRequestException
	 */
	public static Envelope summaryPNR(Envelope summaryPnrRequest)
			throws SummaryPNRException, IncorrectRequestException {

		SummaryPNRTransformer summaryPNRTransformer = new SummaryPNRTransformer();
		String request = summaryPNRTransformer.transformRequest(summaryPnrRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 4);

		Envelope summaryPnrResponse = summaryPNRTransformer.transformResponse(postresponse);
		return summaryPnrResponse;
	}

	/**
	 * API call to get the Transaction fees
	 * 
	 * @param transactionFeesRequest
	 * @return Envelope
	 * @throws TransactionFeesException
	 * @throws IncorrectRequestException
	 */
	public static Envelope agencyTransactionFees(Envelope transactionFeesRequest)
			throws TransactionFeesException, IncorrectRequestException {

		AgencyTransactionFeesTransformer transactionFeesTransformer = new AgencyTransactionFeesTransformer();
		String request = transactionFeesTransformer.transformRequest(transactionFeesRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 5);

		Envelope transactionFeesResponse = transactionFeesTransformer.transformResponse(postresponse);
		return transactionFeesResponse;
	}

	/**
	 * API call to get the PNR Payment Process
	 * 
	 * @param processPnrRequest
	 * @return Envelope
	 * @throws ProcessPNRException
	 * @throws IncorrectRequestException
	 */
	public static Envelope processPNR(Envelope processPnrRequest)
			throws ProcessPNRException, IncorrectRequestException {

		ProcessPNRTransformer processPNRTransformer = new ProcessPNRTransformer();
		String request = processPNRTransformer.transformRequest(processPnrRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 6);

		Envelope processPnrResponse = processPNRTransformer.transformResponse(postresponse);
		return processPnrResponse;
	}

	/**
	 * API call to get the PNR Pin number
	 * 
	 * @param createPnrRequest
	 * @return Envelope
	 * @throws CreatePNRException
	 * @throws IncorrectRequestException
	 */
	public static Envelope createPNR(Envelope createPnrRequest) throws CreatePNRException, IncorrectRequestException {

		CreatePNRTransformer createPNRTransformer = new CreatePNRTransformer();
		String request = createPNRTransformer.transformRequest(createPnrRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 7);

		Envelope createPnrResponse = createPNRTransformer.transformResponse(postresponse);
		return createPnrResponse;
	}
	
	/**
	 * API call to Get the Reservation PNR
	 * @param retrievePnrRequest
	 * @return Envelope
	 * @throws IncorrectRequestException
	 * @throws RetrievePNRException
	 */
	public static Envelope retreivePNR(Envelope retrievePnrRequest)
			throws IncorrectRequestException, RetrievePNRException {

		RetrievePNRTransformer retrievePNRTransformer = new RetrievePNRTransformer();
		String request = retrievePNRTransformer.transformRequest(retrievePnrRequest);

		// execution of Connection and get response
		String postresponse = sendAndReceive(request, 8);

		Envelope retrievePnrResponse = retrievePNRTransformer.transformResponse(postresponse);
		return retrievePnrResponse;
	}
}

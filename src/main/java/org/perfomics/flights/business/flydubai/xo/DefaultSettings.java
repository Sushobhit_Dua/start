package org.perfomics.flights.business.flydubai.xo;

public class DefaultSettings {

	//static final constants for HttpClient timeout
	public static final int HTTPCLIENT_CONNECTION_TIMEOUT=60000;
	public static final int HTTPCLIENT_SOCKET_TIMEOUT=90000;

	public static String[] PostLinks={"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.Security.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.TravelAgents.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.TravelAgents.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.Pricing.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.Reservation.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.Fees.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.Fulfillment.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.Reservation.svc",
			"http://uat.ops.connectpoint.flydubai.com/ConnectPoint.Reservation.svc"};
	
	public static String[] SoapActions={"http://tempuri.org/IConnectPoint_Security/RetrieveSecurityToken",
			"http://tempuri.org/IConnectPoint_TravelAgents/LoginTravelAgent",
			"http://tempuri.org/IConnectPoint_TravelAgents/RetrieveAgencyCommission",
			"http://tempuri.org/IConnectPoint_Pricing/RetrieveFareQuote",
			"http://tempuri.org/IConnectPoint_Reservation/SummaryPNR",
			"http://tempuri.org/IConnectPoint_Fees/AssessAgencyTransactionFees",
			"http://tempuri.org/IConnectPoint_Fulfillment/ProcessPNRPayment",
			"http://tempuri.org/IConnectPoint_Reservation/CreatePNR",
			"http://tempuri.org/IConnectPoint_Reservation/RetrievePNR"};

}

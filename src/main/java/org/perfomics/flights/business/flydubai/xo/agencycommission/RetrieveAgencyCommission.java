package org.perfomics.flights.business.flydubai.xo.agencycommission;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrieveAgencyCommission")
public class RetrieveAgencyCommission {
	
	@XStreamAlias("tem:RetrieveAgencyCommissionRequest")
	private RetrieveAgencyCommissionRequest retrieveAgencyCommissionRequest;

	public RetrieveAgencyCommissionRequest getRetrieveAgencyCommissionRequest() {
		return retrieveAgencyCommissionRequest;
	}

	public void setRetrieveAgencyCommissionRequest(RetrieveAgencyCommissionRequest retrieveAgencyCommissionRequest) {
		this.retrieveAgencyCommissionRequest = retrieveAgencyCommissionRequest;
	}


}

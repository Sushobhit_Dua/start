package org.perfomics.flights.business.flydubai.xo.agencycommission;

import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrieveAgencyCommissionRequest")
public class RetrieveAgencyCommissionRequest {
	
	@XStreamAlias("rad:CarrierCodes")	
	private CarrierCodes carrierCodes;

	@XStreamAlias("rad:SecurityGUID")	
	private String securityGUID;

	@XStreamAlias("rad:ClientIPAddress")	
	private String clientIP;

	@XStreamAlias("rad:HistoricUserName")	
	private String historicUserName;

	@XStreamAlias("rad1:CurrencyCode")	
	private String currencyCode;

	public CarrierCodes getCarrierCodes() {
		return carrierCodes;
	}

	public void setCarrierCodes(CarrierCodes carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	public String getSecurityGUID() {
		return securityGUID;
	}

	public void setSecurityGUID(String securityGUID) {
		this.securityGUID = securityGUID;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getHistoricUserName() {
		return historicUserName;
	}

	public void setHistoricUserName(String historicUserName) {
		this.historicUserName = historicUserName;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}



}

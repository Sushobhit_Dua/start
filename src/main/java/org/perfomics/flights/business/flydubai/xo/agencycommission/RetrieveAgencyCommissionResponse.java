package org.perfomics.flights.business.flydubai.xo.agencycommission;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("RetrieveAgencyCommissionResponse")
public class RetrieveAgencyCommissionResponse {
	
	@XStreamAlias("RetrieveAgencyCommissionResult")	
	private RetrieveAgencyCommissionResult retrieveAgencyCommissionResult;

	@XStreamAlias("xmlns")	
	@XStreamAsAttribute
	private String xmlns;

	public RetrieveAgencyCommissionResult getRetrieveAgencyCommissionResult() {
		return retrieveAgencyCommissionResult;
	}

	public void setRetrieveAgencyCommissionResult(RetrieveAgencyCommissionResult retrieveAgencyCommissionResult) {
		this.retrieveAgencyCommissionResult = retrieveAgencyCommissionResult;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}


}

package org.perfomics.flights.business.flydubai.xo.agencycommission;

import org.perfomics.flights.business.flydubai.xo.exceptions.Exceptions;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("RetrieveAgencyCommissionResult")	
public class RetrieveAgencyCommissionResult {
	
	@XStreamAlias("a:TravelAgencyCommissions")
	private TravelAgencyCommissions travelAgencyCommissions;
	
	@XStreamAlias("a:Exceptions")
	private Exceptions exceptions;

	public TravelAgencyCommissions getTravelAgencyCommissions() {
		return travelAgencyCommissions;
	}

	public void setTravelAgencyCommissions(TravelAgencyCommissions travelAgencyCommissions) {
		this.travelAgencyCommissions = travelAgencyCommissions;
	}

	public Exceptions getExceptions() {
		return exceptions;
	}

	public void setExceptions(Exceptions exceptions) {
		this.exceptions = exceptions;
	}
	

}

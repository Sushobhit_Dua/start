package org.perfomics.flights.business.flydubai.xo.agencycommission;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:TravelAgencyCommission")
public class TravelAgencyCommission {
	
	@XStreamAlias("a:Amount")	
	private int amount;

	@XStreamAlias("a:IsPercentage")	
	private boolean percentage;

	@XStreamAlias("a:MinAmount")	
	private int minAmount;

	@XStreamAlias("a:MaxAmount")	
	private int maxAmount;
	
	@XStreamAlias("a:PaymentMethod")	
	private String paymentMethod;

	@XStreamAlias("a:AssessAsFee")	
	private boolean assessAsFee;

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public boolean isPercentage() {
		return percentage;
	}

	public void setPercentage(boolean percentage) {
		this.percentage = percentage;
	}

	public int getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(int minAmount) {
		this.minAmount = minAmount;
	}

	public int getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(int maxAmount) {
		this.maxAmount = maxAmount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public boolean isAssessAsFee() {
		return assessAsFee;
	}

	public void setAssessAsFee(boolean assessAsFee) {
		this.assessAsFee = assessAsFee;
	}

	
}

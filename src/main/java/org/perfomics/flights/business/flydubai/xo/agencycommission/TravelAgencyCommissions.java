package org.perfomics.flights.business.flydubai.xo.agencycommission;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:TravelAgencyCommissions")
public class TravelAgencyCommissions {
	
	@XStreamAlias("a:TravelAgencyCommission")
	private TravelAgencyCommission travelAgencyCommission;

	public TravelAgencyCommission getTravelAgencyCommission() {
		return travelAgencyCommission;
	}

	public void setTravelAgencyCommission(TravelAgencyCommission travelAgencyCommission) {
		this.travelAgencyCommission = travelAgencyCommission;
	}

}

package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:Airline")
public class Airline {
	 
	 @XStreamAlias("a:Key")
	 private String key;

	 @XStreamAlias("a:LogicalFlight")
	 private LogicalFlight logicalFlight;
	 
	 @XStreamAlias("a:OAFlights")
	 private String oaFlights;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public LogicalFlight getLogicalFlight() {
		return logicalFlight;
	}

	public void setLogicalFlight(LogicalFlight logicalFlight) {
		this.logicalFlight = logicalFlight;
	}

	public String getOaFlights() {
		return oaFlights;
	}

	public void setOaFlights(String oaFlights) {
		this.oaFlights = oaFlights;
	}


}

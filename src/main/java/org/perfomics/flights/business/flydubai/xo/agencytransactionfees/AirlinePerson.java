package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:AirlinePerson")
public class AirlinePerson {
	
	 @XStreamAlias("a:Key")
	 private String key;

	 @XStreamAlias("a:PersonOrgID")
	 private String personOrgID;

	 @XStreamAlias("a:FirstName")
	 private String firstName;

	 @XStreamAlias("a:LastName")
	 private String lastName;

	 @XStreamAlias("a:MiddleName")
	 private String middleName;

	 @XStreamAlias("a:Age")
	 private String age;

	 @XStreamAlias("a:DOB")
	 private Date dob;

	 @XStreamAlias("a:Gender")
	 private String gender;
	 
	 @XStreamAlias("a:Title")
	 private String title;
	
	 @XStreamAlias("a:NationalityLaguageID")
	 private String nationalityLaguageID;
	
	 @XStreamAlias("a:RelationType")
	 private String relationType;
	
	 @XStreamAlias("a:WBCID")
	 private int wbCID;
	
	 @XStreamAlias("a:PTCID")
	 private int ptcID;

	 @XStreamAlias("a:UseInventory")
	 private boolean useInventory;
	
	 @XStreamAlias("a:Address")
	 private String address;
	
	 @XStreamAlias("a:Address2")
	 private String address2;
	
	 @XStreamAlias("a:City")
	 private String city;
	
	 @XStreamAlias("a:State")
	 private String state;
	
	 @XStreamAlias("a:Postal")
	 private String postal;
	
	 @XStreamAlias("a:Country")
	 private String country;
	
	 @XStreamAlias("a:Company")
	 private String company;
	
	 @XStreamAlias("a:Comments")
	 private String comments;
		 
	 @XStreamAlias("a:Passport")
	 private String passport;
	
	 @XStreamAlias("a:Nationality")
	 private String nationality;
	
	 @XStreamAlias("a:ProfileId")
	 private String profileId;
	
	 @XStreamAlias("a:Weight")
	 private String weight;
	 
	 @XStreamAlias("a:FFNum")
	 private String ffNum;
	
	 @XStreamAlias("a:PaxActive")
	 private boolean paxActive;
	 
	 @XStreamAlias("a:RecordNumber")
	 private String recordNumber;
	
	 @XStreamAlias("a:RecordLocator")
	 private String recordLocator;
	 
	 @XStreamAlias("a:OriginalRecordNumber")
	 private String originalRecordNumber;

	 @XStreamAlias("a:ContactInfo")
	 private String contactInfo;
	
	 @XStreamAlias("a:DropoffID")
	 private String dropoffID;
	
	 @XStreamAlias("a:PickupID")
	 private String pickupID;
	 
	 @XStreamAlias("a:LapChildID")
	 private String lapChildID;
	
	 @XStreamAlias("a:FareClassCode")
	 private String fareClassCode;
	 
	 @XStreamAlias("a:FareBasisCode")
	 private String fareBasisCode;
	
	 @XStreamAlias("a:WebFareType")
	 private String webFareType;
	 
	 @XStreamAlias("a:FareBasisSched")
	 private String fareBasisSched;
	
	 @XStreamAlias("a:FareAmount")
	 private double fareAmount;
	 
	 @XStreamAlias("a:ResSegStatus")
	 private String resSegStatus;
	
	 @XStreamAlias("a:SegSubStatus")
	 private String segSubStatus;
	 
	 @XStreamAlias("a:SelecteeStatus")
	 private String selecteeStatus;

	 @XStreamAlias("a:CheckinStatus")
	 private boolean checkinStatus;
	
	 @XStreamAlias("a:Cabin")
	 private String cabin;
	 
	 @XStreamAlias("a:TicketNumber")
	 private String ticketNumber;

	 @XStreamAlias("a:HasTickets")
	 private boolean hasTickets;
	 
	 @XStreamAlias("a:UIDisplayValue")
	 private String uiDisplayValue;
	 
	 @XStreamAlias("a:InterlinedSegment")
	 private String interlinedSegment;

	 @XStreamAlias("a:InterlinedCarrierCode")
	 private String interlinedCarrierCode;
	
	 @XStreamAlias("a:ManualFare")
	 private String manualFare;
	 
	 @XStreamAlias("a:InventoryOverbooked")
	 private String inventoryOverbooked;

	 @XStreamAlias("a:TicketCouponNumber")
	 private String ticketCouponNumber;

	 @XStreamAlias("a:TicketControl")
	 private String ticketControl;

	 @XStreamAlias("a:TicketControlOwner")
	 private String ticketControlOwner;

	 @XStreamAlias("a:TicketControlModifiedDate")
	 private Date ticketControlModifiedDate;

	 @XStreamAlias("a:MarketingCode")
	 private String marketingCode;

	 @XStreamAlias("a:MarketingOptIn")
	 private boolean marketingOptIn;
	 
	 @XStreamAlias("a:EmergencyContactID")
	 private String emergencyContactID;

	 @XStreamAlias("a:DisclosedEmergencyContact")
	 private String disclosedEmergencyContact;

	 @XStreamAlias("a:CappsStatus")
	 private String cappsStatus;

	 @XStreamAlias("a:ToRecordNumber")
	 private String toRecordNumber;

	 @XStreamAlias("a:FromRecordNumber")
	 private String fromRecordNumber;

	 @XStreamAlias("a:StoreFrontID")
	 private String storeFrontID;

	 @XStreamAlias("a:InsuranceConfNum")
	 private String insuranceConfNum;

	 @XStreamAlias("a:InsuranceTransID")
	 private String insuranceTransID;

	 @XStreamAlias("a:RedressNum")
	 private String redressNum;

	 @XStreamAlias("a:KnownTravNum")
	 private String knownTravNum;

	 @XStreamAlias("a:PrimaryPassenger")
	 private String primaryPassenger;

	 @XStreamAlias("a:NameChangeCount")
	 private String nameChangeCount;

	 @XStreamAlias("a:CrsCode")
	 private String crsCode;

	 @XStreamAlias("a:SeatAssignments")
	 private SeatAssignments seatAssignments;

	 @XStreamAlias("a:Charges")
	 private Charges charges;
	
	 @XStreamAlias("a:Bags")
	 private String bags;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPersonOrgID() {
		return personOrgID;
	}

	public void setPersonOrgID(String personOrgID) {
		this.personOrgID = personOrgID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNationalityLaguageID() {
		return nationalityLaguageID;
	}

	public void setNationalityLaguageID(String nationalityLaguageID) {
		this.nationalityLaguageID = nationalityLaguageID;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public int getWbCID() {
		return wbCID;
	}

	public void setWbCID(int wbCID) {
		this.wbCID = wbCID;
	}

	public int getPtcID() {
		return ptcID;
	}

	public void setPtcID(int ptcID) {
		this.ptcID = ptcID;
	}

	public boolean isUseInventory() {
		return useInventory;
	}

	public void setUseInventory(boolean useInventory) {
		this.useInventory = useInventory;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getWeight() {
		return weight;
	}

	public void setWeight(String weight) {
		this.weight = weight;
	}

	public String getFfNum() {
		return ffNum;
	}

	public void setFfNum(String ffNum) {
		this.ffNum = ffNum;
	}

	public boolean isPaxActive() {
		return paxActive;
	}

	public void setPaxActive(boolean paxActive) {
		this.paxActive = paxActive;
	}

	public String getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

	public String getRecordLocator() {
		return recordLocator;
	}

	public void setRecordLocator(String recordLocator) {
		this.recordLocator = recordLocator;
	}

	public String getOriginalRecordNumber() {
		return originalRecordNumber;
	}

	public void setOriginalRecordNumber(String originalRecordNumber) {
		this.originalRecordNumber = originalRecordNumber;
	}

	public String getContactInfo() {
		return contactInfo;
	}

	public void setContactInfo(String contactInfo) {
		this.contactInfo = contactInfo;
	}

	public String getDropoffID() {
		return dropoffID;
	}

	public void setDropoffID(String dropoffID) {
		this.dropoffID = dropoffID;
	}

	public String getPickupID() {
		return pickupID;
	}

	public void setPickupID(String pickupID) {
		this.pickupID = pickupID;
	}

	public String getLapChildID() {
		return lapChildID;
	}

	public void setLapChildID(String lapChildID) {
		this.lapChildID = lapChildID;
	}

	public String getFareClassCode() {
		return fareClassCode;
	}

	public void setFareClassCode(String fareClassCode) {
		this.fareClassCode = fareClassCode;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getWebFareType() {
		return webFareType;
	}

	public void setWebFareType(String webFareType) {
		this.webFareType = webFareType;
	}

	public String getFareBasisSched() {
		return fareBasisSched;
	}

	public void setFareBasisSched(String fareBasisSched) {
		this.fareBasisSched = fareBasisSched;
	}

	public double getFareAmount() {
		return fareAmount;
	}

	public void setFareAmount(double fareAmount) {
		this.fareAmount = fareAmount;
	}

	public String getResSegStatus() {
		return resSegStatus;
	}

	public void setResSegStatus(String resSegStatus) {
		this.resSegStatus = resSegStatus;
	}

	public String getSegSubStatus() {
		return segSubStatus;
	}

	public void setSegSubStatus(String segSubStatus) {
		this.segSubStatus = segSubStatus;
	}

	public String getSelecteeStatus() {
		return selecteeStatus;
	}

	public void setSelecteeStatus(String selecteeStatus) {
		this.selecteeStatus = selecteeStatus;
	}

	public boolean isCheckinStatus() {
		return checkinStatus;
	}

	public void setCheckinStatus(boolean checkinStatus) {
		this.checkinStatus = checkinStatus;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public String getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public boolean isHasTickets() {
		return hasTickets;
	}

	public void setHasTickets(boolean hasTickets) {
		this.hasTickets = hasTickets;
	}


	public String getInterlinedSegment() {
		return interlinedSegment;
	}

	public void setInterlinedSegment(String interlinedSegment) {
		this.interlinedSegment = interlinedSegment;
	}

	public String getInterlinedCarrierCode() {
		return interlinedCarrierCode;
	}

	public void setInterlinedCarrierCode(String interlinedCarrierCode) {
		this.interlinedCarrierCode = interlinedCarrierCode;
	}

	public String getManualFare() {
		return manualFare;
	}

	public void setManualFare(String manualFare) {
		this.manualFare = manualFare;
	}

	public String getInventoryOverbooked() {
		return inventoryOverbooked;
	}

	public void setInventoryOverbooked(String inventoryOverbooked) {
		this.inventoryOverbooked = inventoryOverbooked;
	}

	public String getTicketCouponNumber() {
		return ticketCouponNumber;
	}

	public void setTicketCouponNumber(String ticketCouponNumber) {
		this.ticketCouponNumber = ticketCouponNumber;
	}

	public String getTicketControl() {
		return ticketControl;
	}

	public void setTicketControl(String ticketControl) {
		this.ticketControl = ticketControl;
	}

	public String getTicketControlOwner() {
		return ticketControlOwner;
	}

	public void setTicketControlOwner(String ticketControlOwner) {
		this.ticketControlOwner = ticketControlOwner;
	}

	public Date getTicketControlModifiedDate() {
		return ticketControlModifiedDate;
	}

	public void setTicketControlModifiedDate(Date ticketControlModifiedDate) {
		this.ticketControlModifiedDate = ticketControlModifiedDate;
	}

	public String getMarketingCode() {
		return marketingCode;
	}

	public void setMarketingCode(String marketingCode) {
		this.marketingCode = marketingCode;
	}

	public boolean isMarketingOptIn() {
		return marketingOptIn;
	}

	public void setMarketingOptIn(boolean marketingOptIn) {
		this.marketingOptIn = marketingOptIn;
	}

	public String getEmergencyContactID() {
		return emergencyContactID;
	}

	public void setEmergencyContactID(String emergencyContactID) {
		this.emergencyContactID = emergencyContactID;
	}

	public String getDisclosedEmergencyContact() {
		return disclosedEmergencyContact;
	}

	public void setDisclosedEmergencyContact(String disclosedEmergencyContact) {
		this.disclosedEmergencyContact = disclosedEmergencyContact;
	}

	public String getCappsStatus() {
		return cappsStatus;
	}

	public void setCappsStatus(String cappsStatus) {
		this.cappsStatus = cappsStatus;
	}

	public String getToRecordNumber() {
		return toRecordNumber;
	}

	public void setToRecordNumber(String toRecordNumber) {
		this.toRecordNumber = toRecordNumber;
	}

	public String getFromRecordNumber() {
		return fromRecordNumber;
	}

	public void setFromRecordNumber(String fromRecordNumber) {
		this.fromRecordNumber = fromRecordNumber;
	}

	public String getStoreFrontID() {
		return storeFrontID;
	}

	public void setStoreFrontID(String storeFrontID) {
		this.storeFrontID = storeFrontID;
	}

	public String getInsuranceConfNum() {
		return insuranceConfNum;
	}

	public void setInsuranceConfNum(String insuranceConfNum) {
		this.insuranceConfNum = insuranceConfNum;
	}

	public String getInsuranceTransID() {
		return insuranceTransID;
	}

	public void setInsuranceTransID(String insuranceTransID) {
		this.insuranceTransID = insuranceTransID;
	}

	public String getRedressNum() {
		return redressNum;
	}

	public void setRedressNum(String redressNum) {
		this.redressNum = redressNum;
	}

	public String getKnownTravNum() {
		return knownTravNum;
	}

	public void setKnownTravNum(String knownTravNum) {
		this.knownTravNum = knownTravNum;
	}

	public String getPrimaryPassenger() {
		return primaryPassenger;
	}

	public void setPrimaryPassenger(String primaryPassenger) {
		this.primaryPassenger = primaryPassenger;
	}

	public String getNameChangeCount() {
		return nameChangeCount;
	}

	public void setNameChangeCount(String nameChangeCount) {
		this.nameChangeCount = nameChangeCount;
	}

	public String getCrsCode() {
		return crsCode;
	}

	public void setCrsCode(String crsCode) {
		this.crsCode = crsCode;
	}

	public SeatAssignments getSeatAssignments() {
		return seatAssignments;
	}

	public void setSeatAssignments(SeatAssignments seatAssignments) {
		this.seatAssignments = seatAssignments;
	}

	public Charges getCharges() {
		return charges;
	}

	public void setCharges(Charges charges) {
		this.charges = charges;
	}

	public String getBags() {
		return bags;
	}

	public void setBags(String bags) {
		this.bags = bags;
	}

	public String getUiDisplayValue() {
		return uiDisplayValue;
	}

	public void setUiDisplayValue(String uiDisplayValue) {
		this.uiDisplayValue = uiDisplayValue;
	}

}

package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:AirlinePersons")
public class AirlinePersons {

	@XStreamImplicit
	private List<AirlinePerson> airlinePersonList=new ArrayList<AirlinePerson>();

	public List<AirlinePerson> getAirlinePersonList() {
		return airlinePersonList;
	}

	public void addAirlinePersonList(AirlinePerson airlinePerson) {
		airlinePersonList.add(airlinePerson);
	}
	
}

package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:Airlines")
public class Airlines {

	@XStreamImplicit
	private List<Airline> airlineList=new ArrayList<Airline>();

	public List<Airline> getAirlineList() {
		return airlineList;
	}

	public void addAirlineList(Airline airline) {
		airlineList.add(airline);
	}
		
}

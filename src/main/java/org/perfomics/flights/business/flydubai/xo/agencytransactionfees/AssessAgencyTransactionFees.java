package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:AssessAgencyTransactionFees")
public class AssessAgencyTransactionFees {
	
	@XStreamAlias("tem:AssessAgencyTransactionFeesRequest")
	private AssessAgencyTransactionFeesRequest assessAgencyTransactionFeesRequest;

	public AssessAgencyTransactionFeesRequest getAssessAgencyTransactionFeesRequest() {
		return assessAgencyTransactionFeesRequest;
	}

	public void setAssessAgencyTransactionFeesRequest(
			AssessAgencyTransactionFeesRequest assessAgencyTransactionFeesRequest) {
		this.assessAgencyTransactionFeesRequest = assessAgencyTransactionFeesRequest;
	}


}

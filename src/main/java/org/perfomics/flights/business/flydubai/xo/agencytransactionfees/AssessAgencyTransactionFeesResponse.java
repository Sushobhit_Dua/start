package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("AssessAgencyTransactionFeesResponse")
public class AssessAgencyTransactionFeesResponse {
	
	@XStreamAlias("AssessAgencyTransactionFeesResult")	
	private AssessAgencyTransactionFeesResult assessAgencyTransactionFeesResult;

	@XStreamAlias("xmlns")	
	@XStreamAsAttribute
	private String xmlns;

	public AssessAgencyTransactionFeesResult getAssessAgencyTransactionFeesResult() {
		return assessAgencyTransactionFeesResult;
	}

	public void setAssessAgencyTransactionFeesResult(AssessAgencyTransactionFeesResult assessAgencyTransactionFeesResult) {
		this.assessAgencyTransactionFeesResult = assessAgencyTransactionFeesResult;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}


}

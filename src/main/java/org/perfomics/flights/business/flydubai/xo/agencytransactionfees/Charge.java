package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:Charge")
public class Charge {

	 @XStreamAlias("a:Key")
	 private String key;
	 
	 @XStreamAlias("a:ReservationChargeID")
	 private String reservationChargeID;

	 @XStreamAlias("a:OriginalChargeID")
	 private String originalChargeID;

	 @XStreamAlias("a:CodeType")
	 private String codeType;

	 @XStreamAlias("a:RefundPersonOrgID")
	 private String refundPersonOrgID;
	
	 @XStreamAlias("a:RefundType")
	 private String refundType;
	
	 @XStreamAlias("a:CurrencyCode")
	 private String currencyCode;
	
	 @XStreamAlias("a:OriginalCurrency")
	 private String originalCurrency;
	
	 @XStreamAlias("a:RecordNumber")
	 private String recordNumber;
	
	 @XStreamAlias("a:VoucherNumber")
	 private String voucherNumber;
	
	 @XStreamAlias("a:BillDate")
	 private Date billDate;
	
	 @XStreamAlias("a:RPM")
	 private int rpm;
	
	 @XStreamAlias("a:TaxID")
	 private String taxId;
	
	 @XStreamAlias("a:ChargeComment")
	 private String chargeComment;
	
	 @XStreamAlias("a:Amount")
	 private double amount;
	
	 @XStreamAlias("a:ChargeStatus")
	 private int chargeStatus;
	
	 @XStreamAlias("a:IsRefundable")
	 private int isRefundable;
	
	 @XStreamAlias("a:ExchangeRate")
	 private String exchangeRate;
	
	 @XStreamAlias("a:ExchangeRateDate")
	 private Date exchangeRateDate;
	 
	 @XStreamAlias("a:OriginalAmount")
	 private double originalAmount;
	
	 @XStreamAlias("a:Description")
	 private String description;
	
	 @XStreamAlias("a:StatusReasonID")
	 private int statusReasonID;
	
	 @XStreamAlias("a:IsSSR")
	 private boolean isSSR;
	
	 @XStreamAlias("a:PaymentNumber")
	 private String paymentNumber;
	 
	 @XStreamAlias("a:LogicalFlightID")
	 private String logicalFlightID;
	
	 @XStreamAlias("a:IncludeInPenalty")
	 private boolean includeInPenalty;
	
	 @XStreamAlias("a:TaxChargeID")
	 private String taxChargeID;
	 
	 @XStreamAlias("a:TaxIncludedInFare")
	 private boolean taxIncludedInFare;
	
	 @XStreamAlias("a:Commission")
	 private String commission;
	
	 @XStreamAlias("a:ResChannelId")
	 private String resChannelId;
	
	 @XStreamAlias("a:CommissionDueRptCurrency")
	 private String commissionDueRptCurrency;
	
	 @XStreamAlias("a:CommissionDueResCurrency")
	 private String commissionDueResCurrency;
	
	 @XStreamAlias("a:NonRefundableResCurrency")
	 private String nonRefundableResCurrency;
	
	 @XStreamAlias("a:NonRefundableRptCurrency")
	 private String nonRefundableRptCurrency;
	
	 @XStreamAlias("a:RefundableResCurrency")
	 private String refundableResCurrency;
	
	 @XStreamAlias("a:RefundableRptCurrency")
	 private String refundableRptCurrency;
	
	 @XStreamAlias("a:PenaltyResCurrency")
	 private String penaltyResCurrency;
	
	 @XStreamAlias("a:PenaltyRptCurrency")
	 private String penaltyRptCurrency;

	 @XStreamAlias("a:Commissionable")
	 private boolean commissionable;
	
	 @XStreamAlias("a:NonDiscountedResAmt")
	 private double nonDiscountedResAmt;
	
	 @XStreamAlias("a:NonDiscountedRptAmt")
	 private double nonDiscountedRptAmt;

	 @XStreamAlias("a:Bundled")
	 private boolean bundled;
	
	 @XStreamAlias("a:TaxIsRefundable")
	 private boolean taxIsRefundable;
	
	 @XStreamAlias("a:TaxIsCommissionable")
	 private boolean taxIsCommissionable;
	
	 @XStreamAlias("a:ServiceIsRefundable")
	 private boolean serviceIsRefundable;
	
	 @XStreamAlias("a:ServiceIsCommissionable")
	 private boolean serviceIsCommissionable;
	
	 @XStreamAlias("a:ReservationPaymentMaps")
	 private ReservationPaymentMaps reservationPaymentMaps;
	
	 @XStreamAlias("a:PenaltyChargeID")
	 private String penaltyChargeID;
	
	 @XStreamAlias("a:PenaltyTypeID")
	 private String penaltyTypeID;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getReservationChargeID() {
		return reservationChargeID;
	}

	public void setReservationChargeID(String reservationChargeID) {
		this.reservationChargeID = reservationChargeID;
	}

	public String getOriginalChargeID() {
		return originalChargeID;
	}

	public void setOriginalChargeID(String originalChargeID) {
		this.originalChargeID = originalChargeID;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getRefundPersonOrgID() {
		return refundPersonOrgID;
	}

	public void setRefundPersonOrgID(String refundPersonOrgID) {
		this.refundPersonOrgID = refundPersonOrgID;
	}

	public String getRefundType() {
		return refundType;
	}

	public void setRefundType(String refundType) {
		this.refundType = refundType;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getOriginalCurrency() {
		return originalCurrency;
	}

	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public String getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public Date getBillDate() {
		return billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public int getRpm() {
		return rpm;
	}

	public void setRpm(int rpm) {
		this.rpm = rpm;
	}

	public String getTaxId() {
		return taxId;
	}

	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}

	public String getChargeComment() {
		return chargeComment;
	}

	public void setChargeComment(String chargeComment) {
		this.chargeComment = chargeComment;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public int getChargeStatus() {
		return chargeStatus;
	}

	public void setChargeStatus(int chargeStatus) {
		this.chargeStatus = chargeStatus;
	}

	public int getIsRefundable() {
		return isRefundable;
	}

	public void setIsRefundable(int isRefundable) {
		this.isRefundable = isRefundable;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Date getExchangeRateDate() {
		return exchangeRateDate;
	}

	public void setExchangeRateDate(Date exchangeRateDate) {
		this.exchangeRateDate = exchangeRateDate;
	}

	public double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(double originalAmount) {
		this.originalAmount = originalAmount;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStatusReasonID() {
		return statusReasonID;
	}

	public void setStatusReasonID(int statusReasonID) {
		this.statusReasonID = statusReasonID;
	}

	public boolean isSSR() {
		return isSSR;
	}

	public void setSSR(boolean isSSR) {
		this.isSSR = isSSR;
	}

	public String getPaymentNumber() {
		return paymentNumber;
	}

	public void setPaymentNumber(String paymentNumber) {
		this.paymentNumber = paymentNumber;
	}

	public String getLogicalFlightID() {
		return logicalFlightID;
	}

	public void setLogicalFlightID(String logicalFlightID) {
		this.logicalFlightID = logicalFlightID;
	}

	public boolean isIncludeInPenalty() {
		return includeInPenalty;
	}

	public void setIncludeInPenalty(boolean includeInPenalty) {
		this.includeInPenalty = includeInPenalty;
	}

	public String getTaxChargeID() {
		return taxChargeID;
	}

	public void setTaxChargeID(String taxChargeID) {
		this.taxChargeID = taxChargeID;
	}

	public boolean isTaxIncludedInFare() {
		return taxIncludedInFare;
	}

	public void setTaxIncludedInFare(boolean taxIncludedInFare) {
		this.taxIncludedInFare = taxIncludedInFare;
	}

	public String getCommission() {
		return commission;
	}

	public void setCommission(String commission) {
		this.commission = commission;
	}

	public String getResChannelId() {
		return resChannelId;
	}

	public void setResChannelId(String resChannelId) {
		this.resChannelId = resChannelId;
	}

	public String getCommissionDueRptCurrency() {
		return commissionDueRptCurrency;
	}

	public void setCommissionDueRptCurrency(String commissionDueRptCurrency) {
		this.commissionDueRptCurrency = commissionDueRptCurrency;
	}

	public String getCommissionDueResCurrency() {
		return commissionDueResCurrency;
	}

	public void setCommissionDueResCurrency(String commissionDueResCurrency) {
		this.commissionDueResCurrency = commissionDueResCurrency;
	}

	public String getNonRefundableResCurrency() {
		return nonRefundableResCurrency;
	}

	public void setNonRefundableResCurrency(String nonRefundableResCurrency) {
		this.nonRefundableResCurrency = nonRefundableResCurrency;
	}

	public String getNonRefundableRptCurrency() {
		return nonRefundableRptCurrency;
	}

	public void setNonRefundableRptCurrency(String nonRefundableRptCurrency) {
		this.nonRefundableRptCurrency = nonRefundableRptCurrency;
	}

	public String getRefundableResCurrency() {
		return refundableResCurrency;
	}

	public void setRefundableResCurrency(String refundableResCurrency) {
		this.refundableResCurrency = refundableResCurrency;
	}

	public String getRefundableRptCurrency() {
		return refundableRptCurrency;
	}

	public void setRefundableRptCurrency(String refundableRptCurrency) {
		this.refundableRptCurrency = refundableRptCurrency;
	}

	public String getPenaltyResCurrency() {
		return penaltyResCurrency;
	}

	public void setPenaltyResCurrency(String penaltyResCurrency) {
		this.penaltyResCurrency = penaltyResCurrency;
	}

	public String getPenaltyRptCurrency() {
		return penaltyRptCurrency;
	}

	public void setPenaltyRptCurrency(String penaltyRptCurrency) {
		this.penaltyRptCurrency = penaltyRptCurrency;
	}

	public boolean isCommissionable() {
		return commissionable;
	}

	public void setCommissionable(boolean commissionable) {
		this.commissionable = commissionable;
	}

	public double getNonDiscountedResAmt() {
		return nonDiscountedResAmt;
	}

	public void setNonDiscountedResAmt(double nonDiscountedResAmt) {
		this.nonDiscountedResAmt = nonDiscountedResAmt;
	}

	public double getNonDiscountedRptAmt() {
		return nonDiscountedRptAmt;
	}

	public void setNonDiscountedRptAmt(double nonDiscountedRptAmt) {
		this.nonDiscountedRptAmt = nonDiscountedRptAmt;
	}

	public boolean isBundled() {
		return bundled;
	}

	public void setBundled(boolean bundled) {
		this.bundled = bundled;
	}

	public boolean isTaxIsRefundable() {
		return taxIsRefundable;
	}

	public void setTaxIsRefundable(boolean taxIsRefundable) {
		this.taxIsRefundable = taxIsRefundable;
	}

	public boolean isTaxIsCommissionable() {
		return taxIsCommissionable;
	}

	public void setTaxIsCommissionable(boolean taxIsCommissionable) {
		this.taxIsCommissionable = taxIsCommissionable;
	}

	public boolean isServiceIsRefundable() {
		return serviceIsRefundable;
	}

	public void setServiceIsRefundable(boolean serviceIsRefundable) {
		this.serviceIsRefundable = serviceIsRefundable;
	}

	public boolean isServiceIsCommissionable() {
		return serviceIsCommissionable;
	}

	public void setServiceIsCommissionable(boolean serviceIsCommissionable) {
		this.serviceIsCommissionable = serviceIsCommissionable;
	}

	public ReservationPaymentMaps getReservationPaymentMaps() {
		return reservationPaymentMaps;
	}

	public void setReservationPaymentMaps(ReservationPaymentMaps reservationPaymentMaps) {
		this.reservationPaymentMaps = reservationPaymentMaps;
	}

	public String getPenaltyChargeID() {
		return penaltyChargeID;
	}

	public void setPenaltyChargeID(String penaltyChargeID) {
		this.penaltyChargeID = penaltyChargeID;
	}

	public String getPenaltyTypeID() {
		return penaltyTypeID;
	}

	public void setPenaltyTypeID(String penaltyTypeID) {
		this.penaltyTypeID = penaltyTypeID;
	}
		
}

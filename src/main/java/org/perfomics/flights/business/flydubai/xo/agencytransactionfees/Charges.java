package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:Charges")
public class Charges {
	
	@XStreamImplicit
	private List<Charge> chargeList=new ArrayList<Charge>();

	public List<Charge> getChargeList() {
		return chargeList;
	}

	public void addChargeList(Charge charge) {
		chargeList.add(charge);
	}
	


}

package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("a:ContactInfo")
public class ContactInfo {

	@XStreamOmitField
	private String email;

	@XStreamOmitField
	private String workPhone;

	@XStreamAlias("a:ContactID")
	private String contactID;

	@XStreamAlias("a:Key")
	private String key;

	@XStreamAlias("a:PersonOrgID")
	private String personOrgID;

	@XStreamAlias("a:ContactField")
	private String contactField;

	@XStreamAlias("a:ContactType")
	private String contactType;

	@XStreamAlias("a:Extension")
	private String extension;

	@XStreamAlias("a:CountryCode")
	private String countryCode;

	@XStreamAlias("a:AreaCode")
	private String areaCode;

	@XStreamAlias("a:PhoneNumber")
	private String phoneNumber;

	@XStreamAlias("a:Display")
	private String display;

	@XStreamAlias("a:PreferredContactMethod")
	private boolean preferredContactMethod;

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPersonOrgID() {
		return personOrgID;
	}

	public void setPersonOrgID(String personOrgID) {
		this.personOrgID = personOrgID;
	}

	public String getContactField() {
		return contactField;
	}

	public void setContactField(String contactField) {
		this.contactField = contactField;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public boolean isPreferredContactMethod() {
		return preferredContactMethod;
	}

	public void setPreferredContactMethod(boolean preferredContactMethod) {
		this.preferredContactMethod = preferredContactMethod;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getWorkPhone() {
		return workPhone;
	}

	public void setWorkPhone(String workPhone) {
		this.workPhone = workPhone;
	}

}

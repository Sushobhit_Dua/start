package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:ContactInfos")
public class ContactInfos {
	
	@XStreamImplicit
	private List<ContactInfo> contactInfoList=new ArrayList<ContactInfo>();

	public List<ContactInfo> getContactInfoList() {
		return contactInfoList;
	}

	public void addContactInfoList(ContactInfo contactInfo) {
		contactInfoList.add(contactInfo);
	}
	

}

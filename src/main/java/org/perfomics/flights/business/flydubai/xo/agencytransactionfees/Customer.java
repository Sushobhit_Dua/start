package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:Customer")
public class Customer {
	
	 @XStreamAlias("a:Key")
	 private String key;
	
	 @XStreamAlias("a:AirlinePersons")
	 private AirlinePersons airlinePersons;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public AirlinePersons getAirlinePersons() {
		return airlinePersons;
	}

	public void setAirlinePersons(AirlinePersons airlinePersons) {
		this.airlinePersons = airlinePersons;
	}
	
}

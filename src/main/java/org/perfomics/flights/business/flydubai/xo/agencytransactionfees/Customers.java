package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:Customers")
public class Customers {
	
	@XStreamImplicit
	private List<Customer> customerList=new ArrayList<Customer>();

	public List<Customer> getCustomerList() {
		return customerList;
	}

	public void addCustomerList(Customer customer) {
		customerList.add(customer);
	}

}

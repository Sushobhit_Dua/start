package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:History")
public class History {
	
	@XStreamImplicit
	private List<History> historyList=new ArrayList<History>();
	
	public List<History> getHistoryList() {
		return historyList;
	}

	public void addHistoryList(History history) {
		historyList.add(history);
	}

	
	 @XStreamAlias("a:HistoryID")
	 private String historyID;

	 @XStreamAlias("a:Key")
	 private String key;
	
	 @XStreamAlias("a:RecordNumber")
	 private String recordNumber;

	 @XStreamAlias("a:HistoryType")
	 private String historyType;

	 @XStreamAlias("a:ActivityDate")
	 private Date activityDate;

	 @XStreamAlias("a:Passenger")
	 private String passenger;

	 @XStreamAlias("a:FirstName")
	 private String firstName;
	
	 @XStreamAlias("a:LastName")
	 private String lastName;

	 @XStreamAlias("a:Segment")
	 private String segment;

	 @XStreamAlias("a:UserIPAddress")
	 private String userIPAddress;
	
	 @XStreamAlias("a:Action")
	 private String action;

	 @XStreamAlias("a:UserID")
	 private String userID;
	
	 @XStreamAlias("a:PersonOrgID")
	 private String personOrgID;

	 @XStreamAlias("a:Details")
	 private String details;

	 @XStreamAlias("a:ReservationChannelID")
	 private String reservationChannelID;


	public String getHistoryID() {
		return historyID;
	}

	public void setHistoryID(String historyID) {
		this.historyID = historyID;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(String recordNumber) {
		this.recordNumber = recordNumber;
	}

	public String getHistoryType() {
		return historyType;
	}

	public void setHistoryType(String historyType) {
		this.historyType = historyType;
	}

	public Date getActivityDate() {
		return activityDate;
	}

	public void setActivityDate(Date activityDate) {
		this.activityDate = activityDate;
	}

	public String getPassenger() {
		return passenger;
	}

	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public String getUserIPAddress() {
		return userIPAddress;
	}

	public void setUserIPAddress(String userIPAddress) {
		this.userIPAddress = userIPAddress;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getPersonOrgID() {
		return personOrgID;
	}

	public void setPersonOrgID(String personOrgID) {
		this.personOrgID = personOrgID;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getReservationChannelID() {
		return reservationChannelID;
	}

	public void setReservationChannelID(String reservationChannelID) {
		this.reservationChannelID = reservationChannelID;
	}
	
}

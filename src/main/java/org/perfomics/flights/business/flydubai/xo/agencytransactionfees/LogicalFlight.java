package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:LogicalFlight")
public class LogicalFlight {

	@XStreamImplicit
	private List<LogicalFlight> logicalFlightList = new ArrayList<LogicalFlight>();

	public List<LogicalFlight> getLogicalFlightList() {
		return logicalFlightList;
	}

	public void addLogicalFlightList(LogicalFlight logicalFlight) {
		logicalFlightList.add(logicalFlight);
	}

	@XStreamAlias("a:Key")
	private String key;

	@XStreamAlias("a:RecordNumber")
	private int recordNumber;

	@XStreamAlias("a:LogicalFlightID")
	private int logicalFlightID;

	@XStreamAlias("a:DepartureDate")
	private Date departureDate;

	@XStreamAlias("a:Origin")
	private String origin;

	@XStreamAlias("a:OriginDefaultTerminal")
	private String originDefaultTerminal;

	@XStreamAlias("a:OriginName")
	private String originName;

	@XStreamAlias("a:Destination")
	private String destination;

	@XStreamAlias("a:DestinationDefaultTerminal")
	private String destinationDefaultTerminal;

	@XStreamAlias("a:DestinationName")
	private String destinationName;

	@XStreamAlias("a:OriginMetroGroup")
	private String originMetroGroup;

	@XStreamAlias("a:DestinationMetroGroup")
	private String destinationMetroGroup;

	@XStreamAlias("a:SellingCarrier")
	private String sellingCarrier;

	@XStreamAlias("a:OperatingCarrier")
	private String operatingCarrier;

	@XStreamAlias("a:OperatingFlightNumber")
	private String operatingFlightNumber;

	@XStreamAlias("a:DepartureTime")
	private Date departureTime;

	@XStreamAlias("a:Arrivaltime")
	private Date arrivaltime;

	@XStreamAlias("a:PackageItemID")
	private String packageItemID;

	@XStreamAlias("a:PackageItemName")
	private String packageItemName;

	@XStreamAlias("a:PackageItemDescription")
	private String packageItemDescription;

	@XStreamAlias("a:PackageItemBookDate")
	private Date packageItemBookDate;

	@XStreamAlias("a:PackageItemStartDate")
	private Date packageItemStartDate;

	@XStreamAlias("a:PackageItemEndDate")
	private Date packageItemEndDate;

	@XStreamAlias("a:VendorId")
	private String vendorId;

	@XStreamAlias("a:VendorName")
	private String vendorName;

	@XStreamAlias("a:Active")
	private boolean active;

	@XStreamAlias("a:VendorDescription")
	private String vendorDescription;

	@XStreamAlias("a:UIDisplayValue")
	private String uiDisplayValue;

	@XStreamAlias("a:PhysicalFlights")
	private PhysicalFlights physicalFlights;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public PhysicalFlights getPhysicalFlights() {
		return physicalFlights;
	}

	public void setPhysicalFlights(PhysicalFlights physicalFlights) {
		this.physicalFlights = physicalFlights;
	}

	public int getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(int recordNumber) {
		this.recordNumber = recordNumber;
	}

	public int getLogicalFlightID() {
		return logicalFlightID;
	}

	public void setLogicalFlightID(int logicalFlightID) {
		this.logicalFlightID = logicalFlightID;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getOriginDefaultTerminal() {
		return originDefaultTerminal;
	}

	public void setOriginDefaultTerminal(String originDefaultTerminal) {
		this.originDefaultTerminal = originDefaultTerminal;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationDefaultTerminal() {
		return destinationDefaultTerminal;
	}

	public void setDestinationDefaultTerminal(String destinationDefaultTerminal) {
		this.destinationDefaultTerminal = destinationDefaultTerminal;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public String getOriginMetroGroup() {
		return originMetroGroup;
	}

	public void setOriginMetroGroup(String originMetroGroup) {
		this.originMetroGroup = originMetroGroup;
	}

	public String getDestinationMetroGroup() {
		return destinationMetroGroup;
	}

	public void setDestinationMetroGroup(String destinationMetroGroup) {
		this.destinationMetroGroup = destinationMetroGroup;
	}

	public String getSellingCarrier() {
		return sellingCarrier;
	}

	public void setSellingCarrier(String sellingCarrier) {
		this.sellingCarrier = sellingCarrier;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getOperatingFlightNumber() {
		return operatingFlightNumber;
	}

	public void setOperatingFlightNumber(String operatingFlightNumber) {
		this.operatingFlightNumber = operatingFlightNumber;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getArrivaltime() {
		return arrivaltime;
	}

	public void setArrivaltime(Date arrivaltime) {
		this.arrivaltime = arrivaltime;
	}

	public String getPackageItemID() {
		return packageItemID;
	}

	public void setPackageItemID(String packageItemID) {
		this.packageItemID = packageItemID;
	}

	public String getPackageItemName() {
		return packageItemName;
	}

	public void setPackageItemName(String packageItemName) {
		this.packageItemName = packageItemName;
	}

	public String getPackageItemDescription() {
		return packageItemDescription;
	}

	public void setPackageItemDescription(String packageItemDescription) {
		this.packageItemDescription = packageItemDescription;
	}

	public Date getPackageItemBookDate() {
		return packageItemBookDate;
	}

	public void setPackageItemBookDate(Date packageItemBookDate) {
		this.packageItemBookDate = packageItemBookDate;
	}

	public Date getPackageItemStartDate() {
		return packageItemStartDate;
	}

	public void setPackageItemStartDate(Date packageItemStartDate) {
		this.packageItemStartDate = packageItemStartDate;
	}


	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorName() {
		return vendorName;
	}

	public void setVendorName(String vendorName) {
		this.vendorName = vendorName;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getVendorDescription() {
		return vendorDescription;
	}

	public void setVendorDescription(String vendorDescription) {
		this.vendorDescription = vendorDescription;
	}

	public void setLogicalFlightList(List<LogicalFlight> logicalFlightList) {
		this.logicalFlightList = logicalFlightList;
	}

	public Date getPackageItemEndDate() {
		return packageItemEndDate;
	}

	public void setPackageItemEndDate(Date packageItemEndDate) {
		this.packageItemEndDate = packageItemEndDate;
	}

	public String getUiDisplayValue() {
		return uiDisplayValue;
	}

	public void setUiDisplayValue(String uiDisplayValue) {
		this.uiDisplayValue = uiDisplayValue;
	}

}

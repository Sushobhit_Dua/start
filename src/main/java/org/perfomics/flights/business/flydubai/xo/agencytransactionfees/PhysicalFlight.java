package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:PhysicalFlight")
public class PhysicalFlight {
	
	 @XStreamAlias("a:Key")
	 private String key;
	 
	 @XStreamAlias("a:RecordNumber")
	 private int recordNumber;
	 
	 @XStreamAlias("a:LogicalFlightID")
	 private int logicalFlightID;
	 
	 @XStreamAlias("a:DepartureDate")
	 private Date departureDate;
	
	 @XStreamAlias("a:Origin")
	 private String origin;

	 @XStreamAlias("a:OriginDefaultTerminal")
	 private String originDefaultTerminal;

	 @XStreamAlias("a:OriginName")
	 private String originName;
	
	 @XStreamAlias("a:Destination")
	 private String destination;
	 
	 @XStreamAlias("a:DestinationDefaultTerminal")
	 private String destinationDefaultTerminal;

	 @XStreamAlias("a:DestinationName")
	 private String destinationName;
	
	 @XStreamAlias("a:OriginMetroGroup")
	 private String originMetroGroup;

	 @XStreamAlias("a:DestinationMetroGroup")
	 private String destinationMetroGroup;
	 
	 @XStreamAlias("a:SellingCarrier")
	 private String sellingCarrier;

	 @XStreamAlias("a:OperatingCarrier")
	 private String operatingCarrier;
	
	 @XStreamAlias("a:OperatingFlightNumber")
	 private String operatingFlightNumber;
	 
	 @XStreamAlias("a:DepartureTime")
	 private Date departureTime;

	 @XStreamAlias("a:Arrivaltime")
	 private Date arrivaltime;

	 @XStreamAlias("a:Active")
	 private boolean active;
		
	 @XStreamAlias("a:UIDisplayValue")
	 private String uiDisplayValue;
 
	 @XStreamAlias("a:PhysicalFlightID")
	 private int physicalFlightID;

	 @XStreamAlias("a:CarrierCode")
	 private String carrierCode;
 
	 @XStreamAlias("a:CarrierName")
	 private String carrierName;
 
	 @XStreamAlias("a:FlightNumber")
	 private String flightNumber;
 
	 @XStreamAlias("a:FlightOrder")
	 private String flightOrder;
	
	 @XStreamAlias("a:FlightDuration")
	 private String flightDuration;

	 @XStreamAlias("a:Trip")
	 private String trip;
	 
	 @XStreamAlias("a:Gate")
	 private String gate;
 
	 @XStreamAlias("a:TotalWeight")
	 private String totalWeight;
     
     @XStreamAlias("a:Customers")
 	 private Customers customers;
   
     @XStreamAlias("a:FromTerminal")
	 private String fromTerminal;
     
     @XStreamAlias("a:ToTerminal")
	 private String toTerminal;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getRecordNumber() {
		return recordNumber;
	}

	public void setRecordNumber(int recordNumber) {
		this.recordNumber = recordNumber;
	}

	public int getLogicalFlightID() {
		return logicalFlightID;
	}

	public void setLogicalFlightID(int logicalFlightID) {
		this.logicalFlightID = logicalFlightID;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getOriginDefaultTerminal() {
		return originDefaultTerminal;
	}

	public void setOriginDefaultTerminal(String originDefaultTerminal) {
		this.originDefaultTerminal = originDefaultTerminal;
	}

	public String getOriginName() {
		return originName;
	}

	public void setOriginName(String originName) {
		this.originName = originName;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getDestinationDefaultTerminal() {
		return destinationDefaultTerminal;
	}

	public void setDestinationDefaultTerminal(String destinationDefaultTerminal) {
		this.destinationDefaultTerminal = destinationDefaultTerminal;
	}

	public String getDestinationName() {
		return destinationName;
	}

	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}

	public String getOriginMetroGroup() {
		return originMetroGroup;
	}

	public void setOriginMetroGroup(String originMetroGroup) {
		this.originMetroGroup = originMetroGroup;
	}

	public String getDestinationMetroGroup() {
		return destinationMetroGroup;
	}

	public void setDestinationMetroGroup(String destinationMetroGroup) {
		this.destinationMetroGroup = destinationMetroGroup;
	}

	public String getSellingCarrier() {
		return sellingCarrier;
	}

	public void setSellingCarrier(String sellingCarrier) {
		this.sellingCarrier = sellingCarrier;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getOperatingFlightNumber() {
		return operatingFlightNumber;
	}

	public void setOperatingFlightNumber(String operatingFlightNumber) {
		this.operatingFlightNumber = operatingFlightNumber;
	}

	public Date getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(Date departureTime) {
		this.departureTime = departureTime;
	}

	public Date getArrivaltime() {
		return arrivaltime;
	}

	public void setArrivaltime(Date arrivaltime) {
		this.arrivaltime = arrivaltime;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}


	public int getPhysicalFlightID() {
		return physicalFlightID;
	}

	public void setPhysicalFlightID(int physicalFlightID) {
		this.physicalFlightID = physicalFlightID;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}


	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getFlightOrder() {
		return flightOrder;
	}

	public void setFlightOrder(String flightOrder) {
		this.flightOrder = flightOrder;
	}

	public String getFlightDuration() {
		return flightDuration;
	}

	public void setFlightDuration(String flightDuration) {
		this.flightDuration = flightDuration;
	}

	public String getTrip() {
		return trip;
	}

	public void setTrip(String trip) {
		this.trip = trip;
	}

	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getTotalWeight() {
		return totalWeight;
	}

	public void setTotalWeight(String totalWeight) {
		this.totalWeight = totalWeight;
	}

	public Customers getCustomers() {
		return customers;
	}

	public void setCustomers(Customers customers) {
		this.customers = customers;
	}

	public String getFromTerminal() {
		return fromTerminal;
	}

	public void setFromTerminal(String fromTerminal) {
		this.fromTerminal = fromTerminal;
	}

	public String getToTerminal() {
		return toTerminal;
	}

	public void setToTerminal(String toTerminal) {
		this.toTerminal = toTerminal;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getUiDisplayValue() {
		return uiDisplayValue;
	}

	public void setUiDisplayValue(String uiDisplayValue) {
		this.uiDisplayValue = uiDisplayValue;
	}
	
}

package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:PhysicalFlights")
public class PhysicalFlights {

	@XStreamImplicit
	private List<PhysicalFlight> physicalFlightList=new ArrayList<PhysicalFlight>();

	public List<PhysicalFlight> getPhysicalFlightList() {
		return physicalFlightList;
	}

	public void addPhysicalFlightList(PhysicalFlight physicalFlight) {
		physicalFlightList.add(physicalFlight);
	}


}

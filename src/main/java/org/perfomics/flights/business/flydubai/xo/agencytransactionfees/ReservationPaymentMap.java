package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:ReservationPaymentMap")
public class ReservationPaymentMap {

	 @XStreamAlias("a:Key")
	 private String key;
	 
	 @XStreamAlias("a:ReservationPaymentID")
	 private String reservationPaymentID;
	
	 @XStreamAlias("a:AmountApplied")
	 private double amountApplied;

	 @XStreamAlias("a:ReservationChargeID")
	 private String reservationChargeID;
 
	 @XStreamAlias("a:DatePaid")
	 private Date datePaid;

	 @XStreamAlias("a:OriginalReferencePaymentID")
	 private String originalReferencePaymentID;
	 
	 @XStreamAlias("a:RefundAmount")
	 private String refundAmount;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getReservationPaymentID() {
		return reservationPaymentID;
	}

	public void setReservationPaymentID(String reservationPaymentID) {
		this.reservationPaymentID = reservationPaymentID;
	}

	public double getAmountApplied() {
		return amountApplied;
	}

	public void setAmountApplied(double amountApplied) {
		this.amountApplied = amountApplied;
	}

	public String getReservationChargeID() {
		return reservationChargeID;
	}

	public void setReservationChargeID(String reservationChargeID) {
		this.reservationChargeID = reservationChargeID;
	}

	public Date getDatePaid() {
		return datePaid;
	}

	public void setDatePaid(Date datePaid) {
		this.datePaid = datePaid;
	}

	public String getOriginalReferencePaymentID() {
		return originalReferencePaymentID;
	}

	public void setOriginalReferencePaymentID(String originalReferencePaymentID) {
		this.originalReferencePaymentID = originalReferencePaymentID;
	}

	public String getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(String refundAmount) {
		this.refundAmount = refundAmount;
	}
 

}

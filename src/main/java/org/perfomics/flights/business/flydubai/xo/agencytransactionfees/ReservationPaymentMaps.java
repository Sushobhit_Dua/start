package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:ReservationPaymentMaps")
public class ReservationPaymentMaps {
	
	@XStreamImplicit
	private List<ReservationPaymentMap> reservationMapList=new ArrayList<ReservationPaymentMap>();

	public List<ReservationPaymentMap> getReservationMapList() {
		return reservationMapList;
	}

	public void addReservationMapList(ReservationPaymentMap reservationMap) {
		reservationMapList.add(reservationMap);
	}


}

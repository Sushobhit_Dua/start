package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:SeatAssignment")
public class SeatAssignment {
	
	@XStreamAlias("a:Key")
	 private String key;
	 
	 @XStreamAlias("a:PhysicalFlightID")
	 private int physicalFlightID;
	
	 @XStreamAlias("a:ActualDepartureDate")
	 private Date actualDepartureDate;

	 @XStreamAlias("a:FlightNumber")
	 private String flightNumber;
	
	 @XStreamAlias("a:ScheduledDeparturetime")
	 private String scheduledDeparturetime;

	 @XStreamAlias("a:ScheduledArrivaltime")
	 private String scheduledArrivaltime;

	 @XStreamAlias("a:BoardingPassNumber")
	 private String boardingPassNumber;
	
	 @XStreamAlias("a:Seat")
	 private String seat;
	
	 @XStreamAlias("a:RowNumber")
	 private String rowNumber;
	
	 @XStreamAlias("a:Gate")
	 private String gate;

	 @XStreamAlias("a:OldSeat")
	 private String oldSeat;
	
	 @XStreamAlias("a:OldRowNumber")
	 private String oldRowNumber;

	 @XStreamAlias("a:Boarded")
	 private boolean boarded;
	
	 @XStreamAlias("a:CheckInAgent")
	 private String checkInAgent;
	 
	 @XStreamAlias("a:CheckInDate")
	 private Date checkInDate;

	 @XStreamAlias("a:CouponStatusIndicator")
	 private String couponStatusIndicator;
	
	 @XStreamAlias("a:BoardingSequence")
	 private String boardingSequence;
	 
	 @XStreamAlias("a:LastModifiedDate")
	 private Date lastModifiedDate;

	 @XStreamAlias("a:BoardingPassPrinted")
	 private String boardingPassPrinted;
	
	 @XStreamAlias("a:FrequentFlyerCarrierCode")
	 private String frequentFlyerCarrierCode;
	
	 @XStreamAlias("a:FrequentFlyerNumber")
	 private String frequentFlyerNumber;
	
	 @XStreamAlias("a:FrequentFlyerSSRCode")
	 private String frequentFlyerSSRCode;
	
	 @XStreamAlias("a:FrequentFlyerMemberTierLevel")
	 private String frequentFlyerMemberTierLevel;
	
	 @XStreamAlias("a:FrequentFlyerMemberRating")
	 private String frequentFlyerMemberRating;
	
	 @XStreamAlias("a:FrequentFlyerNumberActionTracker")
	 private String frequentFlyerNumberActionTracker;
	
	 @XStreamAlias("a:ReservationChannelID")
	 private String reservationChannelID;
	
	 @XStreamAlias("a:FrequentFlyerInfoReservationChannelID")
	 private String frequentFlyerInfoReservationChannelID;
	
	 @XStreamAlias("a:FrequentFlyerNumberLAstModifiedDate")
	 private Date frequentFlyerNumberLAstModifiedDate;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getPhysicalFlightID() {
		return physicalFlightID;
	}

	public void setPhysicalFlightID(int physicalFlightID) {
		this.physicalFlightID = physicalFlightID;
	}

	public Date getActualDepartureDate() {
		return actualDepartureDate;
	}

	public void setActualDepartureDate(Date actualDepartureDate) {
		this.actualDepartureDate = actualDepartureDate;
	}

	public String getFlightNumber() {
		return flightNumber;
	}

	public void setFlightNumber(String flightNumber) {
		this.flightNumber = flightNumber;
	}

	public String getScheduledDeparturetime() {
		return scheduledDeparturetime;
	}

	public void setScheduledDeparturetime(String scheduledDeparturetime) {
		this.scheduledDeparturetime = scheduledDeparturetime;
	}

	public String getScheduledArrivaltime() {
		return scheduledArrivaltime;
	}

	public void setScheduledArrivaltime(String scheduledArrivaltime) {
		this.scheduledArrivaltime = scheduledArrivaltime;
	}

	public String getBoardingPassNumber() {
		return boardingPassNumber;
	}

	public void setBoardingPassNumber(String boardingPassNumber) {
		this.boardingPassNumber = boardingPassNumber;
	}

	public String getSeat() {
		return seat;
	}

	public void setSeat(String seat) {
		this.seat = seat;
	}

	public String getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(String rowNumber) {
		this.rowNumber = rowNumber;
	}

	public String getGate() {
		return gate;
	}

	public void setGate(String gate) {
		this.gate = gate;
	}

	public String getOldSeat() {
		return oldSeat;
	}

	public void setOldSeat(String oldSeat) {
		this.oldSeat = oldSeat;
	}

	public String getOldRowNumber() {
		return oldRowNumber;
	}

	public void setOldRowNumber(String oldRowNumber) {
		this.oldRowNumber = oldRowNumber;
	}

	public boolean isBoarded() {
		return boarded;
	}

	public void setBoarded(boolean boarded) {
		this.boarded = boarded;
	}

	public String getCheckInAgent() {
		return checkInAgent;
	}

	public void setCheckInAgent(String checkInAgent) {
		this.checkInAgent = checkInAgent;
	}

	public Date getCheckInDate() {
		return checkInDate;
	}

	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

	public String getCouponStatusIndicator() {
		return couponStatusIndicator;
	}

	public void setCouponStatusIndicator(String couponStatusIndicator) {
		this.couponStatusIndicator = couponStatusIndicator;
	}

	public String getBoardingSequence() {
		return boardingSequence;
	}

	public void setBoardingSequence(String boardingSequence) {
		this.boardingSequence = boardingSequence;
	}

	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}

	public String getBoardingPassPrinted() {
		return boardingPassPrinted;
	}

	public void setBoardingPassPrinted(String boardingPassPrinted) {
		this.boardingPassPrinted = boardingPassPrinted;
	}

	public String getFrequentFlyerCarrierCode() {
		return frequentFlyerCarrierCode;
	}

	public void setFrequentFlyerCarrierCode(String frequentFlyerCarrierCode) {
		this.frequentFlyerCarrierCode = frequentFlyerCarrierCode;
	}

	public String getFrequentFlyerNumber() {
		return frequentFlyerNumber;
	}

	public void setFrequentFlyerNumber(String frequentFlyerNumber) {
		this.frequentFlyerNumber = frequentFlyerNumber;
	}

	public String getFrequentFlyerSSRCode() {
		return frequentFlyerSSRCode;
	}

	public void setFrequentFlyerSSRCode(String frequentFlyerSSRCode) {
		this.frequentFlyerSSRCode = frequentFlyerSSRCode;
	}

	public String getFrequentFlyerMemberTierLevel() {
		return frequentFlyerMemberTierLevel;
	}

	public void setFrequentFlyerMemberTierLevel(String frequentFlyerMemberTierLevel) {
		this.frequentFlyerMemberTierLevel = frequentFlyerMemberTierLevel;
	}

	public String getFrequentFlyerMemberRating() {
		return frequentFlyerMemberRating;
	}

	public void setFrequentFlyerMemberRating(String frequentFlyerMemberRating) {
		this.frequentFlyerMemberRating = frequentFlyerMemberRating;
	}

	public String getFrequentFlyerNumberActionTracker() {
		return frequentFlyerNumberActionTracker;
	}

	public void setFrequentFlyerNumberActionTracker(String frequentFlyerNumberActionTracker) {
		this.frequentFlyerNumberActionTracker = frequentFlyerNumberActionTracker;
	}

	public String getReservationChannelID() {
		return reservationChannelID;
	}

	public void setReservationChannelID(String reservationChannelID) {
		this.reservationChannelID = reservationChannelID;
	}

	public String getFrequentFlyerInfoReservationChannelID() {
		return frequentFlyerInfoReservationChannelID;
	}

	public void setFrequentFlyerInfoReservationChannelID(String frequentFlyerInfoReservationChannelID) {
		this.frequentFlyerInfoReservationChannelID = frequentFlyerInfoReservationChannelID;
	}

	public Date getFrequentFlyerNumberLAstModifiedDate() {
		return frequentFlyerNumberLAstModifiedDate;
	}

	public void setFrequentFlyerNumberLAstModifiedDate(Date frequentFlyerNumberLAstModifiedDate) {
		this.frequentFlyerNumberLAstModifiedDate = frequentFlyerNumberLAstModifiedDate;
	}



}

package org.perfomics.flights.business.flydubai.xo.agencytransactionfees;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:SeatAssignments")
public class SeatAssignments {
	
	@XStreamImplicit
	private List<SeatAssignment> seatAssignmentsList=new ArrayList<SeatAssignment>();

	public List<SeatAssignment> getSeatAssignmentsList() {
		return seatAssignmentsList;
	}

	public void addSeatAssignmentsList(SeatAssignment seatAssignments) {
		seatAssignmentsList.add(seatAssignments);
	}

}

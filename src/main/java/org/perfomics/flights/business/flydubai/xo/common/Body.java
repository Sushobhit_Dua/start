package org.perfomics.flights.business.flydubai.xo.common;

import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommission;
import org.perfomics.flights.business.flydubai.xo.agencycommission.RetrieveAgencyCommissionResponse;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFees;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.AssessAgencyTransactionFeesResponse;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePNR;
import org.perfomics.flights.business.flydubai.xo.createpnr.CreatePNRResponse;
import org.perfomics.flights.business.flydubai.xo.exceptions.Fault;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuote;
import org.perfomics.flights.business.flydubai.xo.farequote.RetrieveFareQuoteResponse;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgent;
import org.perfomics.flights.business.flydubai.xo.logintravelagent.LoginTravelAgentResponse;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPayment;
import org.perfomics.flights.business.flydubai.xo.processpnr.ProcessPNRPaymentResponse;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePNR;
import org.perfomics.flights.business.flydubai.xo.retrievepnr.RetrievePNRResponse;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityToken;
import org.perfomics.flights.business.flydubai.xo.securitytoken.RetrieveSecurityTokenResponse;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPNR;
import org.perfomics.flights.business.flydubai.xo.summarypnr.SummaryPNRResponse;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("s:Body")
public class Body {

	@XStreamAlias("s:Fault")
	private Fault fault;

	@XStreamAlias("RetrieveSecurityTokenResponse")
	private RetrieveSecurityTokenResponse securityTokenResponse;

	@XStreamAlias("tem:RetrieveSecurityToken")
	private RetrieveSecurityToken securityToken;

	@XStreamAlias("tem:LoginTravelAgent")
	private LoginTravelAgent loginTravelAgent;

	@XStreamAlias("LoginTravelAgentResponse")
	private LoginTravelAgentResponse loginTravelAgentResponse;
	
	@XStreamAlias("tem:RetrieveAgencyCommission")
	private RetrieveAgencyCommission retrieveAgencyCommission;
	
	@XStreamAlias("RetrieveAgencyCommissionResponse")
	private RetrieveAgencyCommissionResponse retrieveAgencyCommissionResponse;
	
	@XStreamAlias("tem:RetrieveFareQuote")
	private RetrieveFareQuote retrieveFareQuote;
	
	@XStreamAlias("RetrieveFareQuoteResponse")
	private RetrieveFareQuoteResponse retrieveFareQuoteResponse;
	
	@XStreamAlias("tem:AssessAgencyTransactionFees")
	private AssessAgencyTransactionFees assessAgencyTransactionFees;

	@XStreamAlias("AssessAgencyTransactionFeesResponse")
	private AssessAgencyTransactionFeesResponse assessAgencyTransactionFeesResponse;

	@XStreamAlias("tem:CreatePNR")
	private CreatePNR createPNR;
	
	@XStreamAlias("CreatePNRResponse")
	private CreatePNRResponse createPNRResponse;
	
	@XStreamAlias("tem:SummaryPNR")
	private SummaryPNR summaryPNR;
	
	@XStreamAlias("SummaryPNRResponse")
	private SummaryPNRResponse summaryPNRResponse;

	@XStreamAlias("tem:ProcessPNRPayment")
	private ProcessPNRPayment processPNRPayment;
	
	@XStreamAlias("ProcessPNRPaymentResponse")
	private ProcessPNRPaymentResponse processPNRPaymentResponse;

	@XStreamAlias("tem:RetrievePNR")
	private RetrievePNR retrievePNR;
	
	@XStreamAlias("RetrievePNRResponse")
	private RetrievePNRResponse retrievePNRResponse;

	public RetrieveSecurityTokenResponse getSecurityTokenResponse() {
		return securityTokenResponse;
	}

	public void setSecurityTokenResponse(RetrieveSecurityTokenResponse securityTokenResponse) {
		this.securityTokenResponse = securityTokenResponse;
	}

	public RetrieveSecurityToken getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(RetrieveSecurityToken securityToken) {
		this.securityToken = securityToken;
	}

	public LoginTravelAgent getLoginTravelAgent() {
		return loginTravelAgent;
	}

	public void setLoginTravelAgent(LoginTravelAgent loginTravelAgent) {
		this.loginTravelAgent = loginTravelAgent;
	}

	public LoginTravelAgentResponse getLoginTravelAgentResponse() {
		return loginTravelAgentResponse;
	}

	public void setLoginTravelAgentResponse(LoginTravelAgentResponse loginTravelAgentResponse) {
		this.loginTravelAgentResponse = loginTravelAgentResponse;
	}

	public RetrieveAgencyCommission getRetrieveAgencyCommission() {
		return retrieveAgencyCommission;
	}

	public void setRetrieveAgencyCommission(RetrieveAgencyCommission retrieveAgencyCommission) {
		this.retrieveAgencyCommission = retrieveAgencyCommission;
	}

	public RetrieveAgencyCommissionResponse getRetrieveAgencyCommissionResponse() {
		return retrieveAgencyCommissionResponse;
	}

	public void setRetrieveAgencyCommissionResponse(RetrieveAgencyCommissionResponse retrieveAgencyCommissionResponse) {
		this.retrieveAgencyCommissionResponse = retrieveAgencyCommissionResponse;
	}

	public RetrieveFareQuote getRetrieveFareQuote() {
		return retrieveFareQuote;
	}

	public void setRetrieveFareQuote(RetrieveFareQuote retrieveFareQuote) {
		this.retrieveFareQuote = retrieveFareQuote;
	}

	public RetrieveFareQuoteResponse getRetrieveFareQuoteResponse() {
		return retrieveFareQuoteResponse;
	}

	public void setRetrieveFareQuoteResponse(RetrieveFareQuoteResponse retrieveFareQuoteResponse) {
		this.retrieveFareQuoteResponse = retrieveFareQuoteResponse;
	}

	public AssessAgencyTransactionFees getAssessAgencyTransactionFees() {
		return assessAgencyTransactionFees;
	}

	public void setAssessAgencyTransactionFees(AssessAgencyTransactionFees assessAgencyTransactionFees) {
		this.assessAgencyTransactionFees = assessAgencyTransactionFees;
	}

	public AssessAgencyTransactionFeesResponse getAssessAgencyTransactionFeesResponse() {
		return assessAgencyTransactionFeesResponse;
	}

	public void setAssessAgencyTransactionFeesResponse(
			AssessAgencyTransactionFeesResponse assessAgencyTransactionFeesResponse) {
		this.assessAgencyTransactionFeesResponse = assessAgencyTransactionFeesResponse;
	}

	public CreatePNR getCreatePNR() {
		return createPNR;
	}

	public void setCreatePNR(CreatePNR createPNR) {
		this.createPNR = createPNR;
	}

	public CreatePNRResponse getCreatePNRResponse() {
		return createPNRResponse;
	}

	public void setCreatePNRResponse(CreatePNRResponse createPNRResponse) {
		this.createPNRResponse = createPNRResponse;
	}

	public SummaryPNR getSummaryPNR() {
		return summaryPNR;
	}

	public void setSummaryPNR(SummaryPNR summaryPNR) {
		this.summaryPNR = summaryPNR;
	}

	public SummaryPNRResponse getSummaryPNRResponse() {
		return summaryPNRResponse;
	}

	public void setSummaryPNRResponse(SummaryPNRResponse summaryPNRResponse) {
		this.summaryPNRResponse = summaryPNRResponse;
	}

	public ProcessPNRPayment getProcessPNRPayment() {
		return processPNRPayment;
	}

	public void setProcessPNRPayment(ProcessPNRPayment processPNRPayment) {
		this.processPNRPayment = processPNRPayment;
	}

	public ProcessPNRPaymentResponse getProcessPNRPaymentResponse() {
		return processPNRPaymentResponse;
	}

	public void setProcessPNRPaymentResponse(ProcessPNRPaymentResponse processPNRPaymentResponse) {
		this.processPNRPaymentResponse = processPNRPaymentResponse;
	}

	public Fault getFault() {
		return fault;
	}

	public void setFault(Fault fault) {
		this.fault = fault;
	}

	public RetrievePNR getRetrievePNR() {
		return retrievePNR;
	}

	public void setRetrievePNR(RetrievePNR retrievePNR) {
		this.retrievePNR = retrievePNR;
	}

	public RetrievePNRResponse getRetrievePNRResponse() {
		return retrievePNRResponse;
	}

	public void setRetrievePNRResponse(RetrievePNRResponse retrievePNRResponse) {
		this.retrievePNRResponse = retrievePNRResponse;
	}


}

package org.perfomics.flights.business.flydubai.xo.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("s:Envelope")
public class Envelope {
		
	@XStreamAlias("xmlns:s")
	@XStreamAsAttribute
	private String xmlnsS;
		
	@XStreamAlias("xmlns:soapenv")
	@XStreamAsAttribute
	private String xmlnsSoapenv;
	
	@XStreamAlias("xmlns:tem")
	@XStreamAsAttribute
	private String xmlnsTem;
	
	@XStreamAlias("xmlns:rad")
	@XStreamAsAttribute
	private String xmlnsRad;
	
	@XStreamAlias("xmlns:rad1")
	@XStreamAsAttribute
	private String xmlnsRad1;
	
	@XStreamAlias("xmlns:rad2")
	@XStreamAsAttribute
	private String xmlnsRad2;

	@XStreamAlias("s:Body")
	private Body body;

	public String getXmlnsS() {
		return xmlnsS;
	}

	public void setXmlnsS(String xmlnsS) {
		this.xmlnsS = xmlnsS;
	}

	public String getXmlnsSoapenv() {
		return xmlnsSoapenv;
	}

	public void setXmlnsSoapenv(String xmlnsSoapenv) {
		this.xmlnsSoapenv = xmlnsSoapenv;
	}

	public String getXmlnsTem() {
		return xmlnsTem;
	}

	public void setXmlnsTem(String xmlnsTem) {
		this.xmlnsTem = xmlnsTem;
	}

	public String getXmlnsRad() {
		return xmlnsRad;
	}

	public void setXmlnsRad(String xmlnsRad) {
		this.xmlnsRad = xmlnsRad;
	}

	public String getXmlnsRad1() {
		return xmlnsRad1;
	}

	public void setXmlnsRad1(String xmlnsRad1) {
		this.xmlnsRad1 = xmlnsRad1;
	}

	public Body getBody() {
		return body;
	}

	public void setBody(Body body) {
		this.body = body;
	}

	public String getXmlnsRad2() {
		return xmlnsRad2;
	}

	public void setXmlnsRad2(String xmlnsRad2) {
		this.xmlnsRad2 = xmlnsRad2;
	}
	
}

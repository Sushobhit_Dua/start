package org.perfomics.flights.business.flydubai.xo.createpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:CreatePNR")
public class CreatePNR {

	@XStreamAlias("tem:CreatePnrRequest")
	private CreatePnrRequest createPnrRequest;

	public CreatePnrRequest getCreatePnrRequest() {
		return createPnrRequest;
	}

	public void setCreatePnrRequest(CreatePnrRequest createPnrRequest) {
		this.createPnrRequest = createPnrRequest;
	}

	
}

package org.perfomics.flights.business.flydubai.xo.createpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("CreatePNRResponse")
public class CreatePNRResponse {
	
	@XStreamAlias("CreatePNRResult")	
	private CreatePNRResult createPNRResult;

	@XStreamAlias("xmlns")	
	@XStreamAsAttribute
	private String xmlns;

	public CreatePNRResult getCreatePNRResult() {
		return createPNRResult;
	}

	public void setCreatePNRResult(CreatePNRResult createPNRResult) {
		this.createPNRResult = createPNRResult;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}
	
}

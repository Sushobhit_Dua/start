package org.perfomics.flights.business.flydubai.xo.createpnr;

import java.util.Date;

import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.Airlines;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfos;
import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.History;
import org.perfomics.flights.business.flydubai.xo.exceptions.Exceptions;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("CreatePNRResult")
public class CreatePNRResult {
	
	@XStreamAlias("xmlns:a")
	@XStreamAsAttribute
	private String xmlnsA;
	
	@XStreamAlias("xmlns:i")
	@XStreamAsAttribute
	private String xmlnsI;
	
	 @XStreamAlias("a:Key")
	 private String key;
	 
	 @XStreamAlias("a:SeriesNumber")
	 private String seriesNumber;
	 
	 @XStreamAlias("a:ConfirmationNumber")
	 private String confirmationNumber;
	 
	 @XStreamAlias("a:BookingAgent")
	 private String bookingAgent;
	 
	 @XStreamAlias("a:CRSCode")
	 private String crsCode;
	 
	 @XStreamAlias("a:TravelGroupID")
	 private int travelGroupID;
	 
	 @XStreamAlias("a:IATANumber")
	 private String iataNumber;
	 
	 @XStreamAlias("a:ExternalAppID")
	 private String externalAppID;
	 
	 @XStreamAlias("a:WebBookingID")
	 private String webBookingID;
	 
	 @XStreamAlias("a:PromotionalID")
	 private String promotionalID;
	 
	 @XStreamAlias("a:PromotionalCode")
	 private String promotionalCode;
	 
	 @XStreamAlias("a:RecieptLanguageID")
	 private int recieptLanguageID;
	 
	 @XStreamAlias("a:ReservationCurrency")
	 private String reservationCurrency;
	 
	 @XStreamAlias("a:ProfileID")
	 private String profileID;
	 
	 @XStreamAlias("a:PNRPin")
	 private String pnrPin;
	 
	 @XStreamAlias("a:BookDate")
	 private Date bookDate;
	 
	 @XStreamAlias("a:ReservationType")
	 private String reservationType;
	 
	 @XStreamAlias("a:TodaysDate")
	 private Date todaysDate;
	 
	 @XStreamAlias("a:LastModified")
	 private Date lastModified;
	 
	 @XStreamAlias("a:ReservationExpirationDate")
	 private Date reservationExpirationDate;
	 
	 @XStreamAlias("a:CorporationID")
	 private String corporationID;
	 
	 @XStreamAlias("a:SecurityGuid")
	 private String securityGuid;
	 
	 @XStreamAlias("a:HasTickets")
	 private boolean hasTickets;
	 
	 @XStreamAlias("a:ValuePackageData")
	 private String valuePackageData;
	 
	 @XStreamAlias("a:UserIPAddress")
	 private String userIPAddress;
	 
	 @XStreamAlias("a:ManageBookingAgent")
	 private String manageBookingAgent;
	 
	 @XStreamAlias("a:HistoricConfirmationNum")
	 private String historicConfirmationNum;
	 
	 @XStreamAlias("a:Cabin")
	 private String cabin;
	 
	 @XStreamAlias("a:ReservationBalance")
	 private double reservationBalance;
	 
	 @XStreamAlias("a:ChangeFee")
	 private double changeFee;
	 
	 @XStreamAlias("a:LogicalFlightCount")
	 private int logicalFlightCount;
	 
	 @XStreamAlias("a:ActivePassengerCount")
	 private int activePassengerCount;
	 
	 @XStreamAlias("a:BalancedReservation")
	 private boolean balancedReservation;
	 
	 @XStreamAlias("a:ReservationFulfillmentRequiredByGMT")
	 private String reservationFulfillmentRequiredByGMT;
	 
	 @XStreamAlias("a:ReservationFulfillmentRequiredByODT")
	 private String reservationFulfillmentRequiredByODT;
	
	@XStreamAlias("a:Airlines")
	private Airlines airlines;
	
	@XStreamAlias("a:Payments")
	private Payments payments;
	
	@XStreamAlias("a:History")
	private History history;

	@XStreamAlias("a:Comments")
	private String comments;
	
	@XStreamAlias("a:GDSHistory")
	private String gdsHistory;
	
	@XStreamAlias("a:ReservationContacts")
	private ReservationContacts reservationContacts;
	
	@XStreamAlias("a:ContactInfos")
	private ContactInfos contactInfos;
	
	@XStreamAlias("a:Vouchers")
	private String vouchers;
	
	@XStreamAlias("a:Cars")
	private String cars;
	
	@XStreamAlias("a:Hotels")
	private String hotels;
	
	@XStreamAlias("a:Packages")
	private String packages;
	
	@XStreamAlias("a:Exceptions")
	private Exceptions exceptions;

	public String getXmlnsA() {
		return xmlnsA;
	}

	public void setXmlnsA(String xmlnsA) {
		this.xmlnsA = xmlnsA;
	}

	public String getXmlnsI() {
		return xmlnsI;
	}

	public void setXmlnsI(String xmlnsI) {
		this.xmlnsI = xmlnsI;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}

	public String getBookingAgent() {
		return bookingAgent;
	}

	public void setBookingAgent(String bookingAgent) {
		this.bookingAgent = bookingAgent;
	}

	public String getCrsCode() {
		return crsCode;
	}

	public void setCrsCode(String crsCode) {
		this.crsCode = crsCode;
	}

	public int getTravelGroupID() {
		return travelGroupID;
	}

	public void setTravelGroupID(int travelGroupID) {
		this.travelGroupID = travelGroupID;
	}

	public String getIataNumber() {
		return iataNumber;
	}

	public void setIataNumber(String iataNumber) {
		this.iataNumber = iataNumber;
	}

	public String getExternalAppID() {
		return externalAppID;
	}

	public void setExternalAppID(String externalAppID) {
		this.externalAppID = externalAppID;
	}

	public String getWebBookingID() {
		return webBookingID;
	}

	public void setWebBookingID(String webBookingID) {
		this.webBookingID = webBookingID;
	}

	public String getPromotionalID() {
		return promotionalID;
	}

	public void setPromotionalID(String promotionalID) {
		this.promotionalID = promotionalID;
	}

	public String getPromotionalCode() {
		return promotionalCode;
	}

	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}

	public int getRecieptLanguageID() {
		return recieptLanguageID;
	}

	public void setRecieptLanguageID(int recieptLanguageID) {
		this.recieptLanguageID = recieptLanguageID;
	}

	public String getReservationCurrency() {
		return reservationCurrency;
	}

	public void setReservationCurrency(String reservationCurrency) {
		this.reservationCurrency = reservationCurrency;
	}

	public String getProfileID() {
		return profileID;
	}

	public void setProfileID(String profileID) {
		this.profileID = profileID;
	}

	public String getPnrPin() {
		return pnrPin;
	}

	public void setPnrPin(String pnrPin) {
		this.pnrPin = pnrPin;
	}

	public Date getBookDate() {
		return bookDate;
	}

	public void setBookDate(Date bookDate) {
		this.bookDate = bookDate;
	}

	public String getReservationType() {
		return reservationType;
	}

	public void setReservationType(String reservationType) {
		this.reservationType = reservationType;
	}

	public Date getTodaysDate() {
		return todaysDate;
	}

	public void setTodaysDate(Date todaysDate) {
		this.todaysDate = todaysDate;
	}

	public Date getLastModified() {
		return lastModified;
	}

	public void setLastModified(Date lastModified) {
		this.lastModified = lastModified;
	}

	public Date getReservationExpirationDate() {
		return reservationExpirationDate;
	}

	public void setReservationExpirationDate(Date reservationExpirationDate) {
		this.reservationExpirationDate = reservationExpirationDate;
	}

	public String getCorporationID() {
		return corporationID;
	}

	public void setCorporationID(String corporationID) {
		this.corporationID = corporationID;
	}

	public String getSecurityGuid() {
		return securityGuid;
	}

	public void setSecurityGuid(String securityGuid) {
		this.securityGuid = securityGuid;
	}

	public boolean isHasTickets() {
		return hasTickets;
	}

	public void setHasTickets(boolean hasTickets) {
		this.hasTickets = hasTickets;
	}

	public String getValuePackageData() {
		return valuePackageData;
	}

	public void setValuePackageData(String valuePackageData) {
		this.valuePackageData = valuePackageData;
	}

	public String getUserIPAddress() {
		return userIPAddress;
	}

	public void setUserIPAddress(String userIPAddress) {
		this.userIPAddress = userIPAddress;
	}

	public String getManageBookingAgent() {
		return manageBookingAgent;
	}

	public void setManageBookingAgent(String manageBookingAgent) {
		this.manageBookingAgent = manageBookingAgent;
	}

	public String getHistoricConfirmationNum() {
		return historicConfirmationNum;
	}

	public void setHistoricConfirmationNum(String historicConfirmationNum) {
		this.historicConfirmationNum = historicConfirmationNum;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public double getReservationBalance() {
		return reservationBalance;
	}

	public void setReservationBalance(double reservationBalance) {
		this.reservationBalance = reservationBalance;
	}

	public double getChangeFee() {
		return changeFee;
	}

	public void setChangeFee(double changeFee) {
		this.changeFee = changeFee;
	}

	public int getLogicalFlightCount() {
		return logicalFlightCount;
	}

	public void setLogicalFlightCount(int logicalFlightCount) {
		this.logicalFlightCount = logicalFlightCount;
	}

	public int getActivePassengerCount() {
		return activePassengerCount;
	}

	public void setActivePassengerCount(int activePassengerCount) {
		this.activePassengerCount = activePassengerCount;
	}

	public boolean isBalancedReservation() {
		return balancedReservation;
	}

	public void setBalancedReservation(boolean balancedReservation) {
		this.balancedReservation = balancedReservation;
	}

	public String getReservationFulfillmentRequiredByGMT() {
		return reservationFulfillmentRequiredByGMT;
	}

	public void setReservationFulfillmentRequiredByGMT(String reservationFulfillmentRequiredByGMT) {
		this.reservationFulfillmentRequiredByGMT = reservationFulfillmentRequiredByGMT;
	}

	public String getReservationFulfillmentRequiredByODT() {
		return reservationFulfillmentRequiredByODT;
	}

	public void setReservationFulfillmentRequiredByODT(String reservationFulfillmentRequiredByODT) {
		this.reservationFulfillmentRequiredByODT = reservationFulfillmentRequiredByODT;
	}

	public Airlines getAirlines() {
		return airlines;
	}

	public void setAirlines(Airlines airlines) {
		this.airlines = airlines;
	}

	public Payments getPayments() {
		return payments;
	}

	public void setPayments(Payments payments) {
		this.payments = payments;
	}

	public History getHistory() {
		return history;
	}

	public void setHistory(History history) {
		this.history = history;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getGdsHistory() {
		return gdsHistory;
	}

	public void setGdsHistory(String gdsHistory) {
		this.gdsHistory = gdsHistory;
	}

	public ReservationContacts getReservationContacts() {
		return reservationContacts;
	}

	public void setReservationContacts(ReservationContacts reservationContacts) {
		this.reservationContacts = reservationContacts;
	}

	public ContactInfos getContactInfos() {
		return contactInfos;
	}

	public void setContactInfos(ContactInfos contactInfos) {
		this.contactInfos = contactInfos;
	}

	public String getVouchers() {
		return vouchers;
	}

	public void setVouchers(String vouchers) {
		this.vouchers = vouchers;
	}

	public String getCars() {
		return cars;
	}

	public void setCars(String cars) {
		this.cars = cars;
	}

	public String getHotels() {
		return hotels;
	}

	public void setHotels(String hotels) {
		this.hotels = hotels;
	}

	public String getPackages() {
		return packages;
	}

	public void setPackages(String packages) {
		this.packages = packages;
	}

	public Exceptions getExceptions() {
		return exceptions;
	}

	public void setExceptions(Exceptions exceptions) {
		this.exceptions = exceptions;
	}


}

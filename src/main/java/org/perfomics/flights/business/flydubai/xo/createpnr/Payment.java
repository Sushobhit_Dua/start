package org.perfomics.flights.business.flydubai.xo.createpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:Payment")
public class Payment {
	
	 @XStreamAlias("a:Key")
	 private String key;
	 
	 @XStreamAlias("a:ReservationPaymentID")
	 private int reservationPaymentID;
	
	 @XStreamAlias("a:CardHolder")
	 private String cardHolder;

	 @XStreamAlias("a:PaymentAmount")
	 private double paymentAmount;
	 
	 @XStreamAlias("a:ExpirationDate")
	 private String expirationDate;

	 @XStreamAlias("a:CardNumber")
	 private String cardNumber;

	 @XStreamAlias("a:CVCode")
	 private String cvcode;

	 @XStreamAlias("a:Authorizationcode")
	 private String authorizationcode;

	 @XStreamAlias("a:TerminalID")
	 private String terminalID;

	 @XStreamAlias("a:CurrencyPaid")
	 private String currencyPaid;

	 @XStreamAlias("a:CheckNumber")
	 private String checkNumber;
	 
	 @XStreamAlias("a:Reference")
	 private String reference;

	 @XStreamAlias("a:Result")
	 private String result;

	 @XStreamAlias("a:TransactionID")
	 private String transactionID;

	 @XStreamAlias("a:ValueCode")
	 private String valueCode;

	 @XStreamAlias("a:TransactionIndicator")
	 private String transactionIndicator;

	 @XStreamAlias("a:SettlementBatch")
	 private String settlementBatch;

	 @XStreamAlias("a:AuthorizedAmount")
	 private String authorizedAmount;

	 @XStreamAlias("a:VoucherNumber")
	 private String voucherNumber;

	 @XStreamAlias("a:FFNumber")
	 private String ffNumber;

	 @XStreamAlias("a:Miles")
	 private String miles;

	 @XStreamAlias("a:PaymentComment")
	 private String paymentComment;

	 @XStreamAlias("a:BaseCurrency")
	 private String baseCurrency;

	 @XStreamAlias("a:BaseAmount")
	 private double baseAmount;

	 @XStreamAlias("a:ExchangeRate")
	 private String exchangeRate;

	 @XStreamAlias("a:ExchangeRateDate")
	 private String exchangeRateDate;

	 @XStreamAlias("a:DocumentReceivedBy")
	 private String documentReceivedBy;

	 @XStreamAlias("a:BatchProcessStart")
	 private String batchProcessStart;

	 @XStreamAlias("a:BatchProcessExtracted")
	 private String batchProcessExtracted;

	 @XStreamAlias("a:TicketCouponNumber")
	 private String ticketCouponNumber;

	 @XStreamAlias("a:ATCANDateProcessed")
	 private String atcANDateProcessed;

	 @XStreamAlias("a:TicketType")
	 private String ticketType;

	 @XStreamAlias("a:GcxID")
	 private String gcxID;

	 @XStreamAlias("a:GcxOptOption")
	 private String gcxOptOption;

	 @XStreamAlias("a:GcxExported")
	 private String gcxExported;

	 @XStreamAlias("a:GcxExportedDate")
	 private String gcxExportedDate;

	 @XStreamAlias("a:OriginalCurrency")
	 private String originalCurrency;

	 @XStreamAlias("a:OriginalAmount")
	 private double originalAmount;
	
	 @XStreamAlias("a:IATANum")
	 private String iataNum;

	 @XStreamAlias("a:ProcessorID")
	 private String processorID;

	 @XStreamAlias("a:MerchantID")
	 private String merchantID;

	 @XStreamAlias("a:ProcessorName")
	 private String processorName;

	 @XStreamAlias("a:OriginalPaymentMethod")
	 private String originalPaymentMethod;

	 @XStreamAlias("a:TransactionStatus")
	 private String transactionStatus;

	 @XStreamAlias("a:ResponseMessage")
	 private String responseMessage;

	 @XStreamAlias("a:ResponseCode")
	 private String responseCode;

	 @XStreamAlias("a:PersonOrgID")
	 private String personOrgID;

	 @XStreamAlias("a:FirstName")
	 private String firstName;

	 @XStreamAlias("a:LastName")
	 private String lastName;

	 @XStreamAlias("a:CompanyName")
	 private String companyName;

	 @XStreamAlias("a:Address")
	 private String address;

	 @XStreamAlias("a:Address2")
	 private String address2;

	 @XStreamAlias("a:City")
	 private String city;

	 @XStreamAlias("a:State")
	 private String state;

	 @XStreamAlias("a:Postal")
	 private String postal;

	 @XStreamAlias("a:Country")
	 private String country;

	 @XStreamAlias("a:PaymentMethod")
	 private String paymentMethod;

	 @XStreamAlias("a:DatePaid")
	 private String datePaid;

	 @XStreamAlias("a:UserData")
	 private String userData;

	 @XStreamAlias("a:CardVerification")
	 private String cardVerification;
	 
	 @XStreamAlias("a:GrossAmtResCurrency")
	 private String grossAmtResCurrency;

	 @XStreamAlias("a:GrossAmtPayCurrency")
	 private String grossAmtPayCurrency;

	 @XStreamAlias("a:NetAmtResCurrency")
	 private String netAmtResCurrency;

	 @XStreamAlias("a:CommissionDeductedResCurrency")
	 private String commissionDeductedResCurrency;

	 @XStreamAlias("a:CommissionDeductedPayCurrency")
	 private String commissionDeductedPayCurrency;

	 @XStreamAlias("a:RptNetFrmResCurrency")
	 private String rptNetFrmResCurrency;

	 @XStreamAlias("a:RptCommissionFromResCurrency")
	 private String rptCommissionFromResCurrency;

	 @XStreamAlias("a:RptCommissionFromPayCurrency")
	 private String rptCommissionFromPayCurrency;

	 @XStreamAlias("a:TotalRefundResCurrency")
	 private String totalRefundResCurrency;

	 @XStreamAlias("a:TotalRefundRptCurrency")
	 private String totalRefundRptCurrency;

	 @XStreamAlias("a:TotalRefundPayCurrency")
	 private String totalRefundPayCurrency;

	 @XStreamAlias("a:RedeemedVoucherAmount")
	 private String redeemedVoucherAmount;

	 @XStreamAlias("a:UserID")
	 private String userID;

	 @XStreamAlias("a:OriginalPaymentID")
	 private String originalPaymentID;

	 @XStreamAlias("a:PaymentReservationChannelID")
	 private String paymentReservationChannelID;

	 @XStreamAlias("a:AncillaryData01")
	 private String ancillaryData01;

	 @XStreamAlias("a:AncillaryData02")
	 private String ancillaryData02;

	 @XStreamAlias("a:AncillaryData03")
	 private String ancillaryData03;

	 @XStreamAlias("a:AncillaryData04")
	 private String ancillaryData04;
	 
	 @XStreamAlias("a:AncillaryData05")
	 private String ancillaryData05;

// Payment Tags of SummaryPNR
	 
	 @XStreamAlias("rad1:GcxOpt")
	 private String gcxOpt;
	 
	 @XStreamAlias("rad1:CardType")
	 private String cardType;

	 @XStreamAlias("rad1:PaymentCurrency")
	 private String paymentCurrency;

	 @XStreamAlias("rad1:ISOCurrency")
	 private String isoCurrency;

	 @XStreamAlias("rad1:IsTACreditCard")
	 private boolean isTACreditCard;
	
	 @XStreamAlias("rad1:BillingCountry")
	 private String billingCountry;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getReservationPaymentID() {
		return reservationPaymentID;
	}

	public void setReservationPaymentID(int reservationPaymentID) {
		this.reservationPaymentID = reservationPaymentID;
	}

	public String getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCvcode() {
		return cvcode;
	}

	public void setCvcode(String cvcode) {
		this.cvcode = cvcode;
	}

	public String getAuthorizationcode() {
		return authorizationcode;
	}

	public void setAuthorizationcode(String authorizationcode) {
		this.authorizationcode = authorizationcode;
	}

	public String getTerminalID() {
		return terminalID;
	}

	public void setTerminalID(String terminalID) {
		this.terminalID = terminalID;
	}

	public String getCurrencyPaid() {
		return currencyPaid;
	}

	public void setCurrencyPaid(String currencyPaid) {
		this.currencyPaid = currencyPaid;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}

	public String getValueCode() {
		return valueCode;
	}

	public void setValueCode(String valueCode) {
		this.valueCode = valueCode;
	}

	public String getTransactionIndicator() {
		return transactionIndicator;
	}

	public void setTransactionIndicator(String transactionIndicator) {
		this.transactionIndicator = transactionIndicator;
	}

	public String getSettlementBatch() {
		return settlementBatch;
	}

	public void setSettlementBatch(String settlementBatch) {
		this.settlementBatch = settlementBatch;
	}

	public String getAuthorizedAmount() {
		return authorizedAmount;
	}

	public void setAuthorizedAmount(String authorizedAmount) {
		this.authorizedAmount = authorizedAmount;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getFfNumber() {
		return ffNumber;
	}

	public void setFfNumber(String ffNumber) {
		this.ffNumber = ffNumber;
	}

	public String getMiles() {
		return miles;
	}

	public void setMiles(String miles) {
		this.miles = miles;
	}

	public String getPaymentComment() {
		return paymentComment;
	}

	public void setPaymentComment(String paymentComment) {
		this.paymentComment = paymentComment;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public String getExchangeRateDate() {
		return exchangeRateDate;
	}

	public void setExchangeRateDate(String exchangeRateDate) {
		this.exchangeRateDate = exchangeRateDate;
	}

	public String getDocumentReceivedBy() {
		return documentReceivedBy;
	}

	public void setDocumentReceivedBy(String documentReceivedBy) {
		this.documentReceivedBy = documentReceivedBy;
	}

	public String getBatchProcessStart() {
		return batchProcessStart;
	}

	public void setBatchProcessStart(String batchProcessStart) {
		this.batchProcessStart = batchProcessStart;
	}

	public String getBatchProcessExtracted() {
		return batchProcessExtracted;
	}

	public void setBatchProcessExtracted(String batchProcessExtracted) {
		this.batchProcessExtracted = batchProcessExtracted;
	}

	public String getTicketCouponNumber() {
		return ticketCouponNumber;
	}

	public void setTicketCouponNumber(String ticketCouponNumber) {
		this.ticketCouponNumber = ticketCouponNumber;
	}

	public String getAtcANDateProcessed() {
		return atcANDateProcessed;
	}

	public void setAtcANDateProcessed(String atcANDateProcessed) {
		this.atcANDateProcessed = atcANDateProcessed;
	}

	public String getTicketType() {
		return ticketType;
	}

	public void setTicketType(String ticketType) {
		this.ticketType = ticketType;
	}

	public String getGcxID() {
		return gcxID;
	}

	public void setGcxID(String gcxID) {
		this.gcxID = gcxID;
	}

	public String getGcxOptOption() {
		return gcxOptOption;
	}

	public void setGcxOptOption(String gcxOptOption) {
		this.gcxOptOption = gcxOptOption;
	}

	public String getGcxExported() {
		return gcxExported;
	}

	public void setGcxExported(String gcxExported) {
		this.gcxExported = gcxExported;
	}

	public String getGcxExportedDate() {
		return gcxExportedDate;
	}

	public void setGcxExportedDate(String gcxExportedDate) {
		this.gcxExportedDate = gcxExportedDate;
	}

	public String getOriginalCurrency() {
		return originalCurrency;
	}

	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(double originalAmount) {
		this.originalAmount = originalAmount;
	}

	public String getIataNum() {
		return iataNum;
	}

	public void setIataNum(String iataNum) {
		this.iataNum = iataNum;
	}

	public String getProcessorID() {
		return processorID;
	}

	public void setProcessorID(String processorID) {
		this.processorID = processorID;
	}

	public String getMerchantID() {
		return merchantID;
	}

	public void setMerchantID(String merchantID) {
		this.merchantID = merchantID;
	}

	public String getProcessorName() {
		return processorName;
	}

	public void setProcessorName(String processorName) {
		this.processorName = processorName;
	}

	public String getOriginalPaymentMethod() {
		return originalPaymentMethod;
	}

	public void setOriginalPaymentMethod(String originalPaymentMethod) {
		this.originalPaymentMethod = originalPaymentMethod;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getPersonOrgID() {
		return personOrgID;
	}

	public void setPersonOrgID(String personOrgID) {
		this.personOrgID = personOrgID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getDatePaid() {
		return datePaid;
	}

	public void setDatePaid(String datePaid) {
		this.datePaid = datePaid;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

	public String getCardVerification() {
		return cardVerification;
	}

	public void setCardVerification(String cardVerification) {
		this.cardVerification = cardVerification;
	}

	public String getGrossAmtResCurrency() {
		return grossAmtResCurrency;
	}

	public void setGrossAmtResCurrency(String grossAmtResCurrency) {
		this.grossAmtResCurrency = grossAmtResCurrency;
	}

	public String getGrossAmtPayCurrency() {
		return grossAmtPayCurrency;
	}

	public void setGrossAmtPayCurrency(String grossAmtPayCurrency) {
		this.grossAmtPayCurrency = grossAmtPayCurrency;
	}

	public String getNetAmtResCurrency() {
		return netAmtResCurrency;
	}

	public void setNetAmtResCurrency(String netAmtResCurrency) {
		this.netAmtResCurrency = netAmtResCurrency;
	}

	public String getCommissionDeductedResCurrency() {
		return commissionDeductedResCurrency;
	}

	public void setCommissionDeductedResCurrency(String commissionDeductedResCurrency) {
		this.commissionDeductedResCurrency = commissionDeductedResCurrency;
	}

	public String getCommissionDeductedPayCurrency() {
		return commissionDeductedPayCurrency;
	}

	public void setCommissionDeductedPayCurrency(String commissionDeductedPayCurrency) {
		this.commissionDeductedPayCurrency = commissionDeductedPayCurrency;
	}

	public String getRptNetFrmResCurrency() {
		return rptNetFrmResCurrency;
	}

	public void setRptNetFrmResCurrency(String rptNetFrmResCurrency) {
		this.rptNetFrmResCurrency = rptNetFrmResCurrency;
	}

	public String getRptCommissionFromResCurrency() {
		return rptCommissionFromResCurrency;
	}

	public void setRptCommissionFromResCurrency(String rptCommissionFromResCurrency) {
		this.rptCommissionFromResCurrency = rptCommissionFromResCurrency;
	}

	public String getRptCommissionFromPayCurrency() {
		return rptCommissionFromPayCurrency;
	}

	public void setRptCommissionFromPayCurrency(String rptCommissionFromPayCurrency) {
		this.rptCommissionFromPayCurrency = rptCommissionFromPayCurrency;
	}

	public String getTotalRefundResCurrency() {
		return totalRefundResCurrency;
	}

	public void setTotalRefundResCurrency(String totalRefundResCurrency) {
		this.totalRefundResCurrency = totalRefundResCurrency;
	}

	public String getTotalRefundRptCurrency() {
		return totalRefundRptCurrency;
	}

	public void setTotalRefundRptCurrency(String totalRefundRptCurrency) {
		this.totalRefundRptCurrency = totalRefundRptCurrency;
	}

	public String getTotalRefundPayCurrency() {
		return totalRefundPayCurrency;
	}

	public void setTotalRefundPayCurrency(String totalRefundPayCurrency) {
		this.totalRefundPayCurrency = totalRefundPayCurrency;
	}

	public String getRedeemedVoucherAmount() {
		return redeemedVoucherAmount;
	}

	public void setRedeemedVoucherAmount(String redeemedVoucherAmount) {
		this.redeemedVoucherAmount = redeemedVoucherAmount;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getOriginalPaymentID() {
		return originalPaymentID;
	}

	public void setOriginalPaymentID(String originalPaymentID) {
		this.originalPaymentID = originalPaymentID;
	}

	public String getPaymentReservationChannelID() {
		return paymentReservationChannelID;
	}

	public void setPaymentReservationChannelID(String paymentReservationChannelID) {
		this.paymentReservationChannelID = paymentReservationChannelID;
	}

	public String getAncillaryData01() {
		return ancillaryData01;
	}

	public void setAncillaryData01(String ancillaryData01) {
		this.ancillaryData01 = ancillaryData01;
	}

	public String getAncillaryData02() {
		return ancillaryData02;
	}

	public void setAncillaryData02(String ancillaryData02) {
		this.ancillaryData02 = ancillaryData02;
	}

	public String getAncillaryData03() {
		return ancillaryData03;
	}

	public void setAncillaryData03(String ancillaryData03) {
		this.ancillaryData03 = ancillaryData03;
	}

	public String getAncillaryData04() {
		return ancillaryData04;
	}

	public void setAncillaryData04(String ancillaryData04) {
		this.ancillaryData04 = ancillaryData04;
	}

	public String getAncillaryData05() {
		return ancillaryData05;
	}

	public void setAncillaryData05(String ancillaryData05) {
		this.ancillaryData05 = ancillaryData05;
	}

	public String getGcxOpt() {
		return gcxOpt;
	}

	public void setGcxOpt(String gcxOpt) {
		this.gcxOpt = gcxOpt;
	}

	public String getCardType() {
		return cardType;
	}

	public void setCardType(String cardType) {
		this.cardType = cardType;
	}

	public String getPaymentCurrency() {
		return paymentCurrency;
	}

	public void setPaymentCurrency(String paymentCurrency) {
		this.paymentCurrency = paymentCurrency;
	}

	public String getIsoCurrency() {
		return isoCurrency;
	}

	public void setIsoCurrency(String isoCurrency) {
		this.isoCurrency = isoCurrency;
	}

	public boolean isTACreditCard() {
		return isTACreditCard;
	}

	public void setTACreditCard(boolean isTACreditCard) {
		this.isTACreditCard = isTACreditCard;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}



}

package org.perfomics.flights.business.flydubai.xo.createpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:Payments")
public class Payments {
	
	 @XStreamAlias("a:Payment")
	 private Payment payment;

	public Payment getPayment() {
		return payment;
	}

	public void setPayment(Payment payment) {
		this.payment = payment;
	}


}

package org.perfomics.flights.business.flydubai.xo.createpnr;

import java.util.Date;

import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfos;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:ReservationContact")
public class ReservationContact {
	
	 @XStreamAlias("a:Key")
	 private String key;

	 @XStreamAlias("a:PersonOrgID")
	 private String personOrgID;

	 @XStreamAlias("a:FirstName")
	 private String firstName;

	 @XStreamAlias("a:LastName")
	 private String lastName;

	 @XStreamAlias("a:MiddleName")
	 private String middleName;

	 @XStreamAlias("a:Age")
	 private String age;

	 @XStreamAlias("a:DOB")
	 private Date dob;

	 @XStreamAlias("a:Gender")
	 private String gender;
	 
	 @XStreamAlias("a:Title")
	 private String title;
	
	 @XStreamAlias("a:NationalityLaguageID")
	 private String nationalityLaguageID;
	
	 @XStreamAlias("a:RelationType")
	 private String relationType;
	
	 @XStreamAlias("a:WBCID")
	 private int wbCID;
	
	 @XStreamAlias("a:PTCID")
	 private int ptcID;

	 @XStreamAlias("a:UseInventory")
	 private boolean useInventory;
	
	 @XStreamAlias("a:Address")
	 private String address;
	
	 @XStreamAlias("a:Address2")
	 private String address2;
	
	 @XStreamAlias("a:City")
	 private String city;
	
	 @XStreamAlias("a:State")
	 private String state;
	
	 @XStreamAlias("a:Postal")
	 private String postal;
	
	 @XStreamAlias("a:Country")
	 private String country;
	
	 @XStreamAlias("a:Company")
	 private String company;
	
	 @XStreamAlias("a:Comments")
	 private String comments;
		 
	 @XStreamAlias("a:Passport")
	 private String passport;
	
	 @XStreamAlias("a:Nationality")
	 private String nationality;
	
	 @XStreamAlias("a:ProfileId")
	 private String profileId;
	
	 @XStreamAlias("a:VendorID")
	 private String vendorID;
	 
	 @XStreamAlias("a:MarketingOptIn")
	 private boolean marketingOptIn;
	 
	 @XStreamAlias("a:RedressNum")
	 private String redressNum;

	 @XStreamAlias("a:KnownTravNum")
	 private String knownTravNum;

	 @XStreamAlias("a:ContactInfos")
	 private ContactInfos contactInfos;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getPersonOrgID() {
		return personOrgID;
	}

	public void setPersonOrgID(String personOrgID) {
		this.personOrgID = personOrgID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNationalityLaguageID() {
		return nationalityLaguageID;
	}

	public void setNationalityLaguageID(String nationalityLaguageID) {
		this.nationalityLaguageID = nationalityLaguageID;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public int getWbCID() {
		return wbCID;
	}

	public void setWbCID(int wbCID) {
		this.wbCID = wbCID;
	}

	public int getPtcID() {
		return ptcID;
	}

	public void setPtcID(int ptcID) {
		this.ptcID = ptcID;
	}

	public boolean isUseInventory() {
		return useInventory;
	}

	public void setUseInventory(boolean useInventory) {
		this.useInventory = useInventory;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public String getVendorID() {
		return vendorID;
	}

	public void setVendorID(String vendorID) {
		this.vendorID = vendorID;
	}

	public boolean isMarketingOptIn() {
		return marketingOptIn;
	}

	public void setMarketingOptIn(boolean marketingOptIn) {
		this.marketingOptIn = marketingOptIn;
	}

	public String getRedressNum() {
		return redressNum;
	}

	public void setRedressNum(String redressNum) {
		this.redressNum = redressNum;
	}

	public String getKnownTravNum() {
		return knownTravNum;
	}

	public void setKnownTravNum(String knownTravNum) {
		this.knownTravNum = knownTravNum;
	}

	public ContactInfos getContactInfos() {
		return contactInfos;
	}

	public void setContactInfos(ContactInfos contactInfos) {
		this.contactInfos = contactInfos;
	}


}

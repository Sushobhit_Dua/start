package org.perfomics.flights.business.flydubai.xo.createpnr;

import java.util.ArrayList;
import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:ReservationContacts")
public class ReservationContacts {

	@XStreamImplicit
	private List<ReservationContact> reservationContactList = new ArrayList<ReservationContact>();

	public List<ReservationContact> getReservationContactList() {
		return reservationContactList;
	}

	public void addReservationContactList(ReservationContact reservationContact) {
		reservationContactList.add(reservationContact);
	}

	
}

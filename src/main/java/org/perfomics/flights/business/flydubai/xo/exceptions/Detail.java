package org.perfomics.flights.business.flydubai.xo.exceptions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("detail")
public class Detail {

	@XStreamAlias("ExceptionDetail")
	private ExceptionDetail exceptionDetail;

	public ExceptionDetail getExceptionDetail() {
		return exceptionDetail;
	}

	public void setExceptionDetail(ExceptionDetail exceptionDetail) {
		this.exceptionDetail = exceptionDetail;
	}
	
}

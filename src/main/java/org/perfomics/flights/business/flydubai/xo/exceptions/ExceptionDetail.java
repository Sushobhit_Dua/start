package org.perfomics.flights.business.flydubai.xo.exceptions;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("ExceptionDetail")
public class ExceptionDetail {

	@XStreamAlias("xmlns")
	@XStreamAsAttribute
	private String xmlns;
	
	@XStreamAlias("xmlns:i")
	@XStreamAsAttribute
	private String xmlnsI;
	
	@XStreamAlias("HelpLink")
	private String helpLink;
	
	@XStreamAlias("InnerException")
	private InnerExcption innerExcption;
	
	@XStreamAlias("Message")
	private String message;
	
	@XStreamAlias("StackTrace")
	private String stackTrace;
	
	@XStreamAlias("Type")
	private String type;

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	public String getXmlnsI() {
		return xmlnsI;
	}

	public void setXmlnsI(String xmlnsI) {
		this.xmlnsI = xmlnsI;
	}

	public String getHelpLink() {
		return helpLink;
	}

	public void setHelpLink(String helpLink) {
		this.helpLink = helpLink;
	}

	public InnerExcption getInnerException() {
		return innerExcption;
	}

	public void setInnerException(InnerExcption innerExcption) {
		this.innerExcption = innerExcption;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	

}

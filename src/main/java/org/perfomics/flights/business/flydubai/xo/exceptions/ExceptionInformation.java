package org.perfomics.flights.business.flydubai.xo.exceptions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("b:ExceptionInformation.Exception")
public class ExceptionInformation {

	@XStreamAlias("b:ExceptionCode")
	private int exceptionCode;
	
	@XStreamAlias("b:ExceptionDescription")
	private String exceptionDescription;
	
	@XStreamAlias("b:ExceptionSource")
	private String exceptionSource;
	
	@XStreamAlias("b:ExceptionLevel")
	private String exceptionLevel;

	public int getExceptionCode() {
		return exceptionCode;
	}

	public void setExceptionCode(int exceptionCode) {
		this.exceptionCode = exceptionCode;
	}

	public String getExceptionDescription() {
		return exceptionDescription;
	}

	public void setExceptionDescription(String exceptionDescription) {
		this.exceptionDescription = exceptionDescription;
	}

	public String getExceptionSource() {
		return exceptionSource;
	}

	public void setExceptionSource(String exceptionSource) {
		this.exceptionSource = exceptionSource;
	}

	public String getExceptionLevel() {
		return exceptionLevel;
	}

	public void setExceptionLevel(String exceptionLevel) {
		this.exceptionLevel = exceptionLevel;
	}

		
}

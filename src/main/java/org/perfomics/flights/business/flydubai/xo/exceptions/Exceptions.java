package org.perfomics.flights.business.flydubai.xo.exceptions;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:Exceptions")
public class Exceptions {
	
	@XStreamAsAttribute
	@XStreamAlias("xmlns:b")
	private String xmlnsB;
	
	@XStreamImplicit
	List<ExceptionInformation> exceptionExceptionInformationList=new ArrayList<ExceptionInformation>();

	public String getXmlnsB() {
		return xmlnsB;
	}

	public void setXmlnsB(String xmlnsB) {
		this.xmlnsB = xmlnsB;
	}

	public List<ExceptionInformation> getExceptionExceptionInformationList() {
		return exceptionExceptionInformationList;
	}

	public void addExceptionExceptionInformationList(ExceptionInformation exceptionExceptionInformation) {
		exceptionExceptionInformationList.add(exceptionExceptionInformation);
	}
	
	
}


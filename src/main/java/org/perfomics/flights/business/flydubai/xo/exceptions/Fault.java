package org.perfomics.flights.business.flydubai.xo.exceptions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("s:Fault")
public class Fault {
	
	@XStreamAlias("faultcode")
	private String faultcode;

	@XStreamAlias("faultstring")
	private String faultstring;

	@XStreamAlias("detail")
	private Detail detail;

	public String getFaultcode() {
		return faultcode;
	}

	public void setFaultcode(String faultcode) {
		this.faultcode = faultcode;
	}

	public String getFaultstring() {
		return faultstring;
	}

	public void setFaultstring(String faultstring) {
		this.faultstring = faultstring;
	}

	public Detail getDetail() {
		return detail;
	}

	public void setDetail(Detail detail) {
		this.detail = detail;
	}


}

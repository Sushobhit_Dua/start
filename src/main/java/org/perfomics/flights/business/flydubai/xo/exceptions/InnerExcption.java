package org.perfomics.flights.business.flydubai.xo.exceptions;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("InnerException")
public class InnerExcption {
	
	@XStreamAlias("HelpLink")
	private String helpLink;
	
	@XStreamAlias("InnerException")
	private InnerExcption innerException;
	
	@XStreamAlias("Message")
	private String message;
	
	@XStreamAlias("StackTrace")
	private String stackTrace;
	
	@XStreamAlias("Type")
	private String type;

	public String getHelpLink() {
		return helpLink;
	}

	public void setHelpLink(String helpLink) {
		this.helpLink = helpLink;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStackTrace() {
		return stackTrace;
	}

	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public InnerExcption getInnerException() {
		return innerException;
	}

	public void setInnerException(InnerExcption innerException) {
		this.innerException = innerException;
	}
	
	

}

package org.perfomics.flights.business.flydubai.xo.farequote;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:ApplicableTaxDetail")
public class ApplicableTaxDetail {
	
	@XStreamAlias("a:TaxID")	
	private int taxID;

	@XStreamAlias("a:Amt")	
	private double amt;

	@XStreamAlias("a:InitiatingTaxID")	
	private int initiatingTaxID;

	@XStreamAlias("a:CommissionAmount")	
	private double commissionAmount;

	public int getTaxID() {
		return taxID;
	}

	public void setTaxID(int taxID) {
		this.taxID = taxID;
	}

	public double getAmt() {
		return amt;
	}

	public void setAmt(double amt) {
		this.amt = amt;
	}

	public int getInitiatingTaxID() {
		return initiatingTaxID;
	}

	public void setInitiatingTaxID(int initiatingTaxID) {
		this.initiatingTaxID = initiatingTaxID;
	}

	public double getCommissionAmount() {
		return commissionAmount;
	}

	public void setCommissionAmount(double commissionAmount) {
		this.commissionAmount = commissionAmount;
	}


}

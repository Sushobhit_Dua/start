package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:ApplicableTaxDetails")
public class ApplicableTaxDetails {
	
	@XStreamImplicit
	private List<ApplicableTaxDetail> applicablelist=new ArrayList<ApplicableTaxDetail>();

	public List<ApplicableTaxDetail> getApplicablelist() {
		return applicablelist;
	}

	public void addApplicablelist(ApplicableTaxDetail applicabletax) {
		applicablelist.add(applicabletax);
	}

}

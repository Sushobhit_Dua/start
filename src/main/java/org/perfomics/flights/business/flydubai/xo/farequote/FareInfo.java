package org.perfomics.flights.business.flydubai.xo.farequote;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:FareInfo")
public class FareInfo {

	@XStreamAlias("a:ApplicableTaxDetails")
	private ApplicableTaxDetails applicableTaxDetails;
	
	@XStreamAlias("a:ReturnFlightSegmentDetails")
	private String returnFlightSegmentDetails;
	
	@XStreamAlias("a:FareID")	
	private int fareID;

	@XStreamAlias("a:FCCode")	
	private String fcCode;
	
	@XStreamAlias("a:FBCode")	
	private String fbCode;
	
	@XStreamAlias("a:BaseFareAmtNoTaxes")	
	private double baseFareAmtNoTaxes;

	@XStreamAlias("a:BaseFareAmt")	
	private double baseFareAmt;

	@XStreamAlias("a:FareAmtNoTaxes")	
	private double fareAmtNoTaxes;

	@XStreamAlias("a:FareAmt")	
	private double fareAmt;

	@XStreamAlias("a:BaseFareAmtInclTax")	
	private double baseFareAmtInclTax;

	@XStreamAlias("a:FareAmtInclTax")	
	private double fareAmtInclTax;
	
	@XStreamAlias("a:PvtFare")	
	private boolean pvtFare;
	
	@XStreamAlias("a:PTCID")	
	private int ptcID;

	@XStreamAlias("a:Cabin")	
	private String cabin;

	@XStreamAlias("a:SeatsAvailable")	
	private int seatsAvailable;

	@XStreamAlias("a:InfantSeatsAvailable")	
	private int infantSeatsAvailable;

	@XStreamAlias("a:FareScheduleID")	
	private int fareScheduleID;

	@XStreamAlias("a:PromotionID")	
	private int promotionID;

	@XStreamAlias("a:RoundTrip")	
	private int roundTrip;
	
	@XStreamAlias("a:DisplayFareAmt")	
	private double displayFareAmt;

	@XStreamAlias("a:DisplayTaxSum")	
	private double displayTaxSum;
	
	@XStreamAlias("a:SpecialMarketed")	
	private boolean specialMarketed;

	@XStreamAlias("a:WaitList")	
	private boolean waitList;
	
	@XStreamAlias("a:SpaceAvailable")	
	private boolean spaceAvailable;

	@XStreamAlias("a:PositiveSpace")	
	private boolean positiveSpace;

	@XStreamAlias("a:PromotionCatID")	
	private double promotionCatID;

	@XStreamAlias("a:CommissionAmount")	
	private double commissionAmount;

	@XStreamAlias("a:PromotionAmount")	
	private double promotionAmount;

	public ApplicableTaxDetails getApplicableTaxDetails() {
		return applicableTaxDetails;
	}

	public void setApplicableTaxDetails(ApplicableTaxDetails applicableTaxDetails) {
		this.applicableTaxDetails = applicableTaxDetails;
	}

	public String getReturnFlightSegmentDetails() {
		return returnFlightSegmentDetails;
	}

	public void setReturnFlightSegmentDetails(String returnFlightSegmentDetails) {
		this.returnFlightSegmentDetails = returnFlightSegmentDetails;
	}

	public int getFareID() {
		return fareID;
	}

	public void setFareID(int fareID) {
		this.fareID = fareID;
	}

	public String getFcCode() {
		return fcCode;
	}

	public void setFcCode(String fcCode) {
		this.fcCode = fcCode;
	}

	public String getFbCode() {
		return fbCode;
	}

	public void setFbCode(String fbCode) {
		this.fbCode = fbCode;
	}

	public double getBaseFareAmtNoTaxes() {
		return baseFareAmtNoTaxes;
	}

	public void setBaseFareAmtNoTaxes(double baseFareAmtNoTaxes) {
		this.baseFareAmtNoTaxes = baseFareAmtNoTaxes;
	}

	public double getBaseFareAmt() {
		return baseFareAmt;
	}

	public void setBaseFareAmt(double baseFareAmt) {
		this.baseFareAmt = baseFareAmt;
	}

	public double getFareAmtNoTaxes() {
		return fareAmtNoTaxes;
	}

	public void setFareAmtNoTaxes(double fareAmtNoTaxes) {
		this.fareAmtNoTaxes = fareAmtNoTaxes;
	}

	public double getFareAmt() {
		return fareAmt;
	}

	public void setFareAmt(double fareAmt) {
		this.fareAmt = fareAmt;
	}

	public double getBaseFareAmtInclTax() {
		return baseFareAmtInclTax;
	}

	public void setBaseFareAmtInclTax(double baseFareAmtInclTax) {
		this.baseFareAmtInclTax = baseFareAmtInclTax;
	}

	public double getFareAmtInclTax() {
		return fareAmtInclTax;
	}

	public void setFareAmtInclTax(double fareAmtInclTax) {
		this.fareAmtInclTax = fareAmtInclTax;
	}

	public boolean isPvtFare() {
		return pvtFare;
	}

	public void setPvtFare(boolean pvtFare) {
		this.pvtFare = pvtFare;
	}

	public int getPtcID() {
		return ptcID;
	}

	public void setPtcID(int ptcID) {
		this.ptcID = ptcID;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public int getSeatsAvailable() {
		return seatsAvailable;
	}

	public void setSeatsAvailable(int seatsAvailable) {
		this.seatsAvailable = seatsAvailable;
	}

	public int getInfantSeatsAvailable() {
		return infantSeatsAvailable;
	}

	public void setInfantSeatsAvailable(int infantSeatsAvailable) {
		this.infantSeatsAvailable = infantSeatsAvailable;
	}

	public int getFareScheduleID() {
		return fareScheduleID;
	}

	public void setFareScheduleID(int fareScheduleID) {
		this.fareScheduleID = fareScheduleID;
	}

	public int getPromotionID() {
		return promotionID;
	}

	public void setPromotionID(int promotionID) {
		this.promotionID = promotionID;
	}

	public int getRoundTrip() {
		return roundTrip;
	}

	public void setRoundTrip(int roundTrip) {
		this.roundTrip = roundTrip;
	}

	public double getDisplayFareAmt() {
		return displayFareAmt;
	}

	public void setDisplayFareAmt(double displayFareAmt) {
		this.displayFareAmt = displayFareAmt;
	}

	public double getDisplayTaxSum() {
		return displayTaxSum;
	}

	public void setDisplayTaxSum(double displayTaxSum) {
		this.displayTaxSum = displayTaxSum;
	}

	public boolean isSpecialMarketed() {
		return specialMarketed;
	}

	public void setSpecialMarketed(boolean specialMarketed) {
		this.specialMarketed = specialMarketed;
	}

	public boolean isWaitList() {
		return waitList;
	}

	public void setWaitList(boolean waitList) {
		this.waitList = waitList;
	}

	public boolean isSpaceAvailable() {
		return spaceAvailable;
	}

	public void setSpaceAvailable(boolean spaceAvailable) {
		this.spaceAvailable = spaceAvailable;
	}

	public boolean isPositiveSpace() {
		return positiveSpace;
	}

	public void setPositiveSpace(boolean positiveSpace) {
		this.positiveSpace = positiveSpace;
	}

	public double getPromotionCatID() {
		return promotionCatID;
	}

	public void setPromotionCatID(double promotionCatID) {
		this.promotionCatID = promotionCatID;
	}

	public double getCommissionAmount() {
		return commissionAmount;
	}

	public void setCommissionAmount(double commissionAmount) {
		this.commissionAmount = commissionAmount;
	}

	public double getPromotionAmount() {
		return promotionAmount;
	}

	public void setPromotionAmount(double promotionAmount) {
		this.promotionAmount = promotionAmount;
	}

}

package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:FareInfos")	
public class FareInfos {
	
	@XStreamImplicit
	private List<FareInfo> fareinfolist=new ArrayList<FareInfo>();

	public List<FareInfo> getFareinfolist() {
		return fareinfolist;
	}

	public void addFareinfolist(FareInfo fareinfo) {
		fareinfolist.add(fareinfo);
	}


}

package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("rad1:FareQuoteDetail")
public class FareQuoteDetail {
       
	@XStreamOmitField
	private int returnOrOneWay;
	
	@XStreamOmitField
	private Date departDate;
		
	@XStreamOmitField
	private Date returnDate;
	
	@XStreamAlias("rad1:FareQuoteRequestInfos")	
 	private FareQuoteRequestInfos fareQuoteRequestInfos;

	@XStreamAlias("rad1:Origin")	
	private String origin;
	
	@XStreamAlias("rad1:Destination")	
	private String destination;

	@XStreamAlias("rad1:UseAirportsNotMetroGroups")	
	private boolean airportsNotMetroGroups;

	@XStreamAlias("rad1:DateOfDeparture")	
	private String departureDate;

	@XStreamAlias("rad1:FareTypeCategory")	
	private int fareTypeCategory;

	@XStreamAlias("rad1:FareClass")	
	private String fareClass;
	
	@XStreamAlias("rad1:FareBasisCode")	
	private String fareBasisCode;

	@XStreamAlias("rad1:Cabin")	
	private String cabin;

	@XStreamAlias("rad1:LFID")	
	private int lfID;

	@XStreamAlias("rad1:OperatingCarrierCode")	
	private String operatingCarrierCode;
	
	@XStreamAlias("rad1:MarketingCarrierCode")	
	private String marketingCarrierCode;

	@XStreamAlias("rad1:NumberOfDaysBefore")	
	private int numberOfDaysBefore;

	@XStreamAlias("rad1:NumberOfDaysAfter")	
	private int numberOfDaysAfter;

	@XStreamAlias("rad1:LanguageCode")	
	private String languageCode;

	@XStreamAlias("rad1:TicketPackageID")	
	private int ticketPackageID;

	public FareQuoteRequestInfos getFareQuoteRequestInfos() {
		return fareQuoteRequestInfos;
	}

	public void setFareQuoteRequestInfos(FareQuoteRequestInfos fareQuoteRequestInfos) {
		this.fareQuoteRequestInfos = fareQuoteRequestInfos;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public boolean isAirportsNotMetroGroups() {
		return airportsNotMetroGroups;
	}

	public void setAirportsNotMetroGroups(boolean airportsNotMetroGroups) {
		this.airportsNotMetroGroups = airportsNotMetroGroups;
	}

	public String getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(String departureDate) {
		this.departureDate = departureDate;
	}

	public int getFareTypeCategory() {
		return fareTypeCategory;
	}

	public void setFareTypeCategory(int fareTypeCategory) {
		this.fareTypeCategory = fareTypeCategory;
	}

	public String getFareClass() {
		return fareClass;
	}

	public void setFareClass(String fareClass) {
		this.fareClass = fareClass;
	}

	public String getFareBasisCode() {
		return fareBasisCode;
	}

	public void setFareBasisCode(String fareBasisCode) {
		this.fareBasisCode = fareBasisCode;
	}

	public String getCabin() {
		return cabin;
	}

	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	public int getLfID() {
		return lfID;
	}

	public void setLfID(int lfID) {
		this.lfID = lfID;
	}

	public String getOperatingCarrierCode() {
		return operatingCarrierCode;
	}

	public void setOperatingCarrierCode(String operatingCarrierCode) {
		this.operatingCarrierCode = operatingCarrierCode;
	}

	public String getMarketingCarrierCode() {
		return marketingCarrierCode;
	}

	public void setMarketingCarrierCode(String marketingCarrierCode) {
		this.marketingCarrierCode = marketingCarrierCode;
	}

	public int getNumberOfDaysBefore() {
		return numberOfDaysBefore;
	}

	public void setNumberOfDaysBefore(int numberOfDaysBefore) {
		this.numberOfDaysBefore = numberOfDaysBefore;
	}

	public int getNumberOfDaysAfter() {
		return numberOfDaysAfter;
	}

	public void setNumberOfDaysAfter(int numberOfDaysAfter) {
		this.numberOfDaysAfter = numberOfDaysAfter;
	}

	public String getLanguageCode() {
		return languageCode;
	}

	public void setLanguageCode(String languageCode) {
		this.languageCode = languageCode;
	}

	public int getTicketPackageID() {
		return ticketPackageID;
	}

	public void setTicketPackageID(int ticketPackageID) {
		this.ticketPackageID = ticketPackageID;
	}

	public Date getDepartDate() {
		return departDate;
	}

	public void setDepartDate(Date departDate) {
		this.departDate = departDate;
	}

	public Date getReturnDate() {
		return returnDate;
	}

	public void setReturnDate(Date returnDate) {
		this.returnDate = returnDate;
	}

	public int getReturnOrOneWay() {
		return returnOrOneWay;
	}

	public void setReturnOrOneWay(int returnOrOneWay) {
		this.returnOrOneWay = returnOrOneWay;
	}


}

package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("rad1:FareQuoteDetails")
public class FareQuoteDetails {
	
	@XStreamImplicit
	private List<FareQuoteDetail> faredetaillist=new ArrayList<FareQuoteDetail>();

	public List<FareQuoteDetail> getFaredetaillist() {
		return faredetaillist;
	}

	public void addFaredetaillist(FareQuoteDetail farequotedetail) {
		faredetaillist.add(farequotedetail);
	}
		
}

package org.perfomics.flights.business.flydubai.xo.farequote;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("rad1:FareQuoteRequestInfo")
public class FareQuoteRequestInfo {

	@XStreamOmitField
	private int numberOfAdult;
	
	@XStreamOmitField
	private int numberOfChild;
	
	@XStreamOmitField
	private int numberOfInfant;

	@XStreamAlias("rad1:PassengerTypeID")	
	private int passengerTypeID;

	@XStreamAlias("rad1:TotalSeatsRequired")	
	private int totalSeatsRequired;

	public int getPassengerTypeID() {
		return passengerTypeID;
	}

	public void setPassengerTypeID(int passengerTypeID) {
		this.passengerTypeID = passengerTypeID;
	}

	public int getTotalSeatsRequired() {
		return totalSeatsRequired;
	}

	public void setTotalSeatsRequired(int totalSeatsRequired) {
		this.totalSeatsRequired = totalSeatsRequired;
	}

	public int getNumberOfAdult() {
		return numberOfAdult;
	}

	public void setNumberOfAdult(int numberOfAdult) {
		this.numberOfAdult = numberOfAdult;
	}

	public int getNumberOfChild() {
		return numberOfChild;
	}

	public void setNumberOfChild(int numberOfChild) {
		this.numberOfChild = numberOfChild;
	}

	public int getNumberOfInfant() {
		return numberOfInfant;
	}

	public void setNumberOfInfant(int numberOfInfant) {
		this.numberOfInfant = numberOfInfant;
	}
	
	    
}

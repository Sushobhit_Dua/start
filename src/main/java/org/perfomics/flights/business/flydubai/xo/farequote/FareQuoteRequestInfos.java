package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("rad1:FareQuoteRequestInfos")	
public class FareQuoteRequestInfos {
	
	@XStreamImplicit
	private List<FareQuoteRequestInfo> fareinfolist=new ArrayList<FareQuoteRequestInfo>();

	public List<FareQuoteRequestInfo> getFareinfolist() {
		return fareinfolist;
	}

	public void addFareinfolist(FareQuoteRequestInfo farerequestinfo) {
		fareinfolist.add(farerequestinfo);
	}

}

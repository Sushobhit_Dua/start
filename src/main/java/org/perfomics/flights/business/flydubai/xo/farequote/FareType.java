package org.perfomics.flights.business.flydubai.xo.farequote;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:FareType")
public class FareType {
	
	@XStreamAlias("a:FareTypeID")	
	private int fareTypeID;
	
	@XStreamAlias("a:FareTypeName")	
	private String fareTypeName;

	@XStreamAlias("a:FilterRemove")	
	private boolean filterRemove;

	@XStreamAlias("a:FareInfos")	
	private FareInfos fareInfos;

	public int getFareTypeID() {
		return fareTypeID;
	}

	public void setFareTypeID(int fareTypeID) {
		this.fareTypeID = fareTypeID;
	}

	public String getFareTypeName() {
		return fareTypeName;
	}

	public void setFareTypeName(String fareTypeName) {
		this.fareTypeName = fareTypeName;
	}

	public boolean isFilterRemove() {
		return filterRemove;
	}

	public void setFilterRemove(boolean filterRemove) {
		this.filterRemove = filterRemove;
	}

	public FareInfos getFareInfos() {
		return fareInfos;
	}

	public void setFareInfos(FareInfos fareInfos) {
		this.fareInfos = fareInfos;
	}


}

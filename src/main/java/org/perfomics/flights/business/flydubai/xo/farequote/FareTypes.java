package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:FareTypes")
public class FareTypes {
	
	@XStreamImplicit
	private List<FareType> faretypelist=new ArrayList<FareType>();

	public List<FareType> getFaretypelist() {
		return faretypelist;
	}

	public void addFaretypelist(FareType faretype) {
		faretypelist.add(faretype);
	}


}

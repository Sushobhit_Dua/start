package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:FlightLegDetail")
public class FlightLegDetail {
	
	@XStreamAlias("a:PFID")	
	private int pfID;
	
	@XStreamAlias("a:DepartureDate")	
	private Date departureDate;

	public int getPfID() {
		return pfID;
	}

	public void setPfID(int pfID) {
		this.pfID = pfID;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

}

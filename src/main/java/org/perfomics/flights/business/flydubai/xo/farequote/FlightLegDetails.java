package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:FlightLegDetails")
public class FlightLegDetails {
	
	@XStreamImplicit
	private List<FlightLegDetail> flightleglist=new ArrayList<FlightLegDetail>();

	public List<FlightLegDetail> getFlightleglist() {
		return flightleglist;
	}

	public void addFlightleglist(FlightLegDetail flightlegdetail) {
		flightleglist.add(flightlegdetail);
	}


}

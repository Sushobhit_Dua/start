package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:FlightSegment")
public class FlightSegment {

	@XStreamAlias("a:DepartureDate")	
	private Date departDate;

	@XStreamAlias("a:LFID")	
	private int lfID;
	
	@XStreamAlias("a:ArrivalDate")	
	private Date arrivalDate;

	@XStreamAlias("a:LegCount")	
	private int legCount;

	@XStreamAlias("a:International")	
	private boolean international;	
	
	@XStreamAlias("a:FareTypes")
	private FareTypes fareTypes;

	@XStreamAlias("a:FlightLegDetails")
	private FlightLegDetails flightLegDetails;

	public int getLfID() {
		return lfID;
	}

	public void setLfID(int lfID) {
		this.lfID = lfID;
	}


	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public int getLegCount() {
		return legCount;
	}

	public void setLegCount(int legCount) {
		this.legCount = legCount;
	}

	public boolean isInternational() {
		return international;
	}

	public void setInternational(boolean international) {
		this.international = international;
	}

	public FareTypes getFareTypes() {
		return fareTypes;
	}

	public void setFareTypes(FareTypes fareTypes) {
		this.fareTypes = fareTypes;
	}

	public FlightLegDetails getFlightLegDetails() {
		return flightLegDetails;
	}

	public void setFlightLegDetails(FlightLegDetails flightLegDetails) {
		this.flightLegDetails = flightLegDetails;
	}

	public Date getDepartDate() {
		return departDate;
	}

	public void setDepartDate(Date departDate) {
		this.departDate = departDate;
	}
	

}

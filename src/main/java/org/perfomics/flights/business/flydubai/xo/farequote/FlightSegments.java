package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:FlightSegments")
public class FlightSegments {
	
	@XStreamImplicit
	private List<FlightSegment> flightlist=new ArrayList<FlightSegment>();

	public List<FlightSegment> getFlightlist() {
		return flightlist;
	}

	public void addFlightlist(FlightSegment flightsegment) {
		flightlist.add(flightsegment);
	}


}

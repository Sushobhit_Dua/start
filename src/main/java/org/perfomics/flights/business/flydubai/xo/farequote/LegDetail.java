package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:LegDetail")
public class LegDetail {
	
	@XStreamAlias("a:PFID")	
	private int pfID;
	
	@XStreamAlias("a:DepartureDate")	
	private Date departureDate;

	@XStreamAlias("a:Origin")	
	private String origin;
	
	@XStreamAlias("a:Destination")	
	private String destination;

	@XStreamAlias("a:FlightNum")	
	private String flightNum;
	
	@XStreamAlias("a:International")	
	private boolean international;

	@XStreamAlias("a:ArrivalDate")	
	private Date arrivalDate;

	@XStreamAlias("a:FlightTime")	
	private int flightTime;

	@XStreamAlias("a:OperatingCarrier")	
	private String operatingCarrier;

	@XStreamAlias("a:FromTerminal")	
	private String fromTerminal;

	@XStreamAlias("a:ToTerminal")	
	private String toTerminal;

	public int getPfID() {
		return pfID;
	}

	public void setPfID(int pfID) {
		this.pfID = pfID;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public boolean isInternational() {
		return international;
	}

	public void setInternational(boolean international) {
		this.international = international;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public int getFlightTime() {
		return flightTime;
	}

	public void setFlightTime(int flightTime) {
		this.flightTime = flightTime;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getFromTerminal() {
		return fromTerminal;
	}

	public void setFromTerminal(String fromTerminal) {
		this.fromTerminal = fromTerminal;
	}

	public String getToTerminal() {
		return toTerminal;
	}

	public void setToTerminal(String toTerminal) {
		this.toTerminal = toTerminal;
	}

}

package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:LegDetails")
public class LegDetails {
	
	@XStreamImplicit
	private List<LegDetail> legdetaillist=new ArrayList<LegDetail>();

	public List<LegDetail> getLegdetaillist() {
		return legdetaillist;
	}

	public void addLegdetaillist(LegDetail legdetail) {
		legdetaillist.add(legdetail);
	}
	

}

package org.perfomics.flights.business.flydubai.xo.farequote;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrieveFareQuote")
public class RetrieveFareQuote {
	
	@XStreamAlias("tem:RetrieveFareQuoteRequest")
	private RetrieveFareQuoteRequest retrieveFareQuoteRequest;

	public RetrieveFareQuoteRequest getRetrieveFareQuoteRequest() {
		return retrieveFareQuoteRequest;
	}

	public void setRetrieveFareQuoteRequest(RetrieveFareQuoteRequest retrieveFareQuoteRequest) {
		this.retrieveFareQuoteRequest = retrieveFareQuoteRequest;
	}
	
}

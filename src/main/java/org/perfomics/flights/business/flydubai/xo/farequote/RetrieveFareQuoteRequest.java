package org.perfomics.flights.business.flydubai.xo.farequote;

import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrieveFareQuoteRequest")
public class RetrieveFareQuoteRequest {	  
    
    @XStreamAlias("rad:CarrierCodes")	
	private CarrierCodes carrierCodes;

	@XStreamAlias("rad:SecurityGUID")	
	private String securityGUID;

	@XStreamAlias("rad:HistoricUserName")	
	private String historicUserName;
	
	@XStreamAlias("rad1:CurrencyOfFareQuote")	
	private String currencyOfFareQuote;

	@XStreamAlias("rad1:PromotionalCode")	
	private String promotionalCode;

	@XStreamAlias("rad1:IataNumberOfRequestor")	
	private String iataNumberRequestor;

	@XStreamAlias("rad1:CorporationID")	
	private int corporationID;

	@XStreamAlias("rad1:FareFilterMethod")	
	private String fareFilterMethod;

	@XStreamAlias("rad1:FareGroupMethod")	
	private String fareGroupMethod;

	@XStreamAlias("rad1:InventoryFilterMethod")	
	private String inventoryFilterMethod;

	@XStreamAlias("rad1:FareQuoteDetails")	
	private FareQuoteDetails fareQuoteDetails;

	public CarrierCodes getCarrierCodes() {
		return carrierCodes;
	}

	public void setCarrierCodes(CarrierCodes carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	public String getSecurityGUID() {
		return securityGUID;
	}

	public void setSecurityGUID(String securityGUID) {
		this.securityGUID = securityGUID;
	}

	public String getHistoricUserName() {
		return historicUserName;
	}

	public void setHistoricUserName(String historicUserName) {
		this.historicUserName = historicUserName;
	}

	public String getCurrencyOfFareQuote() {
		return currencyOfFareQuote;
	}

	public void setCurrencyOfFareQuote(String currencyOfFareQuote) {
		this.currencyOfFareQuote = currencyOfFareQuote;
	}

	public String getPromotionalCode() {
		return promotionalCode;
	}

	public void setPromotionalCode(String promotionalCode) {
		this.promotionalCode = promotionalCode;
	}

	public String getIataNumberRequestor() {
		return iataNumberRequestor;
	}

	public void setIataNumberRequestor(String iataNumberRequestor) {
		this.iataNumberRequestor = iataNumberRequestor;
	}

	public int getCorporationID() {
		return corporationID;
	}

	public void setCorporationID(int corporationID) {
		this.corporationID = corporationID;
	}

	public String getFareFilterMethod() {
		return fareFilterMethod;
	}

	public void setFareFilterMethod(String fareFilterMethod) {
		this.fareFilterMethod = fareFilterMethod;
	}

	public String getFareGroupMethod() {
		return fareGroupMethod;
	}

	public void setFareGroupMethod(String fareGroupMethod) {
		this.fareGroupMethod = fareGroupMethod;
	}

	public String getInventoryFilterMethod() {
		return inventoryFilterMethod;
	}

	public void setInventoryFilterMethod(String inventoryFilterMethod) {
		this.inventoryFilterMethod = inventoryFilterMethod;
	}

	public FareQuoteDetails getFareQuoteDetails() {
		return fareQuoteDetails;
	}

	public void setFareQuoteDetails(FareQuoteDetails fareQuoteDetails) {
		this.fareQuoteDetails = fareQuoteDetails;
	}
	
	
}

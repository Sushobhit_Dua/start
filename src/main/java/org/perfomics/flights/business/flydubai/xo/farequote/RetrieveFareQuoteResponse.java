package org.perfomics.flights.business.flydubai.xo.farequote;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("RetrieveFareQuoteResponse")
public class RetrieveFareQuoteResponse {
	
	@XStreamAlias("RetrieveFareQuoteResult")	
	private RetrieveFareQuoteResult retrieveFareQuoteResult;

	@XStreamAlias("xmlns")	
	@XStreamAsAttribute
	private String xmlns;

	public RetrieveFareQuoteResult getRetrieveFareQuoteResult() {
		return retrieveFareQuoteResult;
	}

	public void setRetrieveFareQuoteResult(RetrieveFareQuoteResult retrieveFareQuoteResult) {
		this.retrieveFareQuoteResult = retrieveFareQuoteResult;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}


}

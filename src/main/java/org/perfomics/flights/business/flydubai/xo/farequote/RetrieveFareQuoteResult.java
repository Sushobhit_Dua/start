package org.perfomics.flights.business.flydubai.xo.farequote;

import org.perfomics.flights.business.flydubai.xo.exceptions.Exceptions;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("RetrieveFareQuoteResult")
public class RetrieveFareQuoteResult {
	
	@XStreamAlias("xmlns:a")
	@XStreamAsAttribute
	private String xmlnsA;
	
	@XStreamAlias("xmlns:i")
	@XStreamAsAttribute
	private String xmlnsI;
	
	@XStreamAlias("a:CommissionIncluded")
	private boolean commissionIncluded;
	
	@XStreamAlias("a:Exceptions")
	private Exceptions exceptions;

	@XStreamAlias("a:FlightSegments")
	private FlightSegments flightSegments;

	@XStreamAlias("a:LegDetails")
	private LegDetails legDetails;
	
	@XStreamAlias("a:SegmentDetails")
	private SegmentDetails segmentDetails;

	@XStreamAlias("a:TaxDetails")
	private TaxDetails taxDetails;
	
	@XStreamAlias("a:RequestedCorporationID")
	private int requestedCorporationID;

	@XStreamAlias("a:RequestedCurrencyOfFareQuote")
	private String requestedCurrencyOfFareQuote;
	
	@XStreamAlias("a:RequestedFareFilterMethod")
	private int requestedFareFilterMethod;
	
	@XStreamAlias("a:RequestedGroupMethod")
	private int requestedGroupMethod;

	@XStreamAlias("a:RequestedInventoryFilterMethod")
	private int requestedInventoryFilterMethod;
	
	@XStreamAlias("a:RequestedReservationChannel")
	private int requestedReservationChannel;
	
	@XStreamAlias("a:RequestedIataNumber")
	private String requestedIataNumber;
	
	@XStreamAlias("a:RequestedPromotionalCode")
	private String requestedPromotionalCode;

	public String getXmlnsA() {
		return xmlnsA;
	}

	public void setXmlnsA(String xmlnsA) {
		this.xmlnsA = xmlnsA;
	}

	public String getXmlnsI() {
		return xmlnsI;
	}

	public void setXmlnsI(String xmlnsI) {
		this.xmlnsI = xmlnsI;
	}

	public boolean isCommissionIncluded() {
		return commissionIncluded;
	}

	public void setCommissionIncluded(boolean commissionIncluded) {
		this.commissionIncluded = commissionIncluded;
	}

	public Exceptions getExceptions() {
		return exceptions;
	}

	public void setExceptions(Exceptions exceptions) {
		this.exceptions = exceptions;
	}

	public FlightSegments getFlightSegments() {
		return flightSegments;
	}

	public void setFlightSegments(FlightSegments flightSegments) {
		this.flightSegments = flightSegments;
	}

	public LegDetails getLegDetails() {
		return legDetails;
	}

	public void setLegDetails(LegDetails legDetails) {
		this.legDetails = legDetails;
	}

	public SegmentDetails getSegmentDetails() {
		return segmentDetails;
	}

	public void setSegmentDetails(SegmentDetails segmentDetails) {
		this.segmentDetails = segmentDetails;
	}

	public TaxDetails getTaxDetails() {
		return taxDetails;
	}

	public void setTaxDetails(TaxDetails taxDetails) {
		this.taxDetails = taxDetails;
	}

	public int getRequestedCorporationID() {
		return requestedCorporationID;
	}

	public void setRequestedCorporationID(int requestedCorporationID) {
		this.requestedCorporationID = requestedCorporationID;
	}

	public String getRequestedCurrencyOfFareQuote() {
		return requestedCurrencyOfFareQuote;
	}

	public void setRequestedCurrencyOfFareQuote(String requestedCurrencyOfFareQuote) {
		this.requestedCurrencyOfFareQuote = requestedCurrencyOfFareQuote;
	}

	public int getRequestedFareFilterMethod() {
		return requestedFareFilterMethod;
	}

	public void setRequestedFareFilterMethod(int requestedFareFilterMethod) {
		this.requestedFareFilterMethod = requestedFareFilterMethod;
	}

	public int getRequestedGroupMethod() {
		return requestedGroupMethod;
	}

	public void setRequestedGroupMethod(int requestedGroupMethod) {
		this.requestedGroupMethod = requestedGroupMethod;
	}

	public int getRequestedInventoryFilterMethod() {
		return requestedInventoryFilterMethod;
	}

	public void setRequestedInventoryFilterMethod(int requestedInventoryFilterMethod) {
		this.requestedInventoryFilterMethod = requestedInventoryFilterMethod;
	}

	public int getRequestedReservationChannel() {
		return requestedReservationChannel;
	}

	public void setRequestedReservationChannel(int requestedReservationChannel) {
		this.requestedReservationChannel = requestedReservationChannel;
	}

	public String getRequestedIataNumber() {
		return requestedIataNumber;
	}

	public void setRequestedIataNumber(String requestedIataNumber) {
		this.requestedIataNumber = requestedIataNumber;
	}

	public String getRequestedPromotionalCode() {
		return requestedPromotionalCode;
	}

	public void setRequestedPromotionalCode(String requestedPromotionalCode) {
		this.requestedPromotionalCode = requestedPromotionalCode;
	}
	
	
}

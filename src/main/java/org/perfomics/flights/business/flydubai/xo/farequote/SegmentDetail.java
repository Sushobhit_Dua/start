package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:SegmentDetail")
public class SegmentDetail {
		
	@XStreamAlias("a:LFID")	
	private int lfID;
	
	@XStreamAlias("a:Origin")	
	private String origin;
	
	@XStreamAlias("a:Destination")	
	private String destination;

	@XStreamAlias("a:DepartureDate")	
	private Date departureDate;

	@XStreamAlias("a:CarrierCode")	
	private String carrierCode;

	@XStreamAlias("a:ArrivalDate")	
	private Date arrivalDate;

	@XStreamAlias("a:Stops")	
	private int stops;
	
	@XStreamAlias("a:FlightTime")	
	private int flightTime;
	
	@XStreamAlias("a:AircraftType")	
	private String aircraftType;

	@XStreamAlias("a:SellingCarrier")	
	private String sellingCarrier;

	@XStreamAlias("a:FlightNum")	
	private String flightNum;
	
	@XStreamAlias("a:OperatingCarrier")	
	private String operatingCarrier;

	@XStreamAlias("a:OperatingFlightNum")	
	private String operatingFlightNum;

	@XStreamAlias("a:FlyMonday")	
	private boolean flyMonday;		
	
	@XStreamAlias("a:FlyTuesday")	
	private boolean flyTuesday;		
	
	@XStreamAlias("a:FlyWednesday")	
	private boolean flyWednesday;		
	
	@XStreamAlias("a:FlyThursday")	
	private boolean flyThursday;		
	
	@XStreamAlias("a:FlyFriday")	
	private boolean flyFriday;		
	
	@XStreamAlias("a:FlySaturday")	
	private boolean flySaturday;		
	
	@XStreamAlias("a:FlySunday")	
	private boolean flySunday;

	public int getLfID() {
		return lfID;
	}

	public void setLfID(int lfID) {
		this.lfID = lfID;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public Date getDepartureDate() {
		return departureDate;
	}

	public void setDepartureDate(Date departureDate) {
		this.departureDate = departureDate;
	}

	public String getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(String carrierCode) {
		this.carrierCode = carrierCode;
	}

	public Date getArrivalDate() {
		return arrivalDate;
	}

	public void setArrivalDate(Date arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	public int getStops() {
		return stops;
	}

	public void setStops(int stops) {
		this.stops = stops;
	}

	public int getFlightTime() {
		return flightTime;
	}

	public void setFlightTime(int flightTime) {
		this.flightTime = flightTime;
	}

	public String getAircraftType() {
		return aircraftType;
	}

	public void setAircraftType(String aircraftType) {
		this.aircraftType = aircraftType;
	}

	public String getSellingCarrier() {
		return sellingCarrier;
	}

	public void setSellingCarrier(String sellingCarrier) {
		this.sellingCarrier = sellingCarrier;
	}

	public String getFlightNum() {
		return flightNum;
	}

	public void setFlightNum(String flightNum) {
		this.flightNum = flightNum;
	}

	public String getOperatingCarrier() {
		return operatingCarrier;
	}

	public void setOperatingCarrier(String operatingCarrier) {
		this.operatingCarrier = operatingCarrier;
	}

	public String getOperatingFlightNum() {
		return operatingFlightNum;
	}

	public void setOperatingFlightNum(String operatingFlightNum) {
		this.operatingFlightNum = operatingFlightNum;
	}

	public boolean isFlyMonday() {
		return flyMonday;
	}

	public void setFlyMonday(boolean flyMonday) {
		this.flyMonday = flyMonday;
	}

	public boolean isFlyTuesday() {
		return flyTuesday;
	}

	public void setFlyTuesday(boolean flyTuesday) {
		this.flyTuesday = flyTuesday;
	}

	public boolean isFlyWednesday() {
		return flyWednesday;
	}

	public void setFlyWednesday(boolean flyWednesday) {
		this.flyWednesday = flyWednesday;
	}

	public boolean isFlyThursday() {
		return flyThursday;
	}

	public void setFlyThursday(boolean flyThursday) {
		this.flyThursday = flyThursday;
	}

	public boolean isFlyFriday() {
		return flyFriday;
	}

	public void setFlyFriday(boolean flyFriday) {
		this.flyFriday = flyFriday;
	}

	public boolean isFlySaturday() {
		return flySaturday;
	}

	public void setFlySaturday(boolean flySaturday) {
		this.flySaturday = flySaturday;
	}

	public boolean isFlySunday() {
		return flySunday;
	}

	public void setFlySunday(boolean flySunday) {
		this.flySunday = flySunday;
	}		

	
}

package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:SegmentDetails")
public class SegmentDetails {
	
	@XStreamImplicit
	private List<SegmentDetail> segmentdetaillist=new ArrayList<SegmentDetail>();

	public List<SegmentDetail> getSegmentdetaillist() {
		return segmentdetaillist;
	}

	public void addSegmentdetaillist(SegmentDetail segmentdetail) {
		segmentdetaillist.add(segmentdetail);
	}


}

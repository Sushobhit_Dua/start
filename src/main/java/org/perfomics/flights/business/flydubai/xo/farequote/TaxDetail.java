package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("a:TaxDetail")
public class TaxDetail {
	
	@XStreamAlias("a:TaxID")	
	private int taxID;
	
	@XStreamAlias("a:TaxCode")	
	private String taxCode;

	@XStreamAlias("a:CodeType")	
	private String codeType;

	@XStreamAlias("a:TaxCurr")	
	private String taxCurr;

	@XStreamAlias("a:TaxDesc")	
	private String taxDesc;

	@XStreamAlias("a:TaxType")	
	private int taxType;
	
	@XStreamAlias("a:IsVat")	
	private boolean vat;

	@XStreamAlias("a:IncludedInFare")	
	private boolean includedInFare;
	
	@XStreamAlias("a:OriginalCurrency")	
	private String originalCurrency;
	
	@XStreamAlias("a:ExchangeRate")	
	private double exchangeRate;
	
	@XStreamAlias("a:ExchangeDate")	
	private Date exchangeDate;
	
	@XStreamAlias("a:Commissionable")	
	private boolean commissionable;

	public int getTaxID() {
		return taxID;
	}

	public void setTaxID(int taxID) {
		this.taxID = taxID;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getCodeType() {
		return codeType;
	}

	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}

	public String getTaxCurr() {
		return taxCurr;
	}

	public void setTaxCurr(String taxCurr) {
		this.taxCurr = taxCurr;
	}

	public String getTaxDesc() {
		return taxDesc;
	}

	public void setTaxDesc(String taxDesc) {
		this.taxDesc = taxDesc;
	}

	public int getTaxType() {
		return taxType;
	}

	public void setTaxType(int taxType) {
		this.taxType = taxType;
	}

	public boolean isVat() {
		return vat;
	}

	public void setVat(boolean vat) {
		this.vat = vat;
	}

	public boolean isIncludedInFare() {
		return includedInFare;
	}

	public void setIncludedInFare(boolean includedInFare) {
		this.includedInFare = includedInFare;
	}

	public String getOriginalCurrency() {
		return originalCurrency;
	}

	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public double getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Date getExchangeDate() {
		return exchangeDate;
	}

	public void setExchangeDate(Date exchangeDate) {
		this.exchangeDate = exchangeDate;
	}

	public boolean isCommissionable() {
		return commissionable;
	}

	public void setCommissionable(boolean commissionable) {
		this.commissionable = commissionable;
	}
	
	
}

package org.perfomics.flights.business.flydubai.xo.farequote;

import java.util.ArrayList;
import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("a:TaxDetails")
public class TaxDetails {
	
	@XStreamImplicit
	private List<TaxDetail> taxdetaillist=new ArrayList<TaxDetail>();

	public List<TaxDetail> getTaxdetaillist() {
		return taxdetaillist;
	}

	public void addTaxdetaillist(TaxDetail taxdetail) {
		taxdetaillist.add(taxdetail);
	}

}

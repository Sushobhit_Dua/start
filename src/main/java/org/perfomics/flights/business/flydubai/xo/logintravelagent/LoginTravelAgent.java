package org.perfomics.flights.business.flydubai.xo.logintravelagent;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:LoginTravelAgent")
public class LoginTravelAgent {
	
	@XStreamAlias("tem:LoginTravelAgentRequest")
	private LoginTravelAgentRequest loginTravelAgentRequest;

	public LoginTravelAgentRequest getLoginTravelAgentRequest() {
		return loginTravelAgentRequest;
	}

	public void setLoginTravelAgentRequest(LoginTravelAgentRequest loginTravelAgentRequest) {
		this.loginTravelAgentRequest = loginTravelAgentRequest;
	}


}

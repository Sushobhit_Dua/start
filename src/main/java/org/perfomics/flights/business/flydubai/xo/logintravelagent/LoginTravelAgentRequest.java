package org.perfomics.flights.business.flydubai.xo.logintravelagent;

import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:LoginTravelAgentRequest")
public class LoginTravelAgentRequest {
	
	@XStreamAlias("rad:CarrierCodes")	
	private CarrierCodes carrierCodes;

	@XStreamAlias("rad:SecurityGUID")	
	private String securityGUID;

	@XStreamAlias("rad:ClientIPAddress")	
	private String clientIP;

	@XStreamAlias("rad:HistoricUserName")	
	private String historicUserName;

	@XStreamAlias("rad1:IATANumber")	
	private String iataNumber;

	@XStreamAlias("rad1:UserName")	
	private String userName;

	@XStreamAlias("rad1:Password")	
	private String password;

	public CarrierCodes getCarrierCodes() {
		return carrierCodes;
	}

	public void setCarrierCodes(CarrierCodes carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	public String getSecurityGUID() {
		return securityGUID;
	}

	public void setSecurityGUID(String securityGUID) {
		this.securityGUID = securityGUID;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getHistoricUserName() {
		return historicUserName;
	}

	public void setHistoricUserName(String historicUserName) {
		this.historicUserName = historicUserName;
	}

	public String getIataNumber() {
		return iataNumber;
	}

	public void setIataNumber(String iataNumber) {
		this.iataNumber = iataNumber;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
}

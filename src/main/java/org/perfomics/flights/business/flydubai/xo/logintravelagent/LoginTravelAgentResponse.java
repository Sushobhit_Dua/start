package org.perfomics.flights.business.flydubai.xo.logintravelagent;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("LoginTravelAgentResponse")
public class LoginTravelAgentResponse {
	
		@XStreamAlias("LoginTravelAgentResult")	
		private LoginTravelAgentResult loginTravelAgentResult;

		@XStreamAlias("xmlns")	
		@XStreamAsAttribute
		private String xmlns;

		public LoginTravelAgentResult getLoginTravelAgentResult() {
			return loginTravelAgentResult;
		}

		public void setLoginTravelAgentResult(LoginTravelAgentResult loginTravelAgentResult) {
			this.loginTravelAgentResult = loginTravelAgentResult;
		}

		public String getXmlns() {
			return xmlns;
		}

		public void setXmlns(String xmlns) {
			this.xmlns = xmlns;
		}
		

}

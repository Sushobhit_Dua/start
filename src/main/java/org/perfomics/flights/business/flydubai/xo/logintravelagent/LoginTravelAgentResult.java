package org.perfomics.flights.business.flydubai.xo.logintravelagent;

import org.perfomics.flights.business.flydubai.xo.exceptions.Exceptions;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("LoginTravelAgentResult")	
public class LoginTravelAgentResult {
	
	@XStreamAlias("xmlns:a")
	@XStreamAsAttribute
	private String xmlnsA;
	
	@XStreamAlias("xmlns:i")
	@XStreamAsAttribute
	private String xmlnsI;
	
	@XStreamAlias("a:LoggedIn")
	private boolean loggedIn;
	
	@XStreamAlias("a:Exceptions")
	private Exceptions exceptions;

	public String getXmlnsA() {
		return xmlnsA;
	}

	public void setXmlnsA(String xmlnsA) {
		this.xmlnsA = xmlnsA;
	}

	public String getXmlnsI() {
		return xmlnsI;
	}

	public void setXmlnsI(String xmlnsI) {
		this.xmlnsI = xmlnsI;
	}

	public boolean isLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(boolean loggedIn) {
		this.loggedIn = loggedIn;
	}

	public Exceptions getExceptions() {
		return exceptions;
	}

	public void setExceptions(Exceptions exceptions) {
		this.exceptions = exceptions;
	}
	

}

package org.perfomics.flights.business.flydubai.xo.processpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:PNRPaymentRequest")
public class PNRPaymentRequest {
	
	@XStreamAlias("rad:TransactionInfo")	
	private TransactionInfo transactionInfo;

	@XStreamAlias("rad:ReservationInfo")	
	private ReservationInfo reservationInfo;

	@XStreamAlias("rad:PNRPayments")	
	private PNRPayments pnrPayments;

	public TransactionInfo getTransactionInfo() {
		return transactionInfo;
	}

	public void setTransactionInfo(TransactionInfo transactionInfo) {
		this.transactionInfo = transactionInfo;
	}

	public ReservationInfo getReservationInfo() {
		return reservationInfo;
	}

	public void setReservationInfo(ReservationInfo reservationInfo) {
		this.reservationInfo = reservationInfo;
	}

	public PNRPayments getPnrPayments() {
		return pnrPayments;
	}

	public void setPnrPayments(PNRPayments pnrPayments) {
		this.pnrPayments = pnrPayments;
	}

	
}

package org.perfomics.flights.business.flydubai.xo.processpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad:PNRPayments")
public class PNRPayments {

	@XStreamAlias("rad:ProcessPNRPayment")	
	private ProcessPNRPayment1 processPNRPayment1;

	public ProcessPNRPayment1 getProcessPNRPayment1() {
		return processPNRPayment1;
	}

	public void setProcessPNRPayment1(ProcessPNRPayment1 processPNRPayment1) {
		this.processPNRPayment1 = processPNRPayment1;
	}


	
}

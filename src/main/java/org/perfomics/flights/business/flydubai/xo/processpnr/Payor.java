package org.perfomics.flights.business.flydubai.xo.processpnr;

import java.util.Date;

import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfos;
import org.perfomics.flights.business.flydubai.xo.summarypnr.Address;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;

@XStreamAlias("rad:Payor")	
public class Payor {
	
	 @XStreamAlias("rad2:Address")
	 private Address address;

	 @XStreamAlias("rad2:ContactInfos")
	 private ContactInfos contactInfos;
	 
	 @XStreamAlias("rad2:PersonOrgID")
	 private String personOrgID;
	 
	 @XStreamOmitField
	 private String contactID;

	 @XStreamAlias("rad2:FirstName")
	 private String firstName;

	 @XStreamAlias("rad2:LastName")
	 private String lastName;

	 @XStreamAlias("rad2:MiddleName")
	 private String middleName;

	 @XStreamAlias("rad2:Age")
	 private String age;

	 @XStreamAlias("rad2:DOB")
	 private Date dob;

	 @XStreamAlias("rad2:Gender")
	 private String gender;
	 
	 @XStreamAlias("rad2:Title")
	 private String title;
	
	 @XStreamAlias("rad2:NationalityLaguageID")
	 private String nationalityLaguageID;
	
	 @XStreamAlias("rad2:RelationType")
	 private String relationType;
	
	 @XStreamAlias("rad2:WBCID")
	 private int wbCID;
	
	 @XStreamAlias("rad2:PTCID")
	 private int ptcID;

	 @XStreamAlias("rad2:UseInventory")
	 private boolean useInventory;

	 @XStreamAlias("rad2:PTC")
	 private int ptc;

	 @XStreamAlias("rad2:TravelsWithPersonOrgID")
	 private String travelsWithPersonOrgID;
	
	 @XStreamAlias("rad2:RedressNumber")
	 private String redressNumber;
	
	 @XStreamAlias("rad2:KnownTravelerNumber")
	 private String knownTravelerNumber;

	 @XStreamAlias("rad2:MarketingOptIn")
	 private boolean marketingOptIn;

	 @XStreamAlias("rad2:Company")
	 private String company;
	
	 @XStreamAlias("rad2:Comments")
	 private String comments;

	 @XStreamAlias("rad2:Passport")
	 private String passport;
	
	 @XStreamAlias("rad2:Nationality")
	 private String nationality;

	 @XStreamAlias("rad2:ProfileId")
	 private String profileId;

	 @XStreamAlias("rad2:IsPrimaryPassenger")
	 private boolean isPrimaryPassenger;

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ContactInfos getContactInfos() {
		return contactInfos;
	}

	public void setContactInfos(ContactInfos contactInfos) {
		this.contactInfos = contactInfos;
	}

	public String getPersonOrgID() {
		return personOrgID;
	}

	public void setPersonOrgID(String personOrgID) {
		this.personOrgID = personOrgID;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNationalityLaguageID() {
		return nationalityLaguageID;
	}

	public void setNationalityLaguageID(String nationalityLaguageID) {
		this.nationalityLaguageID = nationalityLaguageID;
	}

	public String getRelationType() {
		return relationType;
	}

	public void setRelationType(String relationType) {
		this.relationType = relationType;
	}

	public int getWbCID() {
		return wbCID;
	}

	public void setWbCID(int wbCID) {
		this.wbCID = wbCID;
	}

	public int getPtcID() {
		return ptcID;
	}

	public void setPtcID(int ptcID) {
		this.ptcID = ptcID;
	}

	public boolean isUseInventory() {
		return useInventory;
	}

	public void setUseInventory(boolean useInventory) {
		this.useInventory = useInventory;
	}

	public int getPtc() {
		return ptc;
	}

	public void setPtc(int ptc) {
		this.ptc = ptc;
	}

	public String getTravelsWithPersonOrgID() {
		return travelsWithPersonOrgID;
	}

	public void setTravelsWithPersonOrgID(String travelsWithPersonOrgID) {
		this.travelsWithPersonOrgID = travelsWithPersonOrgID;
	}

	public String getRedressNumber() {
		return redressNumber;
	}

	public void setRedressNumber(String redressNumber) {
		this.redressNumber = redressNumber;
	}

	public String getKnownTravelerNumber() {
		return knownTravelerNumber;
	}

	public void setKnownTravelerNumber(String knownTravelerNumber) {
		this.knownTravelerNumber = knownTravelerNumber;
	}

	public boolean isMarketingOptIn() {
		return marketingOptIn;
	}

	public void setMarketingOptIn(boolean marketingOptIn) {
		this.marketingOptIn = marketingOptIn;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getPassport() {
		return passport;
	}

	public void setPassport(String passport) {
		this.passport = passport;
	}

	public String getNationality() {
		return nationality;
	}

	public void setNationality(String nationality) {
		this.nationality = nationality;
	}

	public String getProfileId() {
		return profileId;
	}

	public void setProfileId(String profileId) {
		this.profileId = profileId;
	}

	public boolean isPrimaryPassenger() {
		return isPrimaryPassenger;
	}

	public void setPrimaryPassenger(boolean isPrimaryPassenger) {
		this.isPrimaryPassenger = isPrimaryPassenger;
	}

	public String getContactID() {
		return contactID;
	}

	public void setContactID(String contactID) {
		this.contactID = contactID;
	}


}

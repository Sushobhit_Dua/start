package org.perfomics.flights.business.flydubai.xo.processpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:ProcessPNRPayment")
public class ProcessPNRPayment {
	
	@XStreamAlias("tem:PNRPaymentRequest")
	private PNRPaymentRequest pnrPaymentRequest;

	public PNRPaymentRequest getPnrPaymentRequest() {
		return pnrPaymentRequest;
	}

	public void setPnrPaymentRequest(PNRPaymentRequest pnrPaymentRequest) {
		this.pnrPaymentRequest = pnrPaymentRequest;
	}


}

package org.perfomics.flights.business.flydubai.xo.processpnr;

import java.util.Date;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad:ProcessPNRPayment")
public class ProcessPNRPayment1 {
	
	@XStreamAlias("rad:Payor")	
	private Payor payor;
	
	 @XStreamAlias("rad:BaseAmount")
	 private double baseAmount;
	 
	 @XStreamAlias("rad:BaseCurrency")
	 private String baseCurrency;

	 @XStreamAlias("rad:CardHolder")
	 private String cardHolder;
	
	 @XStreamAlias("rad:CardNumber")
	 private String cardNumber;

	 @XStreamAlias("rad:CheckNumber")
	 private String checkNumber;
	
	 @XStreamAlias("rad:CurrencyPaid")
	 private String currencyPaid;

	 @XStreamAlias("rad:CVCode")
	 private String cvCode;
	
	 @XStreamAlias("rad:DatePaid")
	 private Date datePaid;

	 @XStreamAlias("rad:DocumentReceivedBy")
	 private String documentReceivedBy;

	 @XStreamAlias("rad:ExpirationDate")
	 private Date expirationDate;

	 @XStreamAlias("rad:ExchangeRate")
	 private String exchangeRate;

	 @XStreamAlias("rad:ExchangeRateDate")
	 private Date exchangeRateDate;

	 @XStreamAlias("rad:FFNumber")
	 private String ffNumber;
	
	 @XStreamAlias("rad:PaymentComment")
	 private String paymentComment;
	
	 @XStreamAlias("rad:PaymentAmount")
	 private double paymentAmount;
	 
	 @XStreamAlias("rad:PaymentMethod")
	 private String paymentMethod;

	 @XStreamAlias("rad:Reference")
	 private String reference;
	
	 @XStreamAlias("rad:TerminalID")
	 private String terminalID;

	 @XStreamAlias("rad:UserData")
	 private String userData;
	
	 @XStreamAlias("rad:UserID")
	 private String userID;

	 @XStreamAlias("rad:IataNumber")
	 private String iataNumber;
	
	 @XStreamAlias("rad:ValueCode")
	 private String valueCode;

	 @XStreamAlias("rad:VoucherNumber")
	 private String voucherNumber;
	
	 @XStreamAlias("rad:IsTACreditCard")
	 private boolean taCreditCard;

	 @XStreamAlias("rad:GcxID")
	 private String gcxID;
	
	 @XStreamAlias("rad:GcxOptOption")
	 private String gcxOptOption;

	 @XStreamAlias("rad:OriginalCurrency")
	 private String originalCurrency;
	
	 @XStreamAlias("rad:OriginalAmount")
	 private double originalAmount;
	 
	 @XStreamAlias("rad:TransactionStatus")
	 private String transactionStatus;

	 @XStreamAlias("rad:AuthorizationCode")
	 private String authorizationCode;
	
	 @XStreamAlias("rad:PaymentReference")
	 private String paymentReference;

	 @XStreamAlias("rad:ResponseMessage")
	 private String responseMessage;
	
	 @XStreamAlias("rad:CardCurrency")
	 private String cardCurrency;
	 
	 @XStreamAlias("rad:BillingCountry")
	 private String billingCountry;
	
	 @XStreamAlias("rad:FingerPrintingSessionID")
	 private String fingerPrintingSessionID;

	public Payor getPayor() {
		return payor;
	}

	public void setPayor(Payor payor) {
		this.payor = payor;
	}

	public double getBaseAmount() {
		return baseAmount;
	}

	public void setBaseAmount(double baseAmount) {
		this.baseAmount = baseAmount;
	}

	public String getBaseCurrency() {
		return baseCurrency;
	}

	public void setBaseCurrency(String baseCurrency) {
		this.baseCurrency = baseCurrency;
	}

	public String getCardHolder() {
		return cardHolder;
	}

	public void setCardHolder(String cardHolder) {
		this.cardHolder = cardHolder;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCheckNumber() {
		return checkNumber;
	}

	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}

	public String getCurrencyPaid() {
		return currencyPaid;
	}

	public void setCurrencyPaid(String currencyPaid) {
		this.currencyPaid = currencyPaid;
	}

	public String getCvCode() {
		return cvCode;
	}

	public void setCvCode(String cvCode) {
		this.cvCode = cvCode;
	}

	public Date getDatePaid() {
		return datePaid;
	}

	public void setDatePaid(Date datePaid) {
		this.datePaid = datePaid;
	}

	public String getDocumentReceivedBy() {
		return documentReceivedBy;
	}

	public void setDocumentReceivedBy(String documentReceivedBy) {
		this.documentReceivedBy = documentReceivedBy;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getExchangeRate() {
		return exchangeRate;
	}

	public void setExchangeRate(String exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Date getExchangeRateDate() {
		return exchangeRateDate;
	}

	public void setExchangeRateDate(Date exchangeRateDate) {
		this.exchangeRateDate = exchangeRateDate;
	}

	public String getFfNumber() {
		return ffNumber;
	}

	public void setFfNumber(String ffNumber) {
		this.ffNumber = ffNumber;
	}

	public String getPaymentComment() {
		return paymentComment;
	}

	public void setPaymentComment(String paymentComment) {
		this.paymentComment = paymentComment;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentMethod() {
		return paymentMethod;
	}

	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public String getTerminalID() {
		return terminalID;
	}

	public void setTerminalID(String terminalID) {
		this.terminalID = terminalID;
	}

	public String getUserData() {
		return userData;
	}

	public void setUserData(String userData) {
		this.userData = userData;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getIataNumber() {
		return iataNumber;
	}

	public void setIataNumber(String iataNumber) {
		this.iataNumber = iataNumber;
	}

	public String getValueCode() {
		return valueCode;
	}

	public void setValueCode(String valueCode) {
		this.valueCode = valueCode;
	}

	public String getVoucherNumber() {
		return voucherNumber;
	}

	public void setVoucherNumber(String voucherNumber) {
		this.voucherNumber = voucherNumber;
	}

	public String getGcxID() {
		return gcxID;
	}

	public void setGcxID(String gcxID) {
		this.gcxID = gcxID;
	}

	public String getGcxOptOption() {
		return gcxOptOption;
	}

	public void setGcxOptOption(String gcxOptOption) {
		this.gcxOptOption = gcxOptOption;
	}

	public String getOriginalCurrency() {
		return originalCurrency;
	}

	public void setOriginalCurrency(String originalCurrency) {
		this.originalCurrency = originalCurrency;
	}

	public double getOriginalAmount() {
		return originalAmount;
	}

	public void setOriginalAmount(double originalAmount) {
		this.originalAmount = originalAmount;
	}

	public String getTransactionStatus() {
		return transactionStatus;
	}

	public void setTransactionStatus(String transactionStatus) {
		this.transactionStatus = transactionStatus;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}

	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getPaymentReference() {
		return paymentReference;
	}

	public void setPaymentReference(String paymentReference) {
		this.paymentReference = paymentReference;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getCardCurrency() {
		return cardCurrency;
	}

	public void setCardCurrency(String cardCurrency) {
		this.cardCurrency = cardCurrency;
	}

	public String getBillingCountry() {
		return billingCountry;
	}

	public void setBillingCountry(String billingCountry) {
		this.billingCountry = billingCountry;
	}

	public String getFingerPrintingSessionID() {
		return fingerPrintingSessionID;
	}

	public void setFingerPrintingSessionID(String fingerPrintingSessionID) {
		this.fingerPrintingSessionID = fingerPrintingSessionID;
	}

	public boolean isTaCreditCard() {
		return taCreditCard;
	}

	public void setTaCreditCard(boolean taCreditCard) {
		this.taCreditCard = taCreditCard;
	}



}

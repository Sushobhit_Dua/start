package org.perfomics.flights.business.flydubai.xo.processpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("ProcessPNRPaymentResponse")
public class ProcessPNRPaymentResponse {

	@XStreamAlias("ProcessPNRPaymentResult")
	 private ProcessPNRPaymentResult processPNRPaymentResult;

	@XStreamAlias("xmlns")	
	@XStreamAsAttribute
	private String xmlns;

	public ProcessPNRPaymentResult getProcessPNRPaymentResult() {
		return processPNRPaymentResult;
	}

	public void setProcessPNRPaymentResult(ProcessPNRPaymentResult processPNRPaymentResult) {
		this.processPNRPaymentResult = processPNRPaymentResult;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}


}

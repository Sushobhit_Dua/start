package org.perfomics.flights.business.flydubai.xo.processpnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad:ReservationInfo")
public class ReservationInfo {
	
	@XStreamAlias("rad1:SeriesNumber")	
	private String seriesNumber;

	@XStreamAlias("rad1:ConfirmationNumber")	
	private String confirmationNumber;

	public String getSeriesNumber() {
		return seriesNumber;
	}

	public void setSeriesNumber(String seriesNumber) {
		this.seriesNumber = seriesNumber;
	}

	public String getConfirmationNumber() {
		return confirmationNumber;
	}

	public void setConfirmationNumber(String confirmationNumber) {
		this.confirmationNumber = confirmationNumber;
	}
	
	

}

package org.perfomics.flights.business.flydubai.xo.processpnr;

import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad:TransactionInfo")
public class TransactionInfo {
	
	@XStreamAlias("rad1:SecurityGUID")	
	private String securityGUID;

	@XStreamAlias("rad1:CarrierCodes")	
	private CarrierCodes carrierCodes;
	
	@XStreamAlias("rad1:ClientIPAddress")	
	private String clientIP;

	@XStreamAlias("rad1:HistoricUserName")	
	private String historicUserName;

	public String getSecurityGUID() {
		return securityGUID;
	}

	public void setSecurityGUID(String securityGUID) {
		this.securityGUID = securityGUID;
	}

	public CarrierCodes getCarrierCodes() {
		return carrierCodes;
	}

	public void setCarrierCodes(CarrierCodes carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getHistoricUserName() {
		return historicUserName;
	}

	public void setHistoricUserName(String historicUserName) {
		this.historicUserName = historicUserName;
	}
	
	

}

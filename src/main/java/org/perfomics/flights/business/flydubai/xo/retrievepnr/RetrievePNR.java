package org.perfomics.flights.business.flydubai.xo.retrievepnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrievePNR")
public class RetrievePNR {
	
	@XStreamAlias("tem:RetrievePnrRequest")
	private RetrievePnrRequest retrievePnrRequest;

	public RetrievePnrRequest getRetrievePnrRequest() {
		return retrievePnrRequest;
	}

	public void setRetrievePnrRequest(RetrievePnrRequest retrievePnrRequest) {
		this.retrievePnrRequest = retrievePnrRequest;
	}

}

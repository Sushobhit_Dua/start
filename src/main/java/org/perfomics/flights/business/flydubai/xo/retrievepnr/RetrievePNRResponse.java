package org.perfomics.flights.business.flydubai.xo.retrievepnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("RetrievePNRResponse")
public class RetrievePNRResponse {
	
	@XStreamAlias("RetrievePNRResult")	
	private RetrievePNRResult retrievePNRResult;

	@XStreamAlias("xmlns")	
	@XStreamAsAttribute
	private String xmlns;

	public RetrievePNRResult getRetrievePNRResult() {
		return retrievePNRResult;
	}

	public void setRetrievePNRResult(RetrievePNRResult retrievePNRResult) {
		this.retrievePNRResult = retrievePNRResult;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}


}

package org.perfomics.flights.business.flydubai.xo.retrievepnr;


import org.perfomics.flights.business.flydubai.xo.processpnr.ReservationInfo;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrievePnrRequest")
public class RetrievePnrRequest {
	
	@XStreamAlias("rad:SecurityGUID")	
	private String securityGUID;

	@XStreamAlias("rad:CarrierCodes")	
	private CarrierCodes carrierCodes;

	@XStreamAlias("rad:ClientIPAddress")	
	private String clientIP;

	@XStreamAlias("rad:HistoricUserName")	
	private String historicUserName;

	@XStreamAlias("rad1:ActionType")	
	private String actionType;
	
	@XStreamAlias("rad1:ReservationInfo")	
	private ReservationInfo reservationInfo;

	public String getSecurityGUID() {
		return securityGUID;
	}

	public void setSecurityGUID(String securityGUID) {
		this.securityGUID = securityGUID;
	}

	public CarrierCodes getCarrierCodes() {
		return carrierCodes;
	}

	public void setCarrierCodes(CarrierCodes carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getHistoricUserName() {
		return historicUserName;
	}

	public void setHistoricUserName(String historicUserName) {
		this.historicUserName = historicUserName;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public ReservationInfo getReservationInfo() {
		return reservationInfo;
	}

	public void setReservationInfo(ReservationInfo reservationInfo) {
		this.reservationInfo = reservationInfo;
	}


}

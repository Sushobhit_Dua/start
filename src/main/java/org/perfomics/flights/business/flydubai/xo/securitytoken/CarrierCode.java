package org.perfomics.flights.business.flydubai.xo.securitytoken;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad:CarrierCode")
public class CarrierCode {
	
	@XStreamAlias("rad:AccessibleCarrierCode")
	private String accessibleCarrierCode;

	public String getAccessibleCarrierCode() {
		return accessibleCarrierCode;
	}

	public void setAccessibleCarrierCode(String accessibleCarrierCode) {
		this.accessibleCarrierCode = accessibleCarrierCode;
	}
	

}

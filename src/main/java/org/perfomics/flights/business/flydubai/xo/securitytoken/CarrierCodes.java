package org.perfomics.flights.business.flydubai.xo.securitytoken;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad:CarrierCodes")
public class CarrierCodes {
	
	@XStreamAlias("rad:CarrierCode")	
	private CarrierCode carrierCode;

	public CarrierCode getCarrierCode() {
		return carrierCode;
	}

	public void setCarrierCode(CarrierCode carrierCode) {
		this.carrierCode = carrierCode;
	}


}

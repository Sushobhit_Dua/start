package org.perfomics.flights.business.flydubai.xo.securitytoken;
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrieveSecurityToken")
public class RetrieveSecurityToken {
	
	@XStreamAlias("tem:RetrieveSecurityTokenRequest")
	private RetrieveSecurityTokenRequest securityTokenRequest;

	public RetrieveSecurityTokenRequest getSecurityTokenRequest() {
		return securityTokenRequest;
	}

	public void setSecurityTokenRequest(RetrieveSecurityTokenRequest securityTokenRequest) {
		this.securityTokenRequest = securityTokenRequest;
	}


	
}

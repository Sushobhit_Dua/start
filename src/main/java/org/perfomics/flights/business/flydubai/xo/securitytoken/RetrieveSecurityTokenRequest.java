package org.perfomics.flights.business.flydubai.xo.securitytoken;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:RetrieveSecurityTokenRequest")
public class RetrieveSecurityTokenRequest {
	
	@XStreamAlias("rad:CarrierCodes")	
	private CarrierCodes carrierCodes;

	@XStreamAlias("rad1:LogonID")	
	private String logonId;

	@XStreamAlias("rad1:Password")	
	private String password;

	public CarrierCodes getCarrierCodes() {
		return carrierCodes;
	}

	public void setCarrierCodes(CarrierCodes carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	public String getLogonId() {
		return logonId;
	}

	public void setLogonId(String logonId) {
		this.logonId = logonId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


}

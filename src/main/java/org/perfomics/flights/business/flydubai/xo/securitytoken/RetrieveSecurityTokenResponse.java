package org.perfomics.flights.business.flydubai.xo.securitytoken;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("RetrieveSecurityTokenResponse")
public class RetrieveSecurityTokenResponse {
	
	@XStreamAlias("xmlns")
	@XStreamAsAttribute
	private String xmlns;
	
	@XStreamAlias("RetrieveSecurityTokenResult")	
	private RetrieveSecurityTokenResult retrieveSecurityTokenResult;

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	public RetrieveSecurityTokenResult getRetrieveSecurityTokenResult() {
		return retrieveSecurityTokenResult;
	}

	public void setRetrieveSecurityTokenResult(RetrieveSecurityTokenResult retrieveSecurityTokenResult) {
		this.retrieveSecurityTokenResult = retrieveSecurityTokenResult;
	}

	
}

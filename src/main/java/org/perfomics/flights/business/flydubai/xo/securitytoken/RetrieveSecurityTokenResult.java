package org.perfomics.flights.business.flydubai.xo.securitytoken;

import org.perfomics.flights.business.flydubai.xo.exceptions.Exceptions;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("RetrieveSecurityTokenResult")
public class RetrieveSecurityTokenResult {

	@XStreamAlias("xmlns:a")
	@XStreamAsAttribute
	private String xmlnsA;
	
	@XStreamAlias("xmlns:i")
	@XStreamAsAttribute
	private String xmlnsI;
	
	@XStreamAlias("a:SecurityToken")
	private String securitytoken;
	
	@XStreamAlias("a:Exceptions")
	private Exceptions exceptions;

	public String getXmlnsA() {
		return xmlnsA;
	}

	public void setXmlnsA(String xmlnsA) {
		this.xmlnsA = xmlnsA;
	}

	public String getXmlnsI() {
		return xmlnsI;
	}

	public void setXmlnsI(String xmlnsI) {
		this.xmlnsI = xmlnsI;
	}

	public String getSecuritytoken() {
		return securitytoken;
	}

	public void setSecuritytoken(String securitytoken) {
		this.securitytoken = securitytoken;
	}

	public Exceptions getExceptions() {
		return exceptions;
	}

	public void setExceptions(Exceptions exceptions) {
		this.exceptions = exceptions;
	}


	
}

package org.perfomics.flights.business.flydubai.xo.summarypnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad1:Address")
public class Address {
	
	@XStreamAlias("rad1:Address1")	
	private String address1;

	@XStreamAlias("rad1:Address2")	
	private String address2;
	
	@XStreamAlias("rad1:City")	
	private String city;

	@XStreamAlias("rad1:State")	
	private String state;

	@XStreamAlias("rad1:Postal")	
	private String postal;

	@XStreamAlias("rad1:Country")	
	private String country;

	@XStreamAlias("a:CountryCode")
	private String countryCode;

	@XStreamAlias("a:AreaCode")
	private String areaCode;

	@XStreamAlias("a:PhoneNumber")
	private String phoneNumber;
	
	@XStreamAlias("rad1:Display")	
	private String display;

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPostal() {
		return postal;
	}

	public void setPostal(String postal) {
		this.postal = postal;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}

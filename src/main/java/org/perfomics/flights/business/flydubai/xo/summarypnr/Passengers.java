package org.perfomics.flights.business.flydubai.xo.summarypnr;

import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("rad1:Passengers")
public class Passengers {
	
	@XStreamImplicit
	private List<Person> personList=new ArrayList<Person>();

	public List<Person> getPersonList() {
		return personList;
	}

	public void addPersonList(Person person) {
		personList.add(person);
	}

	
}

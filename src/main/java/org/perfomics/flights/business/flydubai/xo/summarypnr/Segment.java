package org.perfomics.flights.business.flydubai.xo.summarypnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rad1:Segment")
public class Segment {
	
	@XStreamAlias("rad1:PersonOrgID")	
	private String personOrgID;

	@XStreamAlias("rad1:FareInformationID")	
	private int fareInformationID;

	@XStreamAlias("rad1:MarketingCode")	
	private String marketingCode;
	
	@XStreamAlias("rad1:StoreFrontID")	
	private String storeFrontID;

	@XStreamAlias("rad1:SpecialServices")	
	private String specialServices;

	@XStreamAlias("rad1:Seats")	
	private String seats;

	public String getPersonOrgID() {
		return personOrgID;
	}

	public void setPersonOrgID(String personOrgID) {
		this.personOrgID = personOrgID;
	}

	public int getFareInformationID() {
		return fareInformationID;
	}

	public void setFareInformationID(int fareInformationID) {
		this.fareInformationID = fareInformationID;
	}

	public String getMarketingCode() {
		return marketingCode;
	}

	public void setMarketingCode(String marketingCode) {
		this.marketingCode = marketingCode;
	}

	public String getStoreFrontID() {
		return storeFrontID;
	}

	public void setStoreFrontID(String storeFrontID) {
		this.storeFrontID = storeFrontID;
	}

	public String getSpecialServices() {
		return specialServices;
	}

	public void setSpecialServices(String specialServices) {
		this.specialServices = specialServices;
	}

	public String getSeats() {
		return seats;
	}

	public void setSeats(String seats) {
		this.seats = seats;
	}

}

package org.perfomics.flights.business.flydubai.xo.summarypnr;

import java.util.ArrayList;
import java.util.List;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("rad1:Segments")	
public class Segments {
	
	@XStreamImplicit
	private List<Segment> segmentList=new ArrayList<Segment>();

	public List<Segment> getSegmentList() {
		return segmentList;
	}

	public void addSegmentList(Segment segment) {
		segmentList.add(segment);
	}

}

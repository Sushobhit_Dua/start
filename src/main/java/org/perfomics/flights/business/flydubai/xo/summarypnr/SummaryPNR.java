package org.perfomics.flights.business.flydubai.xo.summarypnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:SummaryPNR")
public class SummaryPNR {
	
	@XStreamAlias("tem:SummaryPnrRequest")
	private SummaryPnrRequest summaryPnrRequest;

	public SummaryPnrRequest getSummaryPnrRequest() {
		return summaryPnrRequest;
	}

	public void setSummaryPnrRequest(SummaryPnrRequest summaryPnrRequest) {
		this.summaryPnrRequest = summaryPnrRequest;
	}


}

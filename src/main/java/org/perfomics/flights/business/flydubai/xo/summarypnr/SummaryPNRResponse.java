package org.perfomics.flights.business.flydubai.xo.summarypnr;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("SummaryPNRResponse")
public class SummaryPNRResponse {
	
	@XStreamAlias("SummaryPNRResult")	
	private SummaryPNRResult summaryPNRResult;

	@XStreamAlias("xmlns")	
	@XStreamAsAttribute
	private String xmlns;

	public SummaryPNRResult getSummaryPNRResult() {
		return summaryPNRResult;
	}

	public void setSummaryPNRResult(SummaryPNRResult summaryPNRResult) {
		this.summaryPNRResult = summaryPNRResult;
	}

	public String getXmlns() {
		return xmlns;
	}

	public void setXmlns(String xmlns) {
		this.xmlns = xmlns;
	}

	

}

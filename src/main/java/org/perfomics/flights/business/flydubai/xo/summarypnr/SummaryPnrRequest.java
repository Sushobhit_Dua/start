package org.perfomics.flights.business.flydubai.xo.summarypnr;

import org.perfomics.flights.business.flydubai.xo.agencytransactionfees.ContactInfos;
import org.perfomics.flights.business.flydubai.xo.createpnr.Payments;
import org.perfomics.flights.business.flydubai.xo.processpnr.ReservationInfo;
import org.perfomics.flights.business.flydubai.xo.securitytoken.CarrierCodes;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("tem:SummaryPnrRequest")
public class SummaryPnrRequest {
	
	@XStreamAlias("rad:SecurityGUID")	
	private String securityGUID;

	@XStreamAlias("rad:CarrierCodes")	
	private CarrierCodes carrierCodes;

	@XStreamAlias("rad:ClientIPAddress")	
	private String clientIP;

	@XStreamAlias("rad:HistoricUserName")	
	private String historicUserName;

	@XStreamAlias("rad1:ActionType")	
	private String actionType;
	
	@XStreamAlias("rad1:ReservationInfo")	
	private ReservationInfo reservationInfo;

	@XStreamAlias("rad1:SecurityToken")	
	private String securityToken;
	
	@XStreamAlias("rad1:CarrierCurrency")	
	private String carrierCurrency;

	@XStreamAlias("rad1:DisplayCurrency")	
	private String displayCurrency;
	
	@XStreamAlias("rad1:IATANum")	
	private String iataNum;

	@XStreamAlias("rad1:User")	
	private String user;
	
	@XStreamAlias("rad1:ReceiptLanguageID")	
	private String receiptLanguageID;

	@XStreamAlias("rad1:PromoCode")	
	private String promoCode;
	
	@XStreamAlias("rad1:ExternalBookingID")	
	private String externalBookingID;

	@XStreamAlias("rad1:Address")	
	private Address address;

	@XStreamAlias("rad1:ContactInfos")	
	private ContactInfos contactInfos;

	@XStreamAlias("rad1:Passengers")	
	private Passengers passengers;
	
	@XStreamAlias("rad1:Segments")	
	private Segments segments;

	@XStreamAlias("rad1:Payments")	
	private Payments payments;

	public String getSecurityGUID() {
		return securityGUID;
	}

	public void setSecurityGUID(String securityGUID) {
		this.securityGUID = securityGUID;
	}

	public CarrierCodes getCarrierCodes() {
		return carrierCodes;
	}

	public void setCarrierCodes(CarrierCodes carrierCodes) {
		this.carrierCodes = carrierCodes;
	}

	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	public String getHistoricUserName() {
		return historicUserName;
	}

	public void setHistoricUserName(String historicUserName) {
		this.historicUserName = historicUserName;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public ReservationInfo getReservationInfo() {
		return reservationInfo;
	}

	public void setReservationInfo(ReservationInfo reservationInfo) {
		this.reservationInfo = reservationInfo;
	}

	public String getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(String securityToken) {
		this.securityToken = securityToken;
	}

	public String getCarrierCurrency() {
		return carrierCurrency;
	}

	public void setCarrierCurrency(String carrierCurrency) {
		this.carrierCurrency = carrierCurrency;
	}

	public String getDisplayCurrency() {
		return displayCurrency;
	}

	public void setDisplayCurrency(String displayCurrency) {
		this.displayCurrency = displayCurrency;
	}

	public String getIataNum() {
		return iataNum;
	}

	public void setIataNum(String iataNum) {
		this.iataNum = iataNum;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getReceiptLanguageID() {
		return receiptLanguageID;
	}

	public void setReceiptLanguageID(String receiptLanguageID) {
		this.receiptLanguageID = receiptLanguageID;
	}

	public String getPromoCode() {
		return promoCode;
	}

	public void setPromoCode(String promoCode) {
		this.promoCode = promoCode;
	}

	public String getExternalBookingID() {
		return externalBookingID;
	}

	public void setExternalBookingID(String externalBookingID) {
		this.externalBookingID = externalBookingID;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public ContactInfos getContactInfos() {
		return contactInfos;
	}

	public void setContactInfos(ContactInfos contactInfos) {
		this.contactInfos = contactInfos;
	}

	public Passengers getPassengers() {
		return passengers;
	}

	public void setPassengers(Passengers passengers) {
		this.passengers = passengers;
	}

	public Segments getSegments() {
		return segments;
	}

	public void setSegments(Segments segments) {
		this.segments = segments;
	}

	public Payments getPayments() {
		return payments;
	}

	public void setPayments(Payments payments) {
		this.payments = payments;
	}
	 
}
